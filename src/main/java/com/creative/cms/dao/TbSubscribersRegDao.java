package com.creative.cms.dao;

import com.creative.cms.beans.TbSubscribersReg;

public interface TbSubscribersRegDao {
	
	public TbSubscribersReg getUserInfo(String msisdn);

}
