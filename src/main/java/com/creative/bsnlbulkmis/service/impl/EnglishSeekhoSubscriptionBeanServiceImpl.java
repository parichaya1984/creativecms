package com.creative.bsnlbulkmis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.EnglishSeekhoSubscriptionBean;
import com.creative.bsnlbulkmis.dao.EnglishSeekhoSubscriptionBeanDao;
import com.creative.bsnlbulkmis.service.EnglishSeekhoSubscriptionBeanService;

@Service
public class EnglishSeekhoSubscriptionBeanServiceImpl implements EnglishSeekhoSubscriptionBeanService{
	
	private EnglishSeekhoSubscriptionBeanDao englishSeekhoSubscriptionBeanDao;

	public void setEnglishSeekhoSubscriptionBeanDao(EnglishSeekhoSubscriptionBeanDao englishSeekhoSubscriptionBeanDao) {
		this.englishSeekhoSubscriptionBeanDao = englishSeekhoSubscriptionBeanDao;
	}
	
	public EnglishSeekhoSubscriptionBeanServiceImpl(){}
	
	@Override
	@Transactional
	public void add(EnglishSeekhoSubscriptionBean entity) {
		this.englishSeekhoSubscriptionBeanDao.add(entity);
	}

	@Override
	@Transactional
	public void saveOrUpdate(EnglishSeekhoSubscriptionBean entity) {
		this.englishSeekhoSubscriptionBeanDao.saveOrUpdate(entity);
	}

	@Override
	@Transactional
	public void update(EnglishSeekhoSubscriptionBean entity) {
		this.englishSeekhoSubscriptionBeanDao.update(entity);
	}

	@Override
	@Transactional
	public void remove(EnglishSeekhoSubscriptionBean entity) {
		this.englishSeekhoSubscriptionBeanDao.remove(entity);
	}

	@Override
	@Transactional
	public EnglishSeekhoSubscriptionBean find(Integer key) {
		return this.englishSeekhoSubscriptionBeanDao.find(key);
	}

	@Override
	@Transactional
	public List<EnglishSeekhoSubscriptionBean> getAll() {
		return this.englishSeekhoSubscriptionBeanDao.getAll();
	}
	
}
