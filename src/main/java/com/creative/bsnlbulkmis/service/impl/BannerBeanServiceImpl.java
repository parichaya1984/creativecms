package com.creative.bsnlbulkmis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.dao.BannerBeanDao;
import com.creative.bsnlbulkmis.service.BannerBeanService;

@Service
public class BannerBeanServiceImpl implements BannerBeanService{
	
	private BannerBeanDao bannerBeanDao;
	
	public void setBannerBeanDao(BannerBeanDao bannerBeanDao) {
		this.bannerBeanDao = bannerBeanDao;
	}

	@Override
	@Transactional
	public List<BannerBean> getBannerListByOperator(int opsid, String operatorname) {
		return this.bannerBeanDao.getBannerListByOperator(opsid, operatorname);
	}

	@Override
	@Transactional
	public void insertBrowsingDetails(BannerBean p) {
		this.bannerBeanDao.insertBrowsingDetails(p);
	}
	

}
