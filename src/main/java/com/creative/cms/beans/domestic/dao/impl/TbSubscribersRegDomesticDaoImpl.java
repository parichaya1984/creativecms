package com.creative.cms.beans.domestic.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.cms.beans.domestic.TbSubscribersRegDomestic;
import com.creative.cms.beans.domestic.dao.TbSubscribersRegDomesticDao;


@Repository
public class TbSubscribersRegDomesticDaoImpl implements TbSubscribersRegDomesticDao{
	
	private static final Logger logger = LoggerFactory.getLogger(TbSubscribersRegDomesticDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public TbSubscribersRegDomestic getUserInfo(String msisdn) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(TbSubscribersRegDomestic.class);
		criteria.add(Restrictions.eq("msisdn", msisdn));
		
		Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("status", 1));
		criteria.add(Restrictions.and(c));
		
		@SuppressWarnings("unchecked")
		List<TbSubscribersRegDomestic> uList = criteria.list();
		System.out.println("Size::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			logger.info("UserInformation loaded successfully, UserInformation details="+uList);
			//return uList.get(uList.size()-1);
			return uList.get(0);
		}
	}

	@Override
	public void saveSubscriberDataFromCallback(TbSubscribersRegDomestic p) {
			Session session = this.sessionFactory.getCurrentSession();
			session.persist(p);		
	}

	
	

}
