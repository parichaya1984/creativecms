package com.creative.bsnlbulkmis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;
import com.creative.bsnlbulkmis.dao.ContentCategoryMapBeanDao;
import com.creative.bsnlbulkmis.service.ContentCategoryMapBeanService;

@Service
public class ContentCategoryMapBeanServiceImpl implements ContentCategoryMapBeanService{
	
	private ContentCategoryMapBeanDao contentCategoryMapBeanDao;
	
	

	public void setContentCategoryMapBeanDao(ContentCategoryMapBeanDao contentCategoryMapBeanDao) {
		this.contentCategoryMapBeanDao = contentCategoryMapBeanDao;
	}



	@Override
	@Transactional
	public List<ContentCategoryMapBean> getAppInfo(int appid, String operatorname) {
		return this.contentCategoryMapBeanDao.getAppInfo(appid, operatorname);
	}

}
