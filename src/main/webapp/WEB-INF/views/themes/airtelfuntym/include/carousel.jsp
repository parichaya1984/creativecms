<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="jssor_1" >
    <div data-u="slides" class="slides" >
        
											<div class="banner-img">
												<img data-u="image" src="${pageContext.request.contextPath}/resources/airtelfuntym/images/Funtym.jpg" alt="">
											</div>
    </div>

    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:15px;right:auto; left:auto;" data-autocenter="1" data-scale="0" data-scale-bottom="0">
        <div data-u="prototype" class="i" style="width:15px;height:15px;">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>

    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:50px;height:50px;top:0px;left:0;" data-autocenter="2" data-scale="0" data-scale-left="0">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:50px;height:50px;top:0px;right:0;" data-autocenter="2" data-scale="0" data-scale-right="0">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>

</div>

<!--slider js-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/playrabbitsiran/js/banner-carousel.js"></script>
<script type="text/javascript">jssor_1_slider_init();</script>