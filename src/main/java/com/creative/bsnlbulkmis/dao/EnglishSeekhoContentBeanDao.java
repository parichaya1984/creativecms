package com.creative.bsnlbulkmis.dao;

import java.util.List;

import com.creative.bsnlbulkmis.beans.EnglishSeekhoContentBean;


public interface EnglishSeekhoContentBeanDao {

	public void add(EnglishSeekhoContentBean entity) ;
    public void saveOrUpdate(EnglishSeekhoContentBean entity) ;
    public void update(EnglishSeekhoContentBean entity) ;
    public void remove(EnglishSeekhoContentBean entity);
    public EnglishSeekhoContentBean find(Integer key);
    public List<EnglishSeekhoContentBean> getAll() ;
}
