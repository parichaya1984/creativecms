package com.creative.bsnlbulkmis.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;
import com.creative.bsnlbulkmis.dao.CategoryAppInfoBeanDao;

@Repository
public class CategoryAppInfoBeanDaoImpl implements CategoryAppInfoBeanDao{
	
	private static final Logger logger = LoggerFactory.getLogger(CategoryAppInfoBeanDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public List<CategoryAppInfoBean> getAppInfo(int appid) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(CategoryAppInfoBean.class);
		criteria.add(Restrictions.eq("appid", appid));
		/*Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("keyword", keyword));
		criteria.add(Restrictions.and(c));*/
		@SuppressWarnings("unchecked")
		List<CategoryAppInfoBean> uList = criteria.list();
		System.out.println("Size of Banner::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			//logger.info("ContentMsisdnKeywordMapping loaded successfully, ContentMsisdnKeywordMapping details="+uList);
			return uList;
		}
	}
	
	

}
