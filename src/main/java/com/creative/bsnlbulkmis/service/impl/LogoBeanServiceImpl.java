package com.creative.bsnlbulkmis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.LogoBean;
import com.creative.bsnlbulkmis.dao.LogoBeanDao;
import com.creative.bsnlbulkmis.service.LogoBeanService;

@Service
public class LogoBeanServiceImpl implements LogoBeanService{
	
	private LogoBeanDao logoBeanDao;

	public void setLogoBeanDao(LogoBeanDao logoBeanDao) {
		this.logoBeanDao = logoBeanDao;
	}

	@Override
	@Transactional
	public List<LogoBean> getAppInfo(String operatorname) {
		return this.logoBeanDao.getAppInfo(operatorname);
	}
	
	

}
