package com.creative.bsnlbulkmis.dao;

import java.util.List;

import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;

public interface CategoryAppInfoBeanDao {

	public List<CategoryAppInfoBean> getAppInfo(int appid);
}
