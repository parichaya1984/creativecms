<!DOCTYPE html>
<html>
<head lang="en">

    <!-- libraries start-->
    <jsp:include page="include/libraries.jsp"/>
    <!-- libraries end -->

</head>

<body>

<!-- header Start -->
<jsp:include page="include/header.jsp"/>
<!-- header End -->

<div class="page-wrapper">
    <div class="container">

        <!-- main content Start -->
        <section>
            <!-- subscription start -->
            <div class="subscription-bg">
                <h3>Subscription</h3>
            </div>
            <div class="clearfix"></div>
            <div class="subscription-info">
                <div><b>you are not subscribe</b></div>
                <div><b>click</b> I want to Subscribe</div>
                <a href="${pageContext.request.contextPath}/playrabbitsbsnl/subscriptionrequest"> <div class="active-subscription"> I want to Subscribe</div> </a>
            </div>
            <!-- subscription end -->
        </section>
        <!-- main content end -->

    </div>
</div>

<!-- footer Start -->
<jsp:include page="include/footer.jsp"/>
<!-- footer End -->

</body>

</html>





