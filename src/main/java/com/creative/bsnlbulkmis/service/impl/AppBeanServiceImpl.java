package com.creative.bsnlbulkmis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.AppBean;
import com.creative.bsnlbulkmis.dao.AppBeanDao;
import com.creative.bsnlbulkmis.service.AppBeanService;

@Service
public class AppBeanServiceImpl implements AppBeanService{
	
	private AppBeanDao appBeanDao;

	public void setAppBeanDao(AppBeanDao appBeanDao) {
		this.appBeanDao = appBeanDao;
	}



	@Override
	@Transactional
	public List<AppBean> getAppInfo(int appid, String operatorname) {
		return this.appBeanDao.getAppInfo(appid, operatorname);
	}
	
	

}
