package com.creative.bsnlbulkmis.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_englishseekho_subscription")
public class EnglishSeekhoSubscriptionBean { 
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="msisdn")
	private String msisdn;
	@Column(name="serviceid")
	private String serviceid;
	@Column(name="class")
	private String tbclass;
	@Column(name="txnid")
	private String txnid;
	@Column(name="action")
	private String action;
	@Column(name="status")
	private String status;
	@Column(name="charging_mode")
	private String charging_mode;
	@Column(name="req_mode")
	private String req_mode;
	@Column(name="request_id")
	private String request_id;
	@Column(name="circle_id")
	private String circle_id;
	@Column(name="sub_type")
	private String sub_type;
	@Column(name="creationdate")
	private Date creationdate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getServiceid() {
		return serviceid;
	}
	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}
	public String getTbclass() {
		return tbclass;
	}
	public void setTbclass(String tbclass) {
		this.tbclass = tbclass;
	}
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCharging_mode() {
		return charging_mode;
	}
	public void setCharging_mode(String charging_mode) {
		this.charging_mode = charging_mode;
	}
	public String getReq_mode() {
		return req_mode;
	}
	public void setReq_mode(String req_mode) {
		this.req_mode = req_mode;
	}
	public String getRequest_id() {
		return request_id;
	}
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}
	public String getCircle_id() {
		return circle_id;
	}
	public void setCircle_id(String circle_id) {
		this.circle_id = circle_id;
	}
	public String getSub_type() {
		return sub_type;
	}
	public void setSub_type(String sub_type) {
		this.sub_type = sub_type;
	}
	public Date getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
	@Override
	public String toString() {
		return "EnglishSeekhoSubscription [id=" + id + ", msisdn=" + msisdn + ", serviceid=" + serviceid + ", tbclass="
				+ tbclass + ", txnid=" + txnid + ", action=" + action + ", status=" + status + ", charging_mode="
				+ charging_mode + ", req_mode=" + req_mode + ", request_id=" + request_id + ", circle_id=" + circle_id
				+ ", sub_type=" + sub_type + ", creationdate=" + creationdate + "]";
	}

	
}
