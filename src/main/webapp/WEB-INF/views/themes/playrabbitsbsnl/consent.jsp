<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- libraries start-->
<jsp:include page="include/libraries.jsp" />
<!-- libraries end -->

</head>
<body>

	<!-- header Start -->
	<jsp:include page="include/header.jsp" />
	<!-- header End -->

	<div class="page-wrapper">

		<div class="container">
			<!-- consent start -->
			<div class="plan-details">
				<div class="consent">
					<img class="img-responsive"
						src="${pageContext.request.contextPath}/resources/images/slider1.jpg"
						alt="" />
				</div>
				<!-- <div class="consent-1">
                <h4>download 5 bollywood gossip from games boots</h4>
                <h4><i class="fa fa-inr" aria-hidden="true"></i> @ 5.00 for 1 day</h4>
            </div> -->
				<div class="consent-2">
					<h4>
						you are paying through your<br /> postpaid or prepaid account
					</h4>
				</div>
				<h4>
					<div class="navbar navbar-default navbar-fixed-top"
						role="navigation">
					</div>
					<br /> <br /> <br />
				</h4>
				<div class="click-to-proceed">
					<h5>click ok to proceed</h5>
					<div class="consent-btn">
						<a href="${pageContext.request.contextPath}/subscription"><input
							value="Ok" name="consent" class="btn ok-button vd-status-ok"
							type="submit"></a> <a
							href="${pageContext.request.contextPath}/"><input
							value="Cancel" name="consent"
							class="btn cancel-button vd-status-ok" type="submit"></a>
					</div>
				</div>
			</div>
			<!--consent end -->
		</div>

	</div>

	<!-- footer Start -->
	<jsp:include page="include/footer.jsp" />
	<!-- footer End -->

</body>

</html>