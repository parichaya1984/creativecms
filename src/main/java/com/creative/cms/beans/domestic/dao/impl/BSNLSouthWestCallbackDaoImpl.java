package com.creative.cms.beans.domestic.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.cms.beans.domestic.BSNLSouthWestCallback;
import com.creative.cms.beans.domestic.dao.BSNLSouthWestCallbackDao;


@Repository
public class BSNLSouthWestCallbackDaoImpl implements BSNLSouthWestCallbackDao{
	
	private static final Logger logger = LoggerFactory.getLogger(BSNLSouthWestCallbackDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void saveOperatorCallback(BSNLSouthWestCallback p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);		
	}

}
