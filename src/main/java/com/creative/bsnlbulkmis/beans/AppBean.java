package com.creative.bsnlbulkmis.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_app")
public class AppBean { 
	
	@Id
	@Column(name="appid")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer appid;
	
	@Column(name="appname")
	private String appname;
	
	@Column(name="appoperator")
	private String appoperator;
	
	@Column(name="appstatus")
	private int appstatus;
	
	@Column(name="appcreationdate")
	private Date appcreationdate;

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public String getAppoperator() {
		return appoperator;
	}

	public void setAppoperator(String appoperator) {
		this.appoperator = appoperator;
	}

	public int getAppstatus() {
		return appstatus;
	}

	public void setAppstatus(int appstatus) {
		this.appstatus = appstatus;
	}

	public Date getAppcreationdate() {
		return appcreationdate;
	}

	public void setAppcreationdate(Date appcreationdate) {
		this.appcreationdate = appcreationdate;
	}

	@Override
	public String toString() {
		return "AppBean [appid=" + appid + ", appname=" + appname + ", appoperator=" + appoperator + ", appstatus="
				+ appstatus + ", appcreationdate=" + appcreationdate + "]";
	}
	
	

}
