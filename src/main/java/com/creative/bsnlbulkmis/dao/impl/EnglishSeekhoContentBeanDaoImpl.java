package com.creative.bsnlbulkmis.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;
import com.creative.bsnlbulkmis.beans.EnglishSeekhoContentBean;
import com.creative.bsnlbulkmis.dao.EnglishSeekhoContentBeanDao;

@Repository
public class EnglishSeekhoContentBeanDaoImpl implements EnglishSeekhoContentBeanDao{
	
	private static final Logger logger = LoggerFactory.getLogger(EnglishSeekhoContentBeanDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void add(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveOrUpdate(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EnglishSeekhoContentBean find(Integer key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EnglishSeekhoContentBean> getAll() {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(EnglishSeekhoContentBean.class);
		@SuppressWarnings("unchecked")
		List<EnglishSeekhoContentBean> uList = criteria.list();
		System.out.println("EnglishSeekhoContentBean Size::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			return uList;
		}
	}
}
