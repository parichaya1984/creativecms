package com.creative.bsnlbulkmis.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.bsnlbulkmis.beans.AppBean;
import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.dao.AppBeanDao;

@Repository
public class AppBeanDaoImpl implements AppBeanDao{
	
	private static final Logger logger = LoggerFactory.getLogger(AppBeanDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public List<AppBean> getAppInfo(int appid, String operatorname) {
		System.out.println("AppID:"+appid+":Operatorname:"+operatorname);
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(AppBean.class);
		criteria.add(Restrictions.eq("appoperator", operatorname));
		//criteria.add(Restrictions.eq("appid", appid));
		Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("appid", appid));
		c.add(Restrictions.eq("appstatus",1));
		criteria.add(Restrictions.and(c));
		@SuppressWarnings("unchecked")
		List<AppBean> uList = criteria.list();
		System.out.println("Size of App::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			return uList;
		}
	}

}
