package com.creative.bsnlbulkmis.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.beans.LogoBean;
import com.creative.bsnlbulkmis.dao.LogoBeanDao;

@Repository
public class LogoBeanDaoImpl implements LogoBeanDao{
	
	private static final Logger logger = LoggerFactory.getLogger(LogoBeanDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public List<LogoBean> getAppInfo(String operatorname) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(LogoBean.class);
		criteria.add(Restrictions.eq("logooperator", operatorname));
		@SuppressWarnings("unchecked")
		List<LogoBean> uList = criteria.list();
		System.out.println("Size of Banner::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			return uList;
		}
	}

}
