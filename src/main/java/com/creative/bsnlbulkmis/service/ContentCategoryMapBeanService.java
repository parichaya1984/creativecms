package com.creative.bsnlbulkmis.service;

import java.util.List;

import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;

public interface ContentCategoryMapBeanService {
	
	public List<ContentCategoryMapBean> getAppInfo(int appid, String operatorname);

}
