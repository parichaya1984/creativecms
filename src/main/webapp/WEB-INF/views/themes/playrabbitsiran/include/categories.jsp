<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="grid-wrapper">
<c:forEach items="${categories }" var="category" varStatus="cnt">
		<c:if test="${category.categoryname == 'Action' }">
			<div class="title-head">
				<div class="title">${category.categoryname}</div>
				<a href="${pageContext.request.contextPath}/playrabbitsiran/listing"><div
						class="see-all">more</div></a>
			</div>
			<div class="clearfix"></div>
			<div class="owl-carousel owl-theme">
				<c:forEach items="${contentCategoryMapBeans }" var="category1"
					varStatus="cnt1">
					<c:if test="${category1.categoryname == 'Action' }">
						<div class="slide-item">
						<c:if test="${disablelinks == 2 }"> 
							<a href="${category1.contentgamepath }">
							</c:if>
								<div class="img-hover">
									<img class="img-responsive"
										src="${category1.contentimagepath }" alt="" />
								</div>
								<div class="content-info">
									<div class="content-title"><c:if test="${disablelinks == 2 }"> <a href="${category1.contentgamepath }">
									
									</a></c:if><c:if test="${disablelinks == 1 }">${category1.contenttitle }</c:if>
									</div>
									<div class="title-name">${category.categoryname}<span
											class="title-fade"></span>
									</div>
									<div class="star-rating">
										<ul>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
										</ul>
									</div>
									<div class="icon">
										<i class="fa fa-gamepad" aria-hidden="true"></i>
									</div>
								</div>

							</a>

						</div>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
		
		<c:if test="${category.categoryname == 'Sports' }">
			<div class="title-head">
				<div class="title">${category.categoryname}</div>
				<a href="${pageContext.request.contextPath}/playrabbitsiran/listing"><div
						class="see-all">more</div></a>
			</div>
			<div class="clearfix"></div>
			<div class="owl-carousel owl-theme">
				<c:forEach items="${contentCategoryMapBeans }" var="category1"
					varStatus="cnt1">
					<c:if test="${category1.categoryname == 'Race' }">
						<div class="slide-item">
							<c:if test="${disablelinks == 2 }"> 
							<a href="${category1.contentgamepath }">
							</c:if>
								<div class="img-hover">
									<img class="img-responsive"
										src="${category1.contentimagepath }" alt="" />
								</div>
								<div class="content-info">
									<div class="content-title"><c:if test="${disablelinks == 2 }"> <a href="${category1.contentgamepath }">
									
									</a></c:if><c:if test="${disablelinks == 1 }">${category1.contenttitle }</c:if>
									</div><div class="title-name">${category.categoryname}<span
											class="title-fade"></span>
									</div>
									<div class="star-rating">
										<ul>
										
												<li><i class="fa fa-star" aria-hidden="true"></i></li>
											
											<!-- <li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li> -->
										</ul>
									</div>
									<div class="icon">
										<i class="fa fa-gamepad" aria-hidden="true"></i>
									</div>
								</div>

							</a>

						</div>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
		
		<c:if test="${category.categoryname == 'Puzzle' }">
			<div class="title-head">
				<div class="title">${category.categoryname}</div>
				<a href="${pageContext.request.contextPath}/playrabbitsiran/listing"><div
						class="see-all">more</div></a>
			</div>
			<div class="clearfix"></div>
			<div class="owl-carousel owl-theme">
				<c:forEach items="${contentCategoryMapBeans }" var="category1"
					varStatus="cnt1">
					<c:if test="${category1.categoryname == 'Puzzle' }">
						<div class="slide-item">
							<c:if test="${disablelinks == 2 }"> 
							<a href="${category1.contentgamepath }">
							</c:if>
								<div class="img-hover">
									<img class="img-responsive"
										src="${category1.contentimagepath }" alt="" />
								</div>
								<div class="content-info">
									<div class="content-title"><c:if test="${disablelinks == 2 }"> <a href="${category1.contentgamepath }">
									
									</a></c:if><c:if test="${disablelinks == 1 }">${category1.contenttitle }</c:if>
									</div><div class="title-name">${category.categoryname}<span
											class="title-fade"></span>
									</div>
									<div class="star-rating">
										<ul>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star" aria-hidden="true"></i></li>
											<li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
										</ul>
									</div>
									<div class="icon">
										<i class="fa fa-gamepad" aria-hidden="true"></i>
									</div>
								</div>

							</a>

						</div>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
	</c:forEach>
        </div>
