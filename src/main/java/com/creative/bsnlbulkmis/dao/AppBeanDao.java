/**
 * 
 */
package com.creative.bsnlbulkmis.dao;

import java.util.List;

import com.creative.bsnlbulkmis.beans.AppBean;
import com.creative.bsnlbulkmis.beans.BannerBean;

/**
 * @author Sonal
 *
 */
public interface AppBeanDao {
	
	public List<AppBean> getAppInfo(int appid, String operatorname);

}
