package com.creative.cms.beans.domestic.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.cms.beans.domestic.TbSubscribersRegDomestic;
import com.creative.cms.beans.domestic.dao.TbSubscribersRegDomesticDao;
import com.creative.cms.beans.domestic.service.TbSubscribersRegDomesticService;



@Service
public class TbSubscribersRegDomesticServiceImpl implements TbSubscribersRegDomesticService{
	
	private TbSubscribersRegDomesticDao tbSubscribersRegDomesticDao;

	

	public void setTbSubscribersRegDomesticDao(TbSubscribersRegDomesticDao tbSubscribersRegDomesticDao) {
		this.tbSubscribersRegDomesticDao = tbSubscribersRegDomesticDao;
	}

	@Override
	@Transactional
	public TbSubscribersRegDomestic getUserInfo(String msisdn) {
		return this.tbSubscribersRegDomesticDao.getUserInfo(msisdn);
	}

	@Override
	@Transactional
	public void saveSubscriberDataFromCallback(TbSubscribersRegDomestic p) {
		this.tbSubscribersRegDomesticDao.saveSubscriberDataFromCallback(p);
	}

	
	

}
