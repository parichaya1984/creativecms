package com.creative.bsnlbulkmis.service;

import java.util.List;

import com.creative.bsnlbulkmis.beans.LogoBean;

public interface LogoBeanService {
	
	public List<LogoBean> getAppInfo(String operatorname);

}
