package com.creative.bsnlbulkmis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.EnglishSeekhoContentBean;
import com.creative.bsnlbulkmis.dao.EnglishSeekhoContentBeanDao;
import com.creative.bsnlbulkmis.service.EnglishSeekhoContentBeanService;

@Service
public class EnglishSeekhoContentBeanServiceImpl implements EnglishSeekhoContentBeanService{
	
	private EnglishSeekhoContentBeanDao englishSeekhoContentBeanDao;

	public void setEnglishSeekhoContentBeanDao(EnglishSeekhoContentBeanDao englishSeekhoContentBeanDao) {
		this.englishSeekhoContentBeanDao = englishSeekhoContentBeanDao;
	}

	@Override
	public void add(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveOrUpdate(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(EnglishSeekhoContentBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EnglishSeekhoContentBean find(Integer key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public List<EnglishSeekhoContentBean> getAll() {
		return this.englishSeekhoContentBeanDao.getAll();
	}
	
	

}
