package com.creative.cms.utilities;


import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

//import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
//import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;

/*import com.ibm.vodafone.encrytion.EncryptionDecryptionUtil;

import net.common.service.IpCheckService;
import net.persist.bean.IPPool;
import net.persist.bean.TrafficRoutingOperatorDetails;*/

public class MUtility {
	/*@Autowired
	private IpCheckService ipCheckService;*/
	
	private static Random random = new Random();
	private static final Logger logger = Logger.getLogger(MUtility.class.getName());

	public static int noOfDaysDiffrence(Timestamp ts1,Timestamp ts2) {

		try {		
	       return (int)ChronoUnit.DAYS.between( ts1.toLocalDateTime(),ts2.toLocalDateTime());
	       
		} catch (Exception ex) {
			logger.error("noOfDaysDiffrence:::exception"+ex);
		}
		return 0;
	}
	
	
	public static boolean isChurn(int operatorId,Timestamp subDate) {

		try {
			
			long days =-1;
			LocalDateTime today = LocalDateTime.now();
         /*if(operatorId==MConstants.AIRTEL_OPERATOR_ID){*/
			if(operatorId==0){
			 days = ChronoUnit.HOURS.between( subDate.toLocalDateTime(),today);
			 /*if(days<=MConstants.CHURN_HOUR){*/
			 if(days<=5){
				 logger.debug("airtel churn :::operatorId:"+operatorId+"::"+days);
				return true;			 }
			}
			else {
				LocalDate today2 = LocalDate.now();
				days = ChronoUnit.DAYS.between(subDate.toLocalDateTime().toLocalDate(),today2);	
				if(days==0){
					logger.debug("other operator churn ::operatorId:"+operatorId+"::"+days);
					return true;	
				}
			}	
			
		} catch (Exception ex) {
			logger.error("isChurn:::exception"+ex);
		}
		return false;
	}

	
	/*public static String encryptValue(String key, String data) {
		EncryptDecryptUtility ed = new EncryptDecryptUtility();
		return ed.doEncrypt(data);
	}*/

	public static long randomNumber(long lowerlimit, long upperlimit) {
		return random.longs(1, lowerlimit, upperlimit).findFirst().getAsLong();
	}

	public static void main(String arg[]) {
		for (int i = 0; i < 100; i++) {
		System.out.println(randomNumber(0,4));
		}
//		int skipReverseNumber = 2;
//		AtomicInteger atomicActReverseSkipNumber = new AtomicInteger(1);
//
//		for (int i = 0; i < 10; i++) {
//			boolean isSendToAdnetwork = (atomicActReverseSkipNumber.getAndUpdate(n -> n < skipReverseNumber ? n + 1 : 1)
//					% skipReverseNumber) == 0;
//			System.out.println("isSendToAdnetwork:: " + isSendToAdnetwork);
//		}

	}

	

	public static String find10DigitMobileNo(String msisdn) {

		if (msisdn != null && msisdn.length() > 10) {
			return msisdn.substring(2);
		}
		return msisdn;
	}

	public static List<String> findBlockSeriesCode(String msisdn) {

		List<String> list = new ArrayList<String>();
		if (msisdn.length() > 10) {
			msisdn = msisdn.substring(2);
			
		}

		if (msisdn != null && msisdn.length() == 10) {
			list.add(msisdn);
		}

		if (msisdn != null && msisdn.length() > 4) {
			list.add(msisdn.substring(0, 5));
		}
		if (msisdn != null && msisdn.length() > 6) {
			list.add(msisdn.substring(0, 6));
		}

		return list;
	}

	public static String findCircleSeriesCode6Digit(String msisdn) {
		return msisdn.substring(0, 6);

	}
	public static String findCircleSeriesCode7Digit(String msisdn) {
		return msisdn.substring(0, 7);

	}

	public static String formatMsisdn(String msisdn) {

		if (msisdn != null && msisdn.length() == 10) {
			msisdn = "91" + msisdn;
		}
		return msisdn;
	}

	public static int hourDiffrence(Timestamp t1) {
		try {
			return (int) ((System.currentTimeMillis() - t1.getTime()) / (1000 * 60 * 60));
		} catch (Exception ex) {
		}
		return 0;
	}

	public static Integer toInt(String str, int defaultValue) {
		try {
			return Integer.parseInt(str);
		} catch (Exception ex) {
		}
		return defaultValue;
	}
	
	
	public static Double toDouble(String str, double defaultValue) {
		try {
			return Double.parseDouble(str);
		} catch (Exception ex) {
		}
		return defaultValue;
	}


	public static boolean trafficRouting(AtomicInteger atomicTrafficRouting, Integer trafficRoutingCounter) {
		try {
			return atomicTrafficRouting.getAndUpdate(n -> ((n == trafficRoutingCounter) ? 1 : n + 1))
					% trafficRoutingCounter > 0;
		} catch (Exception ex) {
		}
		return false;
	}

	public static String urlEncoding(String url, String token) {
		try {
			url = url.replaceAll("<token>", URLEncoder.encode(token, "utf-8"));
		} catch (Exception ex) {
		}
		return url;
	}

	public static String getDomainName(String url) throws MalformedURLException {
		String host = null;
		if (url != null) {
			URL netUrl = new URL(url);
			host = netUrl.getHost();

			if (host != null) {
				host = host.replaceAll("https://", "").replaceAll("http://", "").replaceAll("www", "");
			}
		}
		return host;
	}
	/* public static String getBase64DecodedString(String str){
	        byte[] byteArray = Base64.decodeBase64(str.getBytes());
	        String decodedString = new String(byteArray);
	        return decodedString;
	        
	    }
	    public static String getBase64EncodedString(String str){
	        byte[] byteArray = Base64.encodeBase64(str.getBytes());
	        String encodedString = new String(byteArray);
	        return encodedString;	        
	    }*/
	    
	    public static Long strToLong(String str,long defaultVal){
	    	 Long retVal=defaultVal;
	        try{
	            retVal=Long.parseLong(str);
	        }catch(Exception e){
	            retVal=defaultVal;
	        }
	        return retVal;
	    }
	    
	     public static String formatString(String strVal,String defaultVal){
	         strVal=(strVal==null)?defaultVal:strVal.trim();
	         return strVal;
	     }
	    
	     public static String find12DigitMobileNo(String msisdn) {

	 		if (msisdn != null && msisdn.length() ==10) {
	 			return "91"+msisdn;
	 		}
	 		return msisdn;
	 	}
	     
	     
	     public static Timestamp addNumberOfDay(Timestamp ts,int noOfDay){
	    		Timestamp ts2 =null;
	    		  try{
	    			LocalDateTime time = LocalDateTime.ofInstant(ts.toInstant(), ZoneOffset.ofHours(0));
	    			time = time.plus(noOfDay, ChronoUnit.DAYS);
	    			//Convert back to instant, again, no time zone offset.
	    			Instant output = time.atZone(ZoneOffset.ofHours(0)).toInstant();
	    			 ts2 = Timestamp.from(output);
	    			}catch(Exception ex){
	    				
	    			}
	    			return ts2;
	    		}
	     

	     public static long getHourDiffrence(Timestamp ts1,Timestamp ts2){
	    	 if(ts1==null){
	    		 return -1;
	    	 }
	    	 if(ts2==null){
	    		 return -2;
	    	 }
	    	 
			//return new Period(ts1.getTime(),ts2.getTime()).getHours();
	    	 return 0l;
					
				 
	     }
	     
	    /* public TrafficRoutingOperatorDetails getOperatorIdHeaderByHeaderAndIp(HttpServletRequest request,Integer adnetworkId){
	    	 TrafficRoutingOperatorDetails trafficRoutingOpDeatail=new TrafficRoutingOperatorDetails();
	    	 //Integer operatorId;
	    	 //if (request.getHeader("x-msisdn") != null && request.getHeader("x-apn-id").equalsIgnoreCase("www"))
	    	 logger.info("getOperatorIdHeaderByHeaderAndIp:::"+request.getRemoteAddr());
	    	try{
	    	 if (request.getHeader("x-msisdn") != null && request.getHeader("x-apn-id").equalsIgnoreCase("www"))
	    	 {
	    		 
	    		 trafficRoutingOpDeatail.setOperatorId(MConstansts.OPERATOR_VODA_ID);
	    		 trafficRoutingOpDeatail.setMsisdn(request.getHeader("x-msisdn"));
	    		 trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	 	 		}
	    	 else if(request.getHeader("X-Nokia-msisdn")!= null)
	    	 {
	    		 trafficRoutingOpDeatail.setOperatorId(MConstansts.IDEA_OPERATOR_ID);
	    		 trafficRoutingOpDeatail.setMsisdn(request.getHeader("X-Nokia-msisdn"));
	    		 trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	    	 }
	    	
	    	
	    	// else if(request.getHeader("MSISDN") != null && request.getHeader("APN").equalsIgnoreCase("aircelgprs")){
	    	/*	 else if(request.getHeader("MSISDN") != null){
	    		 trafficRoutingOpDeatail.setOperatorId(MConstansts.AIRCEL_OPERATOR_ID);
	    		 trafficRoutingOpDeatail.setMsisdn(request.getHeader("MSISDN"));
	    		 trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);*/
	    	// }
	    	/* else if(request.getHeader("x-msisdn") != null){
	 	    	//	 else if(request.getHeader("MSISDN") != null){
	 	    		 trafficRoutingOpDeatail.setOperatorId(MConstansts.BSNLSOUTH_OPERATOR_ID);
	 	    		 trafficRoutingOpDeatail.setMsisdn(request.getHeader("x-msisdn"));
	 	    		 trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	 	    	 }
	    	 else if(request.getHeader("MSISDN") != null&&request.getHeader("APN").equalsIgnoreCase("bsnlnet")){
		 	    	//	 else if(request.getHeader("MSISDN") != null){
		 	    		 trafficRoutingOpDeatail.setOperatorId(MConstansts.BSNLNORTH_OPERATOR_ID);
		 	    		 trafficRoutingOpDeatail.setMsisdn(request.getHeader("x-msisdn"));
		 	    		 trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
		 	    	 }
	    	
	    	 else {
	    			 String remoteAddress = request.getHeader("X-ClientIP");
	    			 //String remoteAddress = request.getRemoteAddr();
	    			// remoteAddress="10.122.0.0";
	    			 //logger.info("getOperatorIdHeaderByHeaderAndIp::: before  ip ppass"+remoteAddress);
	 		     IPPool ipPool = ipCheckService.findOperatorByIp(remoteAddress);
	 		   // IPPool ipPool = ipCheckService.findAllIpPool(remoteAddress);
	 		    
	 		     
	 		    logger.info("getOperatorIdHeaderByHeaderAndIp:::;;;;"+remoteAddress+"::");
	 		    if(ipPool!=null&&ipPool.getOpid() == MConstansts.AIRTEL_OPERATOR_ID){
	 		    	 logger.info("getOperatorIdHeaderByHeaderAndIp:::"+remoteAddress+"::"+ipPool.getOpid());
	 		    	 trafficRoutingOpDeatail.setOperatorId(MConstansts.AIRTEL_OPERATOR_ID);
	 		    	trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	 		    	 
	 		    }
	 		   
	 		    else if(ipPool!=null&&ipPool.getOpid() == MConstansts.BSNLEAST_OPERATOR_ID){
	 		    	 logger.info("getOperatorIdHeaderByHeaderAndIp:::"+remoteAddress+"::"+ipPool.getOpid());
	 		    	 trafficRoutingOpDeatail.setOperatorId(MConstansts.BSNLEAST_OPERATOR_ID);
	 		    	trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	 		    }
	 		   else if(ipPool!=null&&ipPool.getOpid() == MConstansts.BSNLWEST_OPERATOR_ID){
	 			  logger.info("getOperatorIdHeaderByHeaderAndIp:::"+remoteAddress+"::"+ipPool.getOpid());
	 			  trafficRoutingOpDeatail.setOperatorId(MConstansts.BSNLWEST_OPERATOR_ID);
	 			 trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
		 		    }
	 		   else if(ipPool!=null&&ipPool.getOpid() == MConstansts.BSNLNORTH_OPERATOR_ID){
	 			  logger.info("getOperatorIdHeaderByHeaderAndIp:::"+remoteAddress+"::"+ipPool.getOpid());
	 			  trafficRoutingOpDeatail.setOperatorId(MConstansts.BSNLNORTH_OPERATOR_ID);
	 			 trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	 		    }
	 		  
	 		  else if(ipPool!=null&&ipPool.getOpid() == MConstansts.BSNLSOUTH_OPERATOR_ID){
	 			 logger.info("getOperatorIdHeaderByHeaderAndIp:::"+remoteAddress+"::"+ipPool.getOpid());
	 			 trafficRoutingOpDeatail.setOperatorId(MConstansts.BSNLSOUTH_OPERATOR_ID);
	 			trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	 		    }
	 		/* else if(ipPool!=null&&ipPool.getOpid() == MConstansts.IDEA_OPERATOR_ID){
	 			 logger.info("getOperatorIdHeaderByHeaderAndIp:::"+remoteAddress+"::"+ipPool.getOpid());
 		    	 trafficRoutingOpDeatail.setOperatorId(MConstansts.IDEA_OPERATOR_ID);
 		    	trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
 		    }
	 		else if(ipPool!=null&&ipPool.getOpid() == MConstansts.AIRCEL_OPERATOR_ID){
	 			 logger.info("getOperatorIdHeaderByHeaderAndIp:::"+remoteAddress+"::"+ipPool.getOpid());
		    	 trafficRoutingOpDeatail.setOperatorId(MConstansts.AIRCEL_OPERATOR_ID);
		    	trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
		    }
	 		    
	 		 else{
	 			 logger.info("getOperatorIdHeaderByHeaderAndIp:::not suitable ip found::"+remoteAddress);
	 			trafficRoutingOpDeatail.setOperatorId(0);
	 			trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(0);
	 			
	 			
	 			// trafficRoutingOpDeatail.setOperatorId(MConstansts.AIRTEL_OPERATOR_ID);
	 		    //	trafficRoutingOpDeatail.setTrafficRoutingAdnetworkId(adnetworkId);
	 		 }
	    		 
	    	    	 
	    	 }}
	    	catch(Exception ee)
	    	{
	    		logger.error("getOperatorIdHeaderByHeaderAndIp::::exception"+ee);
	    	}
	    	 return trafficRoutingOpDeatail;	
	     }
	    	*/
	    	}

