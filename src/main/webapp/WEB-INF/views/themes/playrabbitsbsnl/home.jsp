<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html dir="ltr">
<head>
    <!-- libraries start-->
    <jsp:include page="include/libraries.jsp"/>
    <!-- libraries end -->
</head>

<body>

<!-- header start-->
<jsp:include page="include/header.jsp"/>
<!-- header end -->

<div class="page-wrapper">

    <!-- carousel start-->
    <jsp:include page="include/carousel.jsp"/>
    <!-- carousel end -->

    <!-- message start-->
    <jsp:include page="include/message.jsp"/>
    <!-- message end -->

    <div class="container">

<jsp:include page="include/categories.jsp"/>
        

    </div>

</div>

<!-- footer start-->
<jsp:include page="include/footer.jsp"/>
<!-- footer end -->

</body>
</html>


