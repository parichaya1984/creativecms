package com.creative.bsnlbulkmis.beans;

import java.util.Date;

public class UploadFileBean {
	
	private String filepathname;
	private String zone;
	private String remark;
	private String uploadername;
	private String content;
	private int contentkeywordid;
	private Date date;
	private int isEnable;
	private String keyword;
	public String getFilepathname() {
		return filepathname;
	}
	public void setFilepathname(String filepathname) {
		this.filepathname = filepathname;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getUploadername() {
		return uploadername;
	}
	public void setUploadername(String uploadername) {
		this.uploadername = uploadername;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getContentkeywordid() {
		return contentkeywordid;
	}
	public void setContentkeywordid(int contentkeywordid) {
		this.contentkeywordid = contentkeywordid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getIsEnable() {
		return isEnable;
	}
	public void setIsEnable(int isEnable) {
		this.isEnable = isEnable;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	@Override
	public String toString() {
		return "UploadFileBean [filepathname=" + filepathname + ", zone=" + zone + ", remark=" + remark
				+ ", uploadername=" + uploadername + ", content=" + content + ", contentkeywordid=" + contentkeywordid
				+ ", date=" + date + ", isEnable=" + isEnable + ", keyword=" + keyword + "]";
	}
	
	
	

}
