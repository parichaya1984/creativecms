<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="left-side-inner">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">
					<li class="active"><a href="${pageContext.request.contextPath }/"><i
							class="lnr lnr-home"></i><span>Home</span></a></li>
					
					<!-- <li><a href="#"><i class="camera"></i> <span>Lessons</span></a></li> -->
					<!-- <li><a href="#" data-toggle="modal" data-target="#myModal1"><i
							class="fa fa-th"></i><span>Apps</span></a></li> -->
					
					<!-- <li><a href="#"><i class="lnr lnr-users"></i> <span>Popular</span></a></li>
					
					<li><a href="#"><i class="lnr lnr-music-note"></i>
							<span>Albums</span></a></li> -->
					
					<!-- <li class="menu-list"><a href="#"><i
							class="lnr lnr-indent-increase"></i> <span>Quiz</span></a>
						<ul class="sub-menu-list">
							<li><a href="#">Play Quiz</a></li>
							<li><a href="#">USSD Quiz</a></li>
						</ul></li> -->
						
						<li><a href="#"><i class="lnr lnr-users"></i> <span>Services</span></a></li>
						
						<li class="menu-list"><a href="#"><i
							class="lnr lnr-indent-increase"></i> <span>Cateogries</span></a>
						<ul class="sub-menu-list">
							<c:forEach items="${categories }" var="category" varStatus="cnt">
								<li><a href="${pageContext.request.contextPath }/">${category.categoryname }</a></li>
							</c:forEach>
						</ul></li>
					<!-- <li><a href="blog.html"><i class="lnr lnr-book"></i><span>Blog</span></a></li>
					
					
					<li><a href="typography.html"><i class="lnr lnr-pencil"></i>
							<span>Typography</span></a></li> -->
					<!-- <li class="menu-list"><a href="#"><i class="lnr lnr-heart"></i>
							<span>My Favourities</span></a>
						<ul class="sub-menu-list">
							<li><a href="radio.html">All Songs</a></li>
						</ul></li> -->
					
					<li class="menu-list"><a href="#"><i
							class="fa fa-thumb-tack"></i><span>Tutorials</span></a>
						<ul class="sub-menu-list">
							<!-- <li><a href="http://live.vodafone.in/">Vodafone Live</a></li> -->
						</ul></li>
				</ul>
				<!--sidebar nav end-->
			</div>