<!DOCTYPE html>
<html>
<head lang="en">

    <!-- libraries start-->
    <jsp:include page="include/libraries.jsp"/>
    <!-- libraries end -->

</head>

<body>

<!-- header Start -->
<jsp:include page="include/header.jsp"/>
<!-- header End -->

<div class="page-wrapper">
    <div class="container">

        <div class="grid-wrapper">
            <div class="title-head">
                <div class="listing-title">race</div>
            </div>
            <div class="clearfix"></div>

                <div class="content-grid">
                    <a href="details.php">
                        <div class="img-hover">
                            <img class="img-responsive" src="${pageContext.request.contextPath}/resources/images/pigonbomber.jpg" alt=""/>
                        </div>
                        <div class="content-info">
                            <div class="content-title">Pigeon Bomber </div>
                            <div class="title-name">puzzle<span class="title-fade"></span></div>
                            <div class="star-rating">
                                <ul>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                            <div class="icon"><i class="fa fa-gamepad" aria-hidden="true"></i></div>
                        </div>
                    </a>
                </div>


        </div>

    </div>
</div>

<!-- footer Start -->
<jsp:include page="include/footer.jsp"/>
<!-- footer End -->

</body>

</html>


