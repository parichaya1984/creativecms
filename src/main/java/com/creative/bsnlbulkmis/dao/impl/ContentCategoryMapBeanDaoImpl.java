package com.creative.bsnlbulkmis.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;
import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;
import com.creative.bsnlbulkmis.dao.ContentCategoryMapBeanDao;

@Repository
public class ContentCategoryMapBeanDaoImpl implements ContentCategoryMapBeanDao{
	
	private static final Logger logger = LoggerFactory.getLogger(ContentCategoryMapBeanDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public List<ContentCategoryMapBean> getAppInfo(int appid, String operatorname) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(ContentCategoryMapBean.class);
		criteria.add(Restrictions.eq("appid", appid));
		Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("operator", operatorname));
		criteria.add(Restrictions.and(c));
		@SuppressWarnings("unchecked")
		List<ContentCategoryMapBean> uList = criteria.list();
		System.out.println("Size of Banner::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			//logger.info("ContentMsisdnKeywordMapping loaded successfully, ContentMsisdnKeywordMapping details="+uList);
			return uList;
		}
	}

	

}
