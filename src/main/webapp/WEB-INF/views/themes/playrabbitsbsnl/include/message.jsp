<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="message-status">
    <c:if test="${screenpage == -1 }">
    
    <div class="message-alert">
       Please Enter OTP Shared on your Mobile
    </div>
    
    <div class="price-btn">
        <!-- <a href="#">monthly</a>
        <a href="#">15days</a>
        <a href="#">weekly</a>
        <a href="#">Daily</a> -->
        <form action="${pageContext.request.contextPath}/playrabbitsiran/validateOtp" id="validateotp" method="get"> 
        	<input type="text" id="otp" name="otp"/>
        	<input type="hidden" id="msisdn" name="msisdn" value="${msisdn }"/>
        	<input type="submit" value="Enter Otp">
        </form>
    </div>
    
    </c:if>
    <c:if test="${otppage == 1 }">
    <div class="message-alert">
      Monthly Unlimited Games @ 30 Rs for 30 Days
    </div>
    
    <div class="price-btn">
        <a href="${pageContext.request.contextPath}/playrabbitsbsnl/unlimitedgamessubscribebsnl">Click to Subscribe</a>
        <!-- <a href="#">15days</a>
        <a href="#">weekly</a>
        <a href="#">Daily</a> -->
        <%-- <form action="${pageContext.request.contextPath}/playrabbitsiran/sendOtp" id="validatemobileno" method="get"> 
        	<input type="text" id="msisdn" name="msisdn" />
        	<input type="submit" value="Validate Mobile Number">
        </form> --%>
    </div>
    </c:if>
    
    <c:if test="${screenpage == 1 }">
    <div class="message-alert">
    <c:if test="${screenmessage == 1 }">
      Congratulation!!!
      </c:if>
      <c:if test="${screenmessage == 0 }">
      Sorry!!!
      </c:if>
    </div>
    
    <div class="price-btn">
    <c:if test="${screenmessage == 1 }">
       We have received your request. You can continue playing games
      </c:if>
       <c:if test="${screenmessage == 0 }">
       We are unable to process your request presently. Please try later!!!
      </c:if>
      
       
        <!-- <a href="#">15days</a>
        <a href="#">weekly</a>
        <a href="#">Daily</a> -->
        <%-- <form action="${pageContext.request.contextPath}/playrabbitsiran/sendOtp" id="validatemobileno" method="get"> 
        	<input type="text" id="msisdn" name="msisdn" />
        	<input type="submit" value="Validate Mobile Number">
        </form> --%>
    </div>
    </c:if>
    <c:if test="${otppage == 3 }">
    <div class="message-alert">
       Welcome to Play Rabbits
    </div>
    </c:if>
</div>