<!--touch script-->

    new(function() {});
! function(g, j, c, f, d, k, h) {
    var e = {
        ec: function(a) {
            return a
        },
        te: function(a) {
            return -a * (a - 2)
        }
    };
    var b = new function() {
        var i = this,
            Ab = /\S+/g,
            I = 1,
            bb = 2,
            eb = 3,
            db = 4,
            hb = 5,
            J, s = 0,
            l = 0,
            t = 0,
            X = 0,
            A = 0,
            L = navigator,
            mb = L.appName,
            o = L.userAgent,
            p = parseFloat;

        function Ib() {
            if (!J) {
                J = {
                    Te: "ontouchstart" in g || "createTouch" in j
                };
                var a;
                if (L.pointerEnabled || (a = L.msPointerEnabled)) J.nd = a ? "msTouchAction" : "touchAction"
            }
            return J
        }

        function w(h) {
            if (!s) {
                s = -1;
                if (mb == "Microsoft Internet Explorer" && !!g.attachEvent && !!g.ActiveXObject) {
                    var e = o.indexOf("MSIE");
                    s = I;
                    t = p(o.substring(e + 5, o.indexOf(";", e))); /*@cc_on X=@_jscript_version@*/ ;
                    l = j.documentMode || t
                } else if (mb == "Netscape" && !!g.addEventListener) {
                    var d = o.indexOf("Firefox"),
                        b = o.indexOf("Safari"),
                        f = o.indexOf("Chrome"),
                        c = o.indexOf("AppleWebKit");
                    if (d >= 0) {
                        s = bb;
                        l = p(o.substring(d + 8))
                    } else if (b >= 0) {
                        var i = o.substring(0, b).lastIndexOf("/");
                        s = f >= 0 ? db : eb;
                        l = p(o.substring(i + 1, b))
                    } else {
                        var a = /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/i.exec(o);
                        if (a) {
                            s = I;
                            l = t = p(a[1])
                        }
                    }
                    if (c >= 0) A = p(o.substring(c + 12))
                } else {
                    var a = /(opera)(?:.*version|)[ \/]([\w.]+)/i.exec(o);
                    if (a) {
                        s = hb;
                        l = p(a[2])
                    }
                }
            }
            return h == s
        }

        function q() {
            return w(I)
        }

        function yb() {
            return q() && (l < 6 || j.compatMode == "BackCompat")
        }

        function Bb() {
            return w(bb)
        }

        function cb() {
            return w(eb)
        }

        function gb() {
            return w(hb)
        }

        function ub() {
            return cb() && A > 534 && A < 535
        }

        function M() {
            w();
            return A > 537 || l > 42 || s == I && l >= 11
        }

        function wb() {
            return q() && l < 9
        }

        function vb(a) {
            var b, c;
            return function(f) {
                if (!b) {
                    b = d;
                    var e = a.substr(0, 1).toUpperCase() + a.substr(1);
                    n([a].concat(["WebKit", "ms", "Moz", "O", "webkit"]), function(g, d) {
                        var b = a;
                        if (d) b = g + e;
                        if (f.style[b] != h) return c = b
                    })
                }
                return c
            }
        }

        function tb(b) {
            var a;
            return function(c) {
                a = a || vb(b)(c) || b;
                return a
            }
        }
        var N = tb("transform");

        function lb(a) {
            return {}.toString.call(a)
        }
        var ib = {};
        n(["Boolean", "Number", "String", "Function", "Array", "Date", "RegExp", "Object"], function(a) {
            ib["[object " + a + "]"] = a.toLowerCase()
        });

        function n(b, d) {
            var a, c;
            if (lb(b) == "[object Array]") {
                for (a = 0; a < b.length; a++)
                    if (c = d(b[a], a, b)) return c
            } else
                for (a in b)
                    if (c = d(b[a], a, b)) return c
        }

        function E(a) {
            return a == f ? String(a) : ib[lb(a)] || "object"
        }

        function jb(a) {
            for (var b in a) return d
        }

        function B(a) {
            try {
                return E(a) == "object" && !a.nodeType && a != a.window && (!a.constructor || {}.hasOwnProperty.call(a.constructor.prototype, "isPrototypeOf"))
            } catch (b) {}
        }

        function v(a, b) {
            return {
                x: a,
                y: b
            }
        }

        function qb(b, a) {
            setTimeout(b, a || 0)
        }

        function K(b, d, c) {
            var a = !b || b == "inherit" ? "" : b;
            n(d, function(c) {
                var b = c.exec(a);
                if (b) {
                    var d = a.substr(0, b.index),
                        e = a.substr(b.index + b[0].length + 1, a.length - 1);
                    a = d + e
                }
            });
            a && (c += (!a.indexOf(" ") ? "" : " ") + a);
            return c
        }

        function sb(b, a) {
            if (l < 9) b.style.filter = a
        }

        function nb(a, b) {
            if (a === h) a = b;
            return a
        }
        i.ze = Ib;
        i.id = q;
        i.De = yb;
        i.Ee = Bb;
        i.Fe = cb;
        i.Ie = M;
        vb("transform");
        i.Xc = function() {
            return l
        };
        i.Wc = function() {
            return t || l
        };
        i.Uc = qb;
        i.E = nb;
        i.Oe = function(a, b) {
            b.call(a);
            return D({}, a)
        };

        function W(a) {
            a.constructor === W.caller && a.Kb && a.Kb.apply(a, W.caller.arguments)
        }
        i.Kb = W;
        i.yb = function(a) {
            if (i.Se(a)) a = j.getElementById(a);
            return a
        };

        function u(a) {
            return a || g.event
        }
        i.Zb = function(b) {
            b = u(b);
            var a = b.target || b.srcElement || j;
            if (a.nodeType == 3) a = i.Xb(a);
            return a
        };
        i.od = function(a) {
            a = u(a);
            return {
                x: a.pageX || a.clientX || 0,
                y: a.pageY || a.clientY || 0
            }
        };

        function x(c, d, a) {
            if (a !== h) c.style[d] = a == h ? "" : a;
            else {
                var b = c.currentStyle || c.style;
                a = b[d];
                if (a == "" && g.getComputedStyle) {
                    b = c.ownerDocument.defaultView.getComputedStyle(c, f);
                    b && (a = b.getPropertyValue(d) || b[d])
                }
                return a
            }
        }

        function Z(b, c, a, d) {
            if (a === h) {
                a = p(x(b, c));
                isNaN(a) && (a = f);
                return a
            }
            if (a == f) a = "";
            else d && (a += "px");
            x(b, c, a)
        }

        function m(c, a) {
            var d = a ? Z : x,
                b;
            if (a & 4) b = tb(c);
            return function(e, f) {
                return d(e, b ? b(e) : c, f, a & 2)
            }
        }

        function Db(b) {
            if (q() && t < 9) {
                var a = /opacity=([^)]*)/.exec(b.style.filter || "");
                return a ? p(a[1]) / 100 : 1
            } else return p(b.style.opacity || "1")
        }

        function Fb(b, a, f) {
            if (q() && t < 9) {
                var h = b.style.filter || "",
                    i = new RegExp(/[\s]*alpha\([^\)]*\)/g),
                    e = c.round(100 * a),
                    d = "";
                if (e < 100 || f) d = "alpha(opacity=" + e + ") ";
                var g = K(h, [i], d);
                sb(b, g)
            } else b.style.opacity = a == 1 ? "" : c.round(a * 100) / 100
        }
        var O = {
            G: ["rotate"],
            W: ["rotateX"],
            S: ["rotateY"],
            Pb: ["skewX"],
            Hb: ["skewY"]
        };
        if (!M()) O = D(O, {
            z: ["scaleX", 2],
            v: ["scaleY", 2],
            V: ["translateZ", 1]
        });

        function P(d, a) {
            var c = "";
            if (a) {
                if (q() && l && l < 10) {
                    delete a.W;
                    delete a.S;
                    delete a.V
                }
                b.f(a, function(d, b) {
                    var a = O[b];
                    if (a) {
                        var e = a[1] || 0;
                        if (Q[b] != d) c += " " + a[0] + "(" + d + (["deg", "px", ""])[e] + ")"
                    }
                });
                if (M()) {
                    if (a.cb || a.Y || a.V != h) c += " translate3d(" + (a.cb || 0) + "px," + (a.Y || 0) + "px," + (a.V || 0) + "px)";
                    if (a.z == h) a.z = 1;
                    if (a.v == h) a.v = 1;
                    if (a.z != 1 || a.v != 1) c += " scale3d(" + a.z + ", " + a.v + ", 1)"
                }
            }
            d.style[N(d)] = c
        }
        i.Ke = m("transformOrigin", 4);
        i.Je = m("backfaceVisibility", 4);
        i.Ce = m("transformStyle", 4);
        i.Ae = m("perspective", 6);
        i.mf = m("perspectiveOrigin", 4);
        i.ef = function(b, a) {
            if (q() && t < 9 || t < 10 && yb()) b.style.zoom = a == 1 ? "" : a;
            else {
                var c = N(b),
                    f = a == 1 ? "" : "scale(" + a + ")",
                    e = b.style[c],
                    g = new RegExp(/[\s]*scale\(.*?\)/g),
                    d = K(e, [g], f);
                b.style[c] = d
            }
        };
        i.a = function(a, d, b, c) {
            a = i.yb(a);
            if (a.addEventListener) {
                d == "mousewheel" && a.addEventListener("DOMMouseScroll", b, c);
                a.addEventListener(d, b, c)
            } else if (a.attachEvent) {
                a.attachEvent("on" + d, b);
                c && a.setCapture && a.setCapture()
            }
        };
        i.H = function(a, c, d, b) {
            a = i.yb(a);
            if (a.removeEventListener) {
                c == "mousewheel" && a.removeEventListener("DOMMouseScroll", d, b);
                a.removeEventListener(c, d, b)
            } else if (a.detachEvent) {
                a.detachEvent("on" + c, d);
                b && a.releaseCapture && a.releaseCapture()
            }
        };
        i.Mb = function(a) {
            a = u(a);
            a.preventDefault && a.preventDefault();
            a.cancel = d;
            a.returnValue = k
        };
        i.bf = function(a) {
            a = u(a);
            a.stopPropagation && a.stopPropagation();
            a.cancelBubble = d
        };
        i.I = function(d, c) {
            var a = [].slice.call(arguments, 2),
                b = function() {
                    var b = a.concat([].slice.call(arguments, 0));
                    return c.apply(d, b)
                };
            return b
        };
        i.jf = function(a, b) {
            if (b == h) return a.textContent || a.innerText;
            var c = j.createTextNode(b);
            i.Yb(a);
            a.appendChild(c)
        };
        i.Bb = function(d, c) {
            for (var b = [], a = d.firstChild; a; a = a.nextSibling)(c || a.nodeType == 1) && b.push(a);
            return b
        };

        function kb(a, c, e, b) {
            b = b || "u";
            for (a = a ? a.firstChild : f; a; a = a.nextSibling)
                if (a.nodeType == 1) {
                    if (H(a, b) == c) return a;
                    if (!e) {
                        var d = kb(a, c, e, b);
                        if (d) return d
                    }
                }
        }
        i.Vb = kb;

        function U(a, d, g, b) {
            b = b || "u";
            var c = [];
            for (a = a ? a.firstChild : f; a; a = a.nextSibling)
                if (a.nodeType == 1) {
                    H(a, b) == d && c.push(a);
                    if (!g) {
                        var e = U(a, d, g, b);
                        if (e.length) c = c.concat(e)
                    }
                }
            return c
        }

        function fb(a, c, d) {
            for (a = a ? a.firstChild : f; a; a = a.nextSibling)
                if (a.nodeType == 1) {
                    if (a.tagName == c) return a;
                    if (!d) {
                        var b = fb(a, c, d);
                        if (b) return b
                    }
                }
        }
        i.Pe = fb;
        i.Qe = function(b, a) {
            return b.getElementsByTagName(a)
        };
        i.tb = function(a, f, d) {
            d = d || "u";
            var e;
            do {
                if (a.nodeType == 1) {
                    var c = b.k(a, d);
                    if (c && c == nb(f, c)) {
                        e = a;
                        break
                    }
                }
                a = b.Xb(a)
            } while (a && a != j.body);
            return e
        };

        function D() {
            var e = arguments,
                d, c, b, a, g = 1 & e[0],
                f = 1 + g;
            d = e[f - 1] || {};
            for (; f < e.length; f++)
                if (c = e[f])
                    for (b in c) {
                        a = c[b];
                        if (a !== h) {
                            a = c[b];
                            var i = d[b];
                            d[b] = g && (B(i) || B(a)) ? D(g, {}, i, a) : a
                        }
                    }
            return d
        }
        i.db = D;

        function Y(f, g) {
            var d = {},
                c, a, b;
            for (c in f) {
                a = f[c];
                b = g[c];
                if (a !== b) {
                    var e;
                    if (B(a) && B(b)) {
                        a = Y(a, b);
                        e = !jb(a)
                    }!e && (d[c] = a)
                }
            }
            return d
        }
        i.Sc = function(a) {
            return E(a) == "function"
        };
        i.Se = function(a) {
            return E(a) == "string"
        };
        i.Ne = function(a) {
            return !isNaN(p(a)) && isFinite(a)
        };
        i.f = n;

        function S(a) {
            return j.createElement(a)
        }
        i.Tb = function() {
            return S("DIV")
        };
        i.Me = function() {
            return S("SPAN")
        };
        i.Mc = function() {};

        function C(b, c, a) {
            if (a == h) return b.getAttribute(c);
            b.setAttribute(c, a)
        }

        function H(a, b) {
            return C(a, b) || C(a, "data-" + b)
        }
        i.u = C;
        i.k = H;
        i.jb = function(d, b, c) {
            var a = i.He(H(d, b));
            if (isNaN(a)) a = c;
            return a
        };

        function y(b, a) {
            return C(b, "class", a) || ""
        }

        function pb(b) {
            var a = {};
            n(b, function(b) {
                if (b != h) a[b] = b
            });
            return a
        }

        function rb(b, a) {
            return b.match(a || Ab)
        }

        function R(b, a) {
            return pb(rb(b || "", a))
        }
        i.Pc = pb;
        i.Ge = rb;

        function ab(b, c) {
            var a = "";
            n(c, function(c) {
                a && (a += b);
                a += c
            });
            return a
        }

        function F(a, c, b) {
            y(a, ab(" ", D(Y(R(y(a)), R(c)), R(b))))
        }
        i.Xb = function(a) {
            return a.parentNode
        };
        i.L = function(a) {
            i.wb(a, "none")
        };
        i.M = function(a, b) {
            i.wb(a, b ? "none" : "")
        };
        i.Be = function(b, a) {
            b.removeAttribute(a)
        };
        i.xe = function(d, a) {
            if (a) d.style.clip = "rect(" + c.round(a.g || a.o || 0) + "px " + c.round(a.l) + "px " + c.round(a.m) + "px " + c.round(a.i || a.q || 0) + "px)";
            else if (a !== h) {
                var g = d.style.cssText,
                    f = [new RegExp(/[\s]*clip: rect\(.*?\)[;]?/i), new RegExp(/[\s]*cliptop: .*?[;]?/i), new RegExp(/[\s]*clipright: .*?[;]?/i), new RegExp(/[\s]*clipbottom: .*?[;]?/i), new RegExp(/[\s]*clipleft: .*?[;]?/i)],
                    e = K(g, f, "");
                b.Qc(d, e)
            }
        };
        i.Q = function() {
            return +new Date
        };
        i.T = function(b, a) {
            b.appendChild(a)
        };
        i.hb = function(b, a, c) {
            (c || a.parentNode).insertBefore(b, a)
        };
        i.rc = function(b, a) {
            a = a || b.parentNode;
            a && a.removeChild(b)
        };
        i.df = function(a, b) {
            n(a, function(a) {
                i.rc(a, b)
            })
        };
        i.Yb = function(a) {
            i.df(i.Bb(a, d), a)
        };
        i.Oc = function(a, b) {
            var c = i.Xb(a);
            b & 1 && i.J(a, (i.n(c) - i.n(a)) / 2);
            b & 2 && i.F(a, (i.p(c) - i.p(a)) / 2)
        };
        var T = {
            g: f,
            l: f,
            m: f,
            i: f,
            C: f,
            A: f
        };
        i.cf = function(a) {
            var b = i.Tb();
            r(b, {
                Lb: "block",
                P: i.kb(a),
                g: 0,
                i: 0,
                C: 0,
                A: 0
            });
            var d = i.Nc(a, T);
            i.hb(b, a);
            i.T(b, a);
            var e = i.Nc(a, T),
                c = {};
            n(d, function(b, a) {
                if (b == e[a]) c[a] = b
            });
            r(b, T);
            r(b, c);
            r(a, {
                g: 0,
                i: 0
            });
            return c
        };
        i.He = p;

        function V(d, c, b) {
            var a = d.cloneNode(!c);
            !b && i.Be(a, "id");
            return a
        }
        i.mb = V;
        i.lb = function(e, f) {
            var a = new Image;

            function b(e, d) {
                i.H(a, "load", b);
                i.H(a, "abort", c);
                i.H(a, "error", c);
                f && f(a, d)
            }

            function c(a) {
                b(a, d)
            }
            if (gb() && l < 11.6 || !e) b(!e);
            else {
                i.a(a, "load", b);
                i.a(a, "abort", c);
                i.a(a, "error", c);
                a.src = e
            }
        };
        i.Le = function(d, a, e) {
            var c = d.length + 1;

            function b(b) {
                c--;
                if (a && b && b.src == a.src) a = b;
                !c && e && e(a)
            }
            n(d, function(a) {
                i.lb(a.src, b)
            });
            b()
        };
        i.ue = function(a, g, i, h) {
            if (h) a = V(a);
            var c = U(a, g);
            if (!c.length) c = b.Qe(a, g);
            for (var f = c.length - 1; f > -1; f--) {
                var d = c[f],
                    e = V(i);
                y(e, y(d));
                b.Qc(e, d.style.cssText);
                b.hb(e, d);
                b.rc(d)
            }
            return a
        };

        function Gb(a) {
            var d = this,
                p = "",
                r = ["av", "pv", "ds", "dn"],
                e = [],
                q, m = 0,
                k = 0,
                f = 0;

            function l() {
                F(a, q, (e[f || k & 2 || k] || "") + " " + (e[m] || ""));
                b.gb(a, "pointer-events", f ? "none" : "")
            }

            function c() {
                m = 0;
                l();
                i.H(j, "mouseup", c);
                i.H(j, "touchend", c);
                i.H(j, "touchcancel", c)
            }

            function g(a) {
                if (f) i.Mb(a);
                else {
                    m = 4;
                    l();
                    i.a(j, "mouseup", c);
                    i.a(j, "touchend", c);
                    i.a(j, "touchcancel", c)
                }
            }
            d.vd = function(a) {
                if (a === h) return k;
                k = a & 2 || a & 1;
                l()
            };
            d.Ib = function(a) {
                if (a === h) return !f;
                f = a ? 0 : 3;
                l()
            };
            d.K = a = i.yb(a);
            C(a, "data-jssor-button", "1");
            var o = b.Ge(y(a));
            if (o) p = o.shift();
            n(r, function(a) {
                e.push(p + a)
            });
            q = ab(" ", e);
            e.unshift("");
            i.a(a, "mousedown", g);
            i.a(a, "touchstart", g)
        }
        i.lc = function(a) {
            return new Gb(a)
        };
        i.gb = x;
        i.tc = m("overflow");
        i.F = m("top", 2);
        i.Cd = m("right", 2);
        i.Dd = m("bottom", 2);
        i.J = m("left", 2);
        i.n = m("width", 2);
        i.p = m("height", 2);
        i.Pd = m("marginLeft", 2);
        i.Fd = m("marginTop", 2);
        i.kb = m("position");
        i.wb = m("display");
        i.s = m("zIndex", 1);
        i.ic = function(b, a, c) {
            if (a != h) Fb(b, a, c);
            else return Db(b)
        };
        i.Qc = function(a, b) {
            if (b != h) a.style.cssText = b;
            else return a.style.cssText
        };
        i.Nd = function(b, a) {
            if (a === h) {
                a = x(b, "backgroundImage") || "";
                var c = /\burl\s*\(\s*["']?([^"'\r\n,]+)["']?\s*\)/gi.exec(a) || [];
                return c[1]
            }
            x(b, "backgroundImage", a ? "url('" + a + "')" : "")
        };
        var G;
        i.Md = G = {
            ob: i.ic,
            g: i.F,
            l: i.Cd,
            m: i.Dd,
            i: i.J,
            C: i.n,
            A: i.p,
            P: i.kb,
            Lb: i.wb,
            xb: i.s
        };
        i.Nc = function(c, b) {
            var a = {};
            n(b, function(d, b) {
                if (G[b]) a[b] = G[b](c)
            });
            return a
        };

        function r(g, l) {
            var e = wb(),
                b = M(),
                d = ub(),
                j = N(g);

            function k(b, d, a) {
                var e = b.eb(v(-d / 2, -a / 2)),
                    f = b.eb(v(d / 2, -a / 2)),
                    g = b.eb(v(d / 2, a / 2)),
                    h = b.eb(v(-d / 2, a / 2));
                b.eb(v(300, 300));
                return v(c.min(e.x, f.x, g.x, h.x) + d / 2, c.min(e.y, f.y, g.y, h.y) + a / 2)
            }

            function a(d, a) {
                a = a || {};
                var n = a.V || 0,
                    p = (a.W || 0) % 360,
                    q = (a.S || 0) % 360,
                    u = (a.G || 0) % 360,
                    l = a.z,
                    m = a.v,
                    f = a.uf;
                if (l == h) l = 1;
                if (m == h) m = 1;
                if (f == h) f = 1;
                if (e) {
                    n = 0;
                    p = 0;
                    q = 0;
                    f = 0
                }
                var c = new Cb(a.cb, a.Y, n);
                c.W(p);
                c.S(q);
                c.zd(u);
                c.ve(a.Pb, a.Hb);
                c.zb(l, m, f);
                if (b) {
                    c.nb(a.q, a.o);
                    d.style[j] = c.xd()
                } else if (!X || X < 9) {
                    var o = "",
                        g = {
                            x: 0,
                            y: 0
                        };
                    if (a.N) g = k(c, a.N, a.X);
                    i.Fd(d, g.y);
                    i.Pd(d, g.x);
                    o = c.Kd();
                    var s = d.style.filter,
                        t = new RegExp(/[\s]*progid:DXImageTransform\.Microsoft\.Matrix\([^\)]*\)/g),
                        r = K(s, [t], o);
                    sb(d, r)
                }
            }
            r = function(e, c) {
                c = c || {};
                var j = c.q,
                    k = c.o,
                    g;
                n(G, function(a, b) {
                    g = c[b];
                    g !== h && a(e, g)
                });
                i.xe(e, c.c);
                if (!b) {
                    j != h && i.J(e, (c.Ic || 0) + j);
                    k != h && i.F(e, (c.Gc || 0) + k)
                }
                if (c.Rd)
                    if (d) qb(i.I(f, P, e, c));
                    else a(e, c)
            };
            i.Db = P;
            if (d) i.Db = r;
            if (e) i.Db = a;
            else if (!b) a = P;
            i.D = r;
            r(g, l)
        }
        i.Db = r;
        i.D = r;

        function Cb(j, k, o) {
            var d = this,
                b = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, j || 0, k || 0, o || 0, 1],
                i = c.sin,
                h = c.cos,
                l = c.tan;

            function g(a) {
                return a * c.PI / 180
            }

            function n(a, b) {
                return {
                    x: a,
                    y: b
                }
            }

            function m(c, e, l, m, o, r, t, u, w, z, A, C, E, b, f, k, a, g, i, n, p, q, s, v, x, y, B, D, F, d, h, j) {
                return [c * a + e * p + l * x + m * F, c * g + e * q + l * y + m * d, c * i + e * s + l * B + m * h, c * n + e * v + l * D + m * j, o * a + r * p + t * x + u * F, o * g + r * q + t * y + u * d, o * i + r * s + t * B + u * h, o * n + r * v + t * D + u * j, w * a + z * p + A * x + C * F, w * g + z * q + A * y + C * d, w * i + z * s + A * B + C * h, w * n + z * v + A * D + C * j, E * a + b * p + f * x + k * F, E * g + b * q + f * y + k * d, E * i + b * s + f * B + k * h, E * n + b * v + f * D + k * j]
            }

            function e(c, a) {
                return m.apply(f, (a || b).concat(c))
            }
            d.zb = function(a, c, d) {
                if (a != 1 || c != 1 || d != 1) b = e([a, 0, 0, 0, 0, c, 0, 0, 0, 0, d, 0, 0, 0, 0, 1])
            };
            d.nb = function(a, c, d) {
                b[12] += a || 0;
                b[13] += c || 0;
                b[14] += d || 0
            };
            d.W = function(c) {
                if (c) {
                    a = g(c);
                    var d = h(a),
                        f = i(a);
                    b = e([1, 0, 0, 0, 0, d, f, 0, 0, -f, d, 0, 0, 0, 0, 1])
                }
            };
            d.S = function(c) {
                if (c) {
                    a = g(c);
                    var d = h(a),
                        f = i(a);
                    b = e([d, 0, -f, 0, 0, 1, 0, 0, f, 0, d, 0, 0, 0, 0, 1])
                }
            };
            d.zd = function(c) {
                if (c) {
                    a = g(c);
                    var d = h(a),
                        f = i(a);
                    b = e([d, f, 0, 0, -f, d, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1])
                }
            };
            d.ve = function(a, c) {
                if (a || c) {
                    j = g(a);
                    k = g(c);
                    b = e([1, l(k), 0, 0, l(j), 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1])
                }
            };
            d.eb = function(c) {
                var a = e(b, [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, c.x, c.y, 0, 1]);
                return n(a[12], a[13])
            };
            d.xd = function() {
                return "matrix3d(" + b.join(",") + ")"
            };
            d.Kd = function() {
                return "progid:DXImageTransform.Microsoft.Matrix(M11=" + b[0] + ", M12=" + b[4] + ", M21=" + b[1] + ", M22=" + b[5] + ", SizingMethod='auto expand')"
            }
        }
        new function() {
            var a = this;

            function b(d, g) {
                for (var j = d[0].length, i = d.length, h = g[0].length, f = [], c = 0; c < i; c++)
                    for (var k = f[c] = [], b = 0; b < h; b++) {
                        for (var e = 0, a = 0; a < j; a++) e += d[c][a] * g[a][b];
                        k[b] = e
                    }
                return f
            }
            a.z = function(b, c) {
                return a.Fc(b, c, 0)
            };
            a.v = function(b, c) {
                return a.Fc(b, 0, c)
            };
            a.Fc = function(a, c, d) {
                return b(a, [
                    [c, 0],
                    [0, d]
                ])
            };
            a.eb = function(d, c) {
                var a = b(d, [
                    [c.x],
                    [c.y]
                ]);
                return v(a[0][0], a[1][0])
            }
        };
        var Q = {
            Ic: 0,
            Gc: 0,
            q: 0,
            o: 0,
            vb: 1,
            z: 1,
            v: 1,
            G: 0,
            W: 0,
            S: 0,
            cb: 0,
            Y: 0,
            V: 0,
            Pb: 0,
            Hb: 0
        };
        i.oe = function(c, d) {
            var a = c || {};
            if (c)
                if (b.Sc(c)) a = {
                    E: a
                };
                else if (b.Sc(c.c)) a.c = {
                    E: c.c
                };
            a.E = a.E || d;
            if (a.c) a.c.E = a.c.E || d;
            return a
        };
        i.ne = function(n, j, s, t, B, C, o) {
            var a = j;
            if (n) {
                a = {};
                for (var i in j) {
                    var D = C[i] || 1,
                        z = B[i] || [0, 1],
                        g = (s - z[0]) / z[1];
                    g = c.min(c.max(g, 0), 1);
                    g = g * D;
                    var x = c.floor(g);
                    if (g != x) g -= x;
                    var k = t.E || e.ec,
                        m, E = n[i],
                        p = j[i];
                    if (b.Ne(p)) {
                        k = t[i] || k;
                        var A = k(g);
                        m = E + p * A
                    } else {
                        m = b.db({
                            Nb: {}
                        }, n[i]);
                        var y = t[i] || {};
                        b.f(p.Nb || p, function(d, a) {
                            k = y[a] || y.E || k;
                            var c = k(g),
                                b = d * c;
                            m.Nb[a] = b;
                            m[a] += b
                        })
                    }
                    a[i] = m
                }
                var w = b.f(j, function(b, a) {
                    return Q[a] != h
                });
                w && b.f(Q, function(c, b) {
                    if (a[b] == h && n[b] !== h) a[b] = n[b]
                });
                if (w) {
                    if (a.vb) a.z = a.v = a.vb;
                    a.N = o.N;
                    a.X = o.X;
                    if (q() && l >= 11 && (j.q || j.o) && s != 0 && s != 1) a.G = a.G || 1e-8;
                    a.Rd = d
                }
            }
            if (j.c && o.nb) {
                var r = a.c.Nb,
                    v = (r.g || 0) + (r.m || 0),
                    u = (r.i || 0) + (r.l || 0);
                a.i = (a.i || 0) + u;
                a.g = (a.g || 0) + v;
                a.c.i -= u;
                a.c.l -= u;
                a.c.g -= v;
                a.c.m -= v
            }
            if (a.c && !a.c.g && !a.c.i && !a.c.o && !a.c.q && a.c.l == o.N && a.c.m == o.X) a.c = f;
            return a
        }
    };

    function o() {
        var a = this,
            d = [];

        function i(a, b) {
            d.push({
                jc: a,
                uc: b
            })
        }

        function h(a, c) {
            b.f(d, function(b, e) {
                b.jc == a && b.uc === c && d.splice(e, 1)
            })
        }
        a.ub = a.addEventListener = i;
        a.removeEventListener = h;
        a.j = function(a) {
            var c = [].slice.call(arguments, 1);
            b.f(d, function(b) {
                b.jc == a && b.uc.apply(g, c)
            })
        }
    }
    var l = function(A, E, h, J, M, L) {
        A = A || 0;
        var a = this,
            p, m, n, s, C = 0,
            G, H, F, D, z = 0,
            i = 0,
            l = 0,
            y, j, e, f, o, x, u = [],
            w;

        function O(a) {
            e += a;
            f += a;
            j += a;
            i += a;
            l += a;
            z += a
        }

        function r(p) {
            var g = p;
            if (o)
                if (!x && (g >= f || g < e) || x && g >= e) g = ((g - e) % o + o) % o + e;
            if (!y || s || i != g) {
                var k = c.min(g, f);
                k = c.max(k, e);
                if (!y || s || k != l) {
                    if (L) {
                        var m = (k - j) / (E || 1);
                        if (h.je) m = 1 - m;
                        var n = b.ne(M, L, m, G, F, H, h);
                        if (w) b.f(n, function(b, a) {
                            w[a] && w[a](J, b)
                        });
                        else b.D(J, n)
                    }
                    a.cc(l - j, k - j);
                    var r = l,
                        q = l = k;
                    b.f(u, function(b, c) {
                        var a = !y && x || g <= i ? u[u.length - c - 1] : b;
                        a.B(l - z)
                    });
                    i = g;
                    y = d;
                    a.Jb(r, q)
                }
            }
        }

        function B(a, b, d) {
            b && a.hc(f);
            if (!d) {
                e = c.min(e, a.Ec() + z);
                f = c.max(f, a.kc() + z)
            }
            u.push(a)
        }
        var v = g.requestAnimationFrame || g.webkitRequestAnimationFrame || g.mozRequestAnimationFrame || g.msRequestAnimationFrame;
        if (b.Fe() && b.Xc() < 7 || !v) v = function(a) {
            b.Uc(a, h.Dc)
        };

        function I() {
            if (p) {
                var d = b.Q(),
                    e = c.min(d - C, h.Rc),
                    a = i + e * n;
                C = d;
                if (a * n >= m * n) a = m;
                r(a);
                if (!s && a * n >= m * n) K(D);
                else v(I)
            }
        }

        function q(g, h, j) {
            if (!p) {
                p = d;
                s = j;
                D = h;
                g = c.max(g, e);
                g = c.min(g, f);
                m = g;
                n = m < i ? -1 : 1;
                a.Bc();
                C = b.Q();
                v(I)
            }
        }

        function K(b) {
            if (p) {
                s = p = D = k;
                a.xc();
                b && b()
            }
        }
        a.vc = function(a, b, c) {
            q(a ? i + a : f, b, c)
        };
        a.Ac = q;
        a.ab = K;
        a.ge = function(a) {
            q(a)
        };
        a.R = function() {
            return i
        };
        a.wc = function() {
            return m
        };
        a.bb = function() {
            return l
        };
        a.B = r;
        a.nb = function(a) {
            r(i + a)
        };
        a.yc = function() {
            return p
        };
        a.de = function(a) {
            o = a
        };
        a.hc = O;
        a.zc = function(a, b) {
            B(a, 0, b)
        };
        a.mc = function(a) {
            B(a, 1)
        };
        a.Ec = function() {
            return e
        };
        a.kc = function() {
            return f
        };
        a.Jb = a.Bc = a.xc = a.cc = b.Mc;
        a.oc = b.Q();
        h = b.db({
            Dc: 16,
            Rc: 50
        }, h);
        o = h.pc;
        x = h.Zd;
        w = h.Yd;
        e = j = A;
        f = A + E;
        H = h.Xd || {};
        F = h.Wd || {};
        G = b.oe(h.qb)
    };
    var m = {
            Gb: "data-scale",
            qc: "data-scale-ratio",
            pb: "data-autocenter"
        },
        n = new function() {
            var a = this;
            a.fb = function(c, a, e, d) {
                (d || !b.u(c, a)) && b.u(c, a, e)
            };
            a.sc = function(a) {
                var c = b.jb(a, m.pb);
                b.Oc(a, c)
            }
        };
    new(function() {});
    var p = {
            dc: 1
        },
        s = function(a, E) {
            var g = this;
            o.call(g);
            a = b.yb(a);
            var u, C, B, t, l = 0,
                e, q, j, y, z, i, h, s, r, D = [],
                A = [];

            function x(a) {
                a != -1 && A[a].vd(a == l)
            }

            function v(a) {
                g.j(p.dc, a * q)
            }
            g.K = a;
            g.Ub = function(a) {
                if (a != t) {
                    var d = l,
                        b = c.floor(a / q);
                    l = b;
                    t = a;
                    x(d);
                    x(b)
                }
            };
            g.Sb = function(c) {
                b.M(a, c)
            };
            var w;
            g.Wb = function(x) {
                if (!w) {
                    u = c.ceil(x / q);
                    l = 0;
                    var n = s + y,
                        o = r + z,
                        m = c.ceil(u / j) - 1;
                    C = s + n * (!i ? m : j - 1);
                    B = r + o * (i ? m : j - 1);
                    b.n(a, C);
                    b.p(a, B);
                    for (var g = 0; g < u; g++) {
                        var t = b.Me();
                        b.jf(t, g + 1);
                        var k = b.ue(h, "numbertemplate", t, d);
                        b.kb(k, "absolute");
                        var p = g % (m + 1);
                        b.J(k, !i ? n * p : g % j * n);
                        b.F(k, i ? o * p : c.floor(g / (m + 1)) * o);
                        b.T(a, k);
                        D[g] = k;
                        e.Qb & 1 && b.a(k, "click", b.I(f, v, g));
                        e.Qb & 2 && b.a(k, "mouseenter", b.I(f, v, g));
                        A[g] = b.lc(k)
                    }
                    w = d
                }
            };
            g.Ob = e = b.db({
                Lc: 10,
                Kc: 10,
                Hc: 1,
                Qb: 1
            }, E);
            h = b.Vb(a, "prototype");
            s = b.n(h);
            r = b.p(h);
            b.rc(h, a);
            q = e.Cc || 1;
            j = e.Tc || 1;
            y = e.Lc;
            z = e.Kc;
            i = e.Hc - 1;
            e.zb == k && n.fb(a, m.Gb, 1);
            e.U && n.fb(a, m.pb, e.U);
            n.sc(a)
        },
        t = function(a, e, i, y, x, w) {
            var c = this;
            o.call(c);
            var j, h, g, l;
            b.n(a);
            b.p(a);
            var s, r;

            function q(a) {
                c.j(p.dc, a, d)
            }

            function v(c) {
                b.M(a, c);
                b.M(e, c)
            }

            function u() {
                s.Ib(i.Ab || !j.Ve(h));
                r.Ib(i.Ab || !j.Ue(h))
            }
            c.Ub = function(c, a, b) {
                h = a;
                !b && u()
            };
            c.Sb = v;
            var t;
            c.Wb = function() {
                h = 0;
                if (!t) {
                    b.a(a, "click", b.I(f, q, -l));
                    b.a(e, "click", b.I(f, q, l));
                    s = b.lc(a);
                    r = b.lc(e);
                    t = d
                }
            };
            c.Ob = g = b.db({
                Cc: 1
            }, i);
            l = g.Cc;
            j = w;
            if (g.zb == k) {
                n.fb(a, m.Gb, 1);
                n.fb(e, m.Gb, 1)
            }
            if (g.U) {
                n.fb(a, m.pb, g.U);
                n.fb(e, m.pb, g.U)
            }
            n.sc(a);
            n.sc(e)
        };

    function r(e, d, c) {
        var a = this;
        l.call(a, 0, c);
        a.hd = b.Mc;
        a.gd = 0;
        a.fd = c
    }
    var i = (g.module || {}).exports = function() {
        var a = this;
        b.Oe(a, o);
        var Ob = "data-jssor-slider",
            bc = "data-jssor-thumb",
            v, n, ab, ob, eb, yb, db, W, K, R, Db, Vb = 1,
            nc = 1,
            dc = 1,
            ec = {},
            y, X, Gb, Jb, Ib, pb, Ab, zb, kb, s = -1,
            Rb, q, O, M, G, vb, wb, w, S, L, bb, z, Y, ub, gb = [],
            jc, lc, fc, Wb, Hc, u, lb, J, hc, tb, Pb, ic, Q, Lb = 0,
            E = 0,
            P = Number.MAX_VALUE,
            H = Number.MIN_VALUE,
            kc, D, mb, U, N = 1,
            cb, B, fb, Sb = 0,
            Tb = 0,
            T, qb, rb, nb, x, ib, A, Hb, hb = [],
            Fb = b.ze(),
            sb = Fb.Te,
            C = [],
            F, V, I, Nb, ac, Z;

        function wc(e, k, o) {
            var l = this,
                h = {
                    g: 2,
                    l: 1,
                    m: 2,
                    i: 1
                },
                n = {
                    g: "top",
                    l: "right",
                    m: "bottom",
                    i: "left"
                },
                g, a, f, i, j = {};
            l.K = e;
            l.Eb = function(q, p, t) {
                var l, s = q,
                    r = p;
                if (!f) {
                    f = b.cf(e);
                    g = e.parentNode;
                    i = {
                        zb: b.jb(e, m.Gb, 1),
                        U: b.jb(e, m.pb)
                    };
                    b.f(n, function(c, a) {
                        j[a] = b.jb(e, "data-scale-" + c, 1)
                    });
                    a = e;
                    if (k) {
                        a = b.mb(g, d);
                        b.D(a, {
                            g: 0,
                            i: 0
                        });
                        b.T(a, e);
                        b.T(g, a)
                    }
                }
                if (o) {
                    l = c.max(q, p);
                    if (k)
                        if (t > 0 && t < 1) {
                            var v = c.min(q, p);
                            l = c.min(l / v, 1 / (1 - t)) * v
                        }
                } else s = r = l = c.pow(K < R ? p : q, i.zb);
                b.ef(a, l);
                b.u(a, m.qc, l);
                b.n(g, f.C * s);
                b.p(g, f.A * r);
                var u = b.id() && b.Wc() < 9 || b.Wc() < 10 && b.De() ? l : 1,
                    w = (s - u) * f.C / 2,
                    x = (r - u) * f.A / 2;
                b.J(a, w);
                b.F(a, x);
                b.f(f, function(d, a) {
                    if (h[a] && d) {
                        var e = (h[a] & 1) * c.pow(q, j[a]) * d + (h[a] & 2) * c.pow(p, j[a]) * d / 2;
                        b.Md[a](g, e)
                    }
                });
                b.Oc(g, i.U)
            }
        }

        function Gc() {
            var b = this;
            l.call(b, -1e8, 2e8);
            b.af = function() {
                var a = b.bb();
                a = t(a);
                var d = c.round(a),
                    g = d,
                    f = a - c.floor(a),
                    e = cc(a);
                return {
                    Z: g,
                    Ze: d,
                    P: f,
                    Lb: a,
                    We: e
                }
            };
            b.Jb = function(e, b) {
                var g = t(b);
                if (c.abs(b - e) > 1e-5) {
                    var f = c.floor(b);
                    if (f != b && b > e && (D & 1 || b > E)) f++;
                    mc(f, g, d)
                }
                a.j(i.Xe, g, t(e), b, e)
            }
        }

        function Fc() {
            var a = this;
            l.call(a, 0, 0, {
                pc: q
            });
            b.f(C, function(b) {
                D & 1 && b.de(q);
                a.mc(b);
                b.hc(Q / w)
            })
        }

        function Ec() {
            var a = this,
                b = Hb.K;
            l.call(a, -1, 2, {
                qb: e.ec,
                Yd: {
                    P: Ub
                },
                pc: q
            }, b, {
                P: 1
            }, {
                P: -2
            });
            a.Rb = b
        }

        function xc(o, m) {
            var b = this,
                e, g, h, j, c;
            l.call(b, -1e8, 2e8, {
                Rc: 100
            });
            b.Bc = function() {
                cb = d;
                fb = f;
                a.j(i.gf, t(x.R()), x.R())
            };
            b.xc = function() {
                cb = k;
                j = k;
                var b = x.af();
                a.j(i.Ye, t(x.R()), x.R());
                if (!B) {
                    Ic(b.Ze, s);
                    (!b.P || b.We) && mc(s, b.Lb)
                }
            };
            b.Jb = function(f, d) {
                var a;
                if (j) a = c;
                else {
                    a = g;
                    if (h) {
                        var b = d / h;
                        a = n.sd(b) * (g - e) + e
                    }
                }
                x.B(a)
            };
            b.ac = function(a, d, c, f) {
                e = a;
                g = d;
                h = c;
                x.B(a);
                b.B(0);
                b.Ac(c, f)
            };
            b.ff = function(a) {
                j = d;
                c = a;
                b.vc(a, f, d)
            };
            b.hf = function(a) {
                c = a
            };
            x = new Gc;
            x.zc(o);
            x.zc(m)
        }

        function yc() {
            var c = this,
                a = pc();
            b.s(a, 0);
            b.gb(a, "pointerEvents", "none");
            c.K = a;
            c.Cb = function() {
                b.L(a);
                b.Yb(a)
            }
        }

        function Dc(m, g) {
            var e = this,
                r, E, v, j, x = [],
                J, y, Q, z, N, H, B, h, w, p;
            l.call(e, -S, S + 1, {});

            function G(a) {
                r && r.hd();
                P(m, a, 0);
                H = d;
                r = new eb.O(m, eb, b.jb(m, "idle", hc), !u);
                r.B(0)
            }

            function V() {
                r.oc < eb.oc && G()
            }

            function K(p, r, o) {
                if (!z) {
                    z = d;
                    if (j && o) {
                        var f = o.width,
                            c = o.height,
                            m = f,
                            l = c;
                        if (f && c && n.ib) {
                            if (n.ib & 3 && (!(n.ib & 4) || f > O || c > M)) {
                                var h = k,
                                    q = O / M * c / f;
                                if (n.ib & 1) h = q > 1;
                                else if (n.ib & 2) h = q < 1;
                                m = h ? f * M / c : O;
                                l = h ? M : c * O / f
                            }
                            b.n(j, m);
                            b.p(j, l);
                            b.F(j, (M - l) / 2);
                            b.J(j, (O - m) / 2)
                        }
                        b.kb(j, "absolute");
                        a.j(i.we, g)
                    }
                }
                b.L(r);
                p && p(e)
            }

            function T(f, b, c, d) {
                if (d == fb && s == g && u)
                    if (!Hc) {
                        var a = t(f);
                        F.kf(a, g, b, e, c, M / O);
                        b.pe();
                        ib.hc(a - ib.Ec() - 1);
                        ib.B(a);
                        A.ac(a, a, 0)
                    }
            }

            function Y(b) {
                if (b == fb && s == g) {
                    if (!h) {
                        var a = f;
                        if (F)
                            if (F.Z == g) a = F.ye();
                            else F.Cb();
                        V();
                        h = new Cc(m, g, a, r);
                        h.pd(p)
                    }!h.yc() && h.nc()
                }
            }

            function I(a, d, k) {
                if (a == g) {
                    if (a != d) C[d] && C[d].rd();
                    else !k && h && h.Ad();
                    p && p.Ib();
                    var l = fb = b.Q();
                    e.lb(b.I(f, Y, l))
                } else {
                    var j = c.min(g, a),
                        i = c.max(g, a),
                        o = c.min(i - j, j + q - i),
                        m = S + n.Bd - 1;
                    (!N || o <= m) && e.lb()
                }
            }

            function Z() {
                if (s == g && h) {
                    h.ab();
                    p && p.Od();
                    p && p.Hd();
                    h.ud()
                }
            }

            function ab() {
                s == g && h && h.ab()
            }

            function W(b) {
                !U && a.j(i.Ld, g, b)
            }

            function L() {
                p = w.pInstance;
                h && h.pd(p)
            }
            e.lb = function(e, c) {
                c = c || v;
                if (x.length && !z) {
                    b.M(c);
                    if (!Q) {
                        Q = d;
                        a.j(i.Jd, g);
                        b.f(x, function(a) {
                            if (!b.u(a, "src")) {
                                a.src = b.k(a, "src2") || "";
                                b.wb(a, a["display-origin"])
                            }
                        })
                    }
                    b.Le(x, j, b.I(f, K, e, c))
                } else K(e, c)
            };
            e.Ed = function() {
                if (q == 1) {
                    e.rd();
                    I(g, g)
                } else {
                    var a;
                    if (F) a = F.Id(q);
                    if (a) {
                        var h = fb = b.Q(),
                            c = g + lb,
                            d = C[t(c)];
                        return d.lb(b.I(f, T, c, d, a, h), v)
                    } else Cb(lb)
                }
            };
            e.bc = function() {
                I(g, g, d)
            };
            e.rd = function() {
                p && p.Od();
                p && p.Hd();
                e.bd();
                h && h.yd();
                h = f;
                G()
            };
            e.pe = function() {
                b.L(m)
            };
            e.bd = function() {
                b.M(m)
            };
            e.wd = function() {
                p && p.Ib()
            };

            function P(a, f, c, h) {
                if (b.u(a, Ob)) return;
                if (!H) {
                    if (a.tagName == "IMG") {
                        x.push(a);
                        if (!b.u(a, "src")) {
                            N = d;
                            a["display-origin"] = b.wb(a);
                            b.L(a)
                        }
                    }
                    var e = b.Nd(a);
                    if (e) {
                        var g = new Image;
                        b.k(g, "src2", e);
                        x.push(g)
                    }
                    c && b.s(a, (b.s(a) || 0) + 1)
                }
                var i = b.Bb(a);
                b.f(i, function(a) {
                    var e = a.tagName,
                        g = b.k(a, "u");
                    if (g == "player" && !w) {
                        w = a;
                        if (w.pInstance) L();
                        else b.a(w, "dataavailable", L)
                    }
                    if (g == "caption") {
                        if (f) {
                            b.Ke(a, b.k(a, "to"));
                            b.Je(a, b.k(a, "bf"));
                            B && b.k(a, "3d") && b.Ce(a, "preserve-3d")
                        }
                    } else if (!H && !c && !j) {
                        if (e == "A") {
                            if (b.k(a, "u") == "image") j = b.Pe(a, "IMG");
                            else j = b.Vb(a, "image", d);
                            if (j) {
                                J = a;
                                b.D(J, kb);
                                y = b.mb(J, d);
                                b.ic(y, 0);
                                b.gb(y, "backgroundColor", "#000")
                            }
                        } else if (e == "IMG" && b.k(a, "u") == "image") j = a;
                        if (j) {
                            j.border = 0;
                            b.D(j, kb)
                        }
                    }
                    P(a, f, c + 1, h)
                })
            }
            e.cc = function(c, b) {
                var a = S - b;
                Ub(E, a)
            };
            e.Z = g;
            o.call(e);
            B = b.k(m, "p");
            b.Ae(m, B);
            b.mf(m, b.k(m, "po"));
            var D = b.Vb(m, "thumb", d);
            if (D) {
                b.mb(D);
                b.L(D)
            }
            b.M(m);
            v = b.mb(X);
            b.s(v, 1e3);
            b.a(m, "click", W);
            G(d);
            e.lf = j;
            e.jd = y;
            e.Rb = E = m;
            b.T(E, v);
            a.ub(203, I);
            a.ub(28, ab);
            a.ub(24, Z)
        }

        function Cc(z, g, p, q) {
            var c = this,
                n = 0,
                v = 0,
                h, j, f, e, m, t, r, o = C[g];
            l.call(c, 0, 0);

            function w() {
                b.Yb(V);
                Wb && m && o.jd && b.T(V, o.jd);
                b.M(V, !m && o.lf)
            }

            function x() {
                c.nc()
            }

            function y(a) {
                r = a;
                c.ab();
                c.nc()
            }
            c.nc = function() {
                var b = c.bb();
                if (!B && !cb && !r && s == g) {
                    if (!b) {
                        if (h && !m) {
                            m = d;
                            c.ud(d);
                            a.j(i.fe, g, n, v, h, e)
                        }
                        w()
                    }
                    var k, p = i.kd;
                    if (b != e)
                        if (b == f) k = e;
                        else if (b == j) k = f;
                        else if (!b) k = j;
                        else k = c.wc();
                    a.j(p, g, b, n, j, f, e);
                    var l = u && (!J || N);
                    if (b == e)(f != e && !(J & 12) || l) && o.Ed();
                    else(l || b != f) && c.Ac(k, x)
                }
            };
            c.Ad = function() {
                f == e && f == c.bb() && c.B(j)
            };
            c.yd = function() {
                F && F.Z == g && F.Cb();
                var b = c.bb();
                b < e && a.j(i.kd, g, -b - 1, n, j, f, e)
            };
            c.ud = function(a) {
                p && b.tc(bb, a && p.Jc.rf ? "" : "hidden")
            };
            c.cc = function(c, b) {
                if (m && b >= h) {
                    m = k;
                    w();
                    o.bd();
                    F.Cb();
                    a.j(i.se, g, n, v, h, e)
                }
                a.j(i.re, g, b, n, j, f, e)
            };
            c.pd = function(a) {
                if (a && !t) {
                    t = a;
                    a.ub($JssorPlayer$.Ud, y)
                }
            };
            p && c.mc(p);
            h = c.kc();
            c.mc(q);
            j = h + q.gd;
            e = c.kc();
            f = u ? h + q.fd : e
        }

        function Qb(a, c, d) {
            b.J(a, c);
            b.F(a, d)
        }

        function Ub(c, b) {
            var a = z > 0 ? z : ab,
                d = (vb * b + Lb) * (a & 1),
                e = (wb * b + Lb) * (a >> 1 & 1);
            Qb(c, d, e)
        }

        function Mb(a) {
            if (!(D & 1)) a = c.min(P, c.max(a, H));
            return a
        }

        function cc(a) {
            return !(D & 1) && (a - H < .0001 || P - a < .0001)
        }

        function gc() {
            Nb = cb;
            ac = A.wc();
            I = x.R()
        }

        function Yb() {
            gc();
            if (B || !N && J & 12) {
                A.ab();
                a.j(i.qe)
            }
        }

        function Xb(g) {
            if (!B && (N || !(J & 12)) && !A.yc()) {
                var b = x.R(),
                    a = I,
                    f = 0;
                if (g && c.abs(T) >= n.Yc) {
                    a = b;
                    f = rb
                }
                if (cc(b)) {
                    if (!g || U) a = c.round(a)
                } else a = c.ceil(a);
                a = Mb(a + f);
                if (!(D & 1)) {
                    if (P - a < .5) a = P;
                    if (a - H < .5) a = H
                }
                var d = c.abs(a - b);
                if (d < 1 && n.sd != e.ec) d = 1 - c.pow(1 - d, 5);
                if (!U && Nb) A.ge(ac);
                else if (b == a) {
                    Rb.wd();
                    Rb.bc()
                } else A.ac(b, a, d * tb)
            }
        }

        function Zb(a) {
            !b.tb(b.Zb(a), "nodrag") && b.Mb(a)
        }

        function Ac(a) {
            qc(a, 1)
        }

        function qc(c, g) {
            var e = b.Zb(c);
            ub = k;
            var l = b.tb(e, "1", bc);
            if ((!l || l === v) && !Y && (!g || c.touches.length == 1)) {
                ub = b.tb(e, "nodrag") || !mb || !Bc();
                var n = b.tb(e, h, m.qc);
                if (n) dc = b.u(n, m.qc);
                if (g) {
                    var p = c.touches[0];
                    Sb = p.clientX;
                    Tb = p.clientY
                } else {
                    var o = b.od(c);
                    Sb = o.x;
                    Tb = o.y
                }
                B = d;
                fb = f;
                b.a(j, g ? "touchmove" : "mousemove", Eb);
                b.Q();
                U = 0;
                Yb();
                if (!Nb) z = 0;
                T = 0;
                qb = 0;
                rb = 0;
                a.j(i.Qd, t(I), I, c)
            }
        }

        function Eb(g) {
            if (B) {
                var a;
                if (g.type != "mousemove")
                    if (g.touches.length == 1) {
                        var o = g.touches[0];
                        a = {
                            x: o.clientX,
                            y: o.clientY
                        }
                    } else jb();
                else a = b.od(g);
                if (a) {
                    var e = a.x - Sb,
                        f = a.y - Tb;
                    if (z || c.abs(e) > 1.5 || c.abs(f) > 1.5) {
                        if (c.floor(I) != I) z = z || ab & Y;
                        if ((e || f) && !z) {
                            if (Y == 3)
                                if (c.abs(f) > c.abs(e)) z = 2;
                                else z = 1;
                            else z = Y;
                            if (sb && z == 1 && c.abs(f) > c.abs(e) * 2.4) ub = d
                        }
                        var n = f,
                            i = wb;
                        if (z == 1) {
                            n = e;
                            i = vb
                        }
                        if (T - qb < -1.5) rb = 0;
                        else if (T - qb > 1.5) rb = -1;
                        qb = T;
                        T = n;
                        Z = I - T / i / dc;
                        if (!(D & 1)) {
                            var l = 0,
                                j = [-I + E, 0, I - q + L - G / w - E];
                            b.f(j, function(b, d) {
                                if (b > 0) {
                                    var a = c.pow(b, 1 / 1.6);
                                    a = c.tan(a * c.PI / 2);
                                    l = (a - b) * (d - 1)
                                }
                            });
                            var h = l + Z,
                                m = k;
                            j = [-h + E, 0, h - q + L + G / w - E];
                            b.f(j, function(a, b) {
                                if (a > 0) {
                                    a = c.min(a, i);
                                    a = c.atan(a) * 2 / c.PI;
                                    a = c.pow(a, 1.6);
                                    Z = a * (b - 1) + E;
                                    if (b) Z += q - L - G / w;
                                    m = d
                                }
                            });
                            if (!m) Z = h
                        }
                        if (T && z && !ub) {
                            b.Mb(g);
                            if (!cb) A.ff(Z);
                            else A.hf(Z)
                        }
                    }
                }
            }
        }

        function jb() {
            zc();
            if (B) {
                U = T;
                b.Q();
                b.H(j, "mousemove", Eb);
                b.H(j, "touchmove", Eb);
                U && u & 8 && (u = 0);
                A.ab();
                B = k;
                var c = x.R();
                a.j(i.me, t(c), c, t(I), I);
                J & 12 && gc();
                Xb(d)
            }
        }

        function vc(c) {
            var a = b.Zb(c),
                d = b.tb(a, "1", Ob);
            if (v === d)
                if (U) {
                    b.bf(c);
                    while (a && v !== a) {
                        (a.tagName == "A" || b.u(a, "data-jssor-button")) && b.Mb(c);
                        a = a.parentNode
                    }
                } else u & 4 && (u = 0)
        }

        function rc(d) {
            if (d != s) {
                var b = nb.bb(),
                    a = Mb(d),
                    e = c.round(t(a));
                if (b - a < .5) a = b;
                C[s];
                s = e;
                Rb = C[s];
                x.B(a)
            }
        }

        function Ic(b, c) {
            z = 0;
            rc(b);
            if (u & 2 && (lb > 0 && s == q - 1 || lb < 0 && !s)) u = 0;
            a.j(i.le, s, c)
        }

        function mc(a, d, e) {
            if (!(D & 1)) {
                a = c.max(0, a);
                a = c.min(a, q - L + E);
                a = c.round(a)
            }
            a = t(a);
            b.f(gb, function(b) {
                b.Ub(a, d, e)
            })
        }

        function Bc() {
            var b = i.ad || 0,
                a = mb;
            i.ad |= a;
            return Y = a & ~b
        }

        function zc() {
            if (Y) {
                i.ad &= ~mb;
                Y = 0
            }
        }

        function pc() {
            var a = b.Tb();
            b.D(a, kb);
            return a
        }

        function t(b, a) {
            a = a || q || 1;
            return (b % a + a) % a
        }

        function Kb(c, a, b) {
            u & 8 && (u = 0);
            xb(c, tb, a, b)
        }

        function Bb() {
            b.f(gb, function(a) {
                a.Sb(a.Ob.sf <= N)
            })
        }

        function tc() {
            if (!N) {
                N = 1;
                Bb();
                if (!B) {
                    J & 12 && Xb();
                    J & 3 && C[s] && C[s].bc()
                }
            }
            a.j(i.ke)
        }

        function sc() {
            if (N) {
                N = 0;
                Bb();
                B || !(J & 12) || Yb()
            }
            a.j(i.ie)
        }

        function uc() {
            b.f(hb, function(a) {
                b.D(a, kb);
                b.tc(a, "hidden");
                b.L(a)
            });
            b.D(X, kb)
        }

        function Cb(b, a) {
            xb(b, a, d)
        }

        function xb(l, f, m, o) {
            if (!B && (N || !(J & 12)) || n.md) {
                cb = d;
                B = k;
                A.ab();
                if (f == h) f = tb;
                var b = t(nb.bb()),
                    e = l;
                if (m) {
                    e = b + l;
                    e = c.round(e)
                }
                var a = e;
                if (!(D & 1)) {
                    if (o) a = t(a);
                    else if (D & 2 && (a < 0 && c.abs(b - H) < .0001 || a > q - L && c.abs(b - P) < .0001)) a = a < 0 ? P : H;
                    a = Mb(a);
                    if (P - a < .5) a = P;
                    if (a - H < .5) a = H
                }
                var j = (a - b) % q;
                a = b + j;
                var g = b == a ? 0 : f * c.abs(j),
                    i = 1;
                if (S > 1) i = (ab & 1 ? Ab : zb) / w;
                g = c.min(g, f * i * 1.5);
                A.ac(b, a, g || 1)
            }
        }
        a.sb = function(a) {
            if (a == h) return u;
            if (a != u) {
                u = a;
                u && C[s] && C[s].bc()
            }
        };
        a.N = function() {
            return K
        };
        a.X = function() {
            return R
        };
        a.he = function(b) {
            if (b == h) return Db || K;
            a.Eb(b, b / K * R)
        };
        a.Eb = function(c, a, d) {
            b.n(v, c);
            b.p(v, a);
            Vb = c / K;
            nc = a / R;
            b.f(ec, function(a) {
                a.Eb(Vb, nc, d)
            });
            if (!Db) {
                b.hb(bb, y);
                b.F(bb, 0);
                b.J(bb, 0)
            }
            Db = c
        };
        a.Ve = function(a) {
            return c.abs(a - H) < .0001
        };
        a.Ue = function(a) {
            return c.abs(a - P) < .0001
        };
        a.vc = function() {
            a.sb(u || 1)
        };
        a.Kb = function(B, m) {
            a.K = v = b.yb(B);
            K = b.n(v);
            R = b.p(v);
            n = b.db({
                ib: 0,
                Bd: 1,
                rb: 1,
                fc: 0,
                sb: 0,
                Ab: 1,
                gc: d,
                md: d,
                ee: 1,
                td: 3e3,
                dd: 1,
                Zc: 500,
                sd: e.te,
                Yc: 20,
                Vc: 0,
                ce: 1,
                ld: 1,
                ed: 1
            }, m);
            n.gc = n.gc && b.Ie();
            if (n.be != h) n.td = n.be;
            if (n.ae != h) n.Fb = n.ae;
            if (n.Vd != h) n.qd = n.Vd;
            ab = n.ld & 3;
            ob = n.nf;
            eb = b.db({
                O: r
            }, n.pf);
            yb = n.Td;
            db = n.Sd;
            W = n.of;
            !n.ce;
            var o = b.Bb(v);
            b.f(o, function(a, d) {
                var c = b.k(a, "u");
                if (c == "loading") X = a;
                else {
                    if (c == "slides") y = a;
                    if (c == "navigator") Gb = a;
                    if (c == "arrowleft") Jb = a;
                    if (c == "arrowright") Ib = a;
                    if (c == "thumbnavigator") pb = a;
                    if (a.tagName != "STYLE" && a.tagName != "SCRIPT") ec[c || d] = new wc(a, c == "slides", b.Pc(["slides", "thumbnavigator"])[c])
                }
            });
            X = X || b.Tb(j);
            Ab = b.n(y);
            zb = b.p(y);
            O = n.cd || Ab;
            M = n.Re || zb;
            kb = {
                C: O,
                A: M,
                g: 0,
                i: 0,
                Lb: "block",
                P: "absolute"
            };
            G = n.Vc;
            vb = O + G;
            wb = M + G;
            w = ab & 1 ? vb : wb;
            var i = ab & 1 ? Ab : zb;
            lb = n.ee;
            J = n.dd;
            hc = n.td;
            tb = n.Zc;
            Hb = new yc;
            if (n.gc && (!b.Ee() || sb)) Qb = function(a, c, d) {
                b.Db(a, {
                    cb: c,
                    Y: d
                })
            };
            u = n.sb & 63;
            a.Ob = m;
            b.u(v, Ob, "1");
            b.s(y, b.s(y) || 0);
            b.kb(y, "absolute");
            bb = b.mb(y, d);
            b.hb(bb, y);
            ib = new Ec;
            b.T(bb, ib.Rb);
            b.tc(y, "hidden");
            J &= sb ? 10 : 5;
            var s = b.Bb(y),
                I = b.Pc(["DIV", "A", "LI"]);
            b.f(s, function(a) {
                I[a.tagName.toUpperCase()] && !b.k(a, "u") && hb.push(a);
                b.s(a, (b.s(a) || 0) + 1)
            });
            V = pc();
            b.gb(V, "backgroundColor", "#000");
            b.ic(V, 0);
            b.s(V, 0);
            b.hb(V, y.firstChild, y);
            q = hb.length;
            if (q) {
                uc();
                Q = n.qd;
                if (Q == h) Q = (i - w + G) / 2;
                L = i / w;
                S = c.min(q, n.Fb || q, c.ceil(L));
                kc = S < q;
                D = kc ? n.Ab : 0;
                if (q * w - G <= i) {
                    L = q - G / w;
                    Q = (i - w + G) / 2;
                    Lb = (i - w * q + G) / 2
                }
                if (ob) {
                    Wb = ob.tf;
                    Pb = ob.O;
                    ic = !Q && S == 1 && q > 1 && Pb && (!b.id() || b.Xc() >= 9)
                }
                if (!(D & 1)) {
                    E = Q / w;
                    if (E > q - 1) {
                        E = q - 1;
                        Q = E * w
                    }
                    H = E;
                    P = H + q - L - G / w
                }
                mb = (S > 1 || Q ? ab : -1) & n.ed;
                Fb.nd && b.gb(y, Fb.nd, ([f, "pan-y", "pan-x", "none"])[mb] || "");
                if (ic) F = new Pb(Hb, O, M, ob, sb, Qb);
                for (var k = 0; k < hb.length; k++) {
                    var x = hb[k],
                        z = new Dc(x, k);
                    C.push(z)
                }
                b.L(X);
                nb = new Fc;
                A = new xc(nb, ib);
                b.a(v, "click", vc, d);
                b.a(v, "mouseleave", tc);
                b.a(v, "mouseenter", sc);
                b.a(v, "mousedown", qc);
                b.a(v, "touchstart", Ac);
                b.a(v, "dragstart", Zb);
                b.a(v, "selectstart", Zb);
                b.a(g, "mouseup", jb);
                b.a(j, "mouseup", jb);
                b.a(j, "touchend", jb);
                b.a(j, "touchcancel", jb);
                b.a(g, "blur", jb);
                if (Gb && yb) {
                    jc = new yb.O(Gb, yb, K, R);
                    gb.push(jc)
                }
                if (db && Jb && Ib) {
                    db.Ab = D;
                    lc = new db.O(Jb, Ib, db, K, R, a);
                    gb.push(lc)
                }
                if (pb && W) {
                    W.fc = n.fc;
                    W.rb = W.rb || 0;
                    fc = new W.O(pb, W);
                    !W.qf && b.u(pb, bc, "1");
                    gb.push(fc)
                }
                b.f(gb, function(a) {
                    a.Wb(q, C, X);
                    a.ub(p.dc, Kb)
                });
                b.gb(v, "visibility", "visible");
                a.Eb(K, R);
                Bb();
                n.rb && b.a(j, "keydown", function(a) {
                    if (a.keyCode == 37) Kb(-n.rb, d);
                    else a.keyCode == 39 && Kb(n.rb, d)
                });
                var l = n.fc;
                l = t(l);
                xb(l, 0)
            }
        };
        b.Kb(a)
    };
    i.Ld = 21;
    i.Qd = 22;
    i.me = 23;
    i.gf = 24;
    i.Ye = 25;
    i.Jd = 26;
    i.we = 27;
    i.qe = 28;
    i.ie = 31;
    i.ke = 32;
    i.Xe = 202;
    i.le = 203;
    i.fe = 206;
    i.se = 207;
    i.re = 208;
    i.kd = 209;
    jssor_1_slider_init = function() {
        var e = {
                sb: 1,
                cd: 720,
                Fb: 2,
                qd: 130,
                Sd: {
                    O: t
                },
                Td: {
                    O: s
                }
            },
            d = new i("jssor_1", e),
            f = 980;

        function a() {
            var e = d.K.parentNode,
                b = e.clientWidth;
            if (b) {
                var h = c.min(f || b, b);
                d.he(h)
            } else g.setTimeout(a, 30)
        }
        a();
        b.a(g, "load", a);
        b.a(g, "resize", a);
        b.a(g, "orientationchange", a)
    }
}(window, document, Math, null, true, false)




