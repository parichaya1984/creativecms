package com.creative.bsnlbulkmis.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_englishseekho_content")
public class EnglishSeekhoContentBean {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id; 
	
	@Column(name="contentid")private Integer contentid;
	@Column(name="contenttitle")private String contenttitle;
	@Column(name="contenttitle2")private String contenttitle2;
	@Column(name="contenttype")private String contenttype;
	@Column(name="contentpreviewpath")private String contentpreviewpath;
	@Column(name="contentbannerpath")private String contentbannerpath;
	@Column(name="contentvideopath")private String contentvideopath;
	@Column(name="contentpreviewvideopath")private String contentpreviewvideopath;
	@Column(name="contentoperator")private String contentoperator;
	@Column(name="contentlanguage")private String contentlanguage;
	@Column(name="contentstartdate")private Date contentstartdate;
	@Column(name="contentenddate")private Date contentenddate;
	@Column(name="contentenable")private Integer contentenable;
	@Column(name="contentrating")private Integer contentrating;
	@Column(name="contentmediatype")private String contentmediatype;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getContentid() {
		return contentid;
	}
	public void setContentid(Integer contentid) {
		this.contentid = contentid;
	}
	public String getContenttitle() {
		return contenttitle;
	}
	public void setContenttitle(String contenttitle) {
		this.contenttitle = contenttitle;
	}
	public String getContenttitle2() {
		return contenttitle2;
	}
	public void setContenttitle2(String contenttitle2) {
		this.contenttitle2 = contenttitle2;
	}
	public String getContenttype() {
		return contenttype;
	}
	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}
	public String getContentpreviewpath() {
		return contentpreviewpath;
	}
	public void setContentpreviewpath(String contentpreviewpath) {
		this.contentpreviewpath = contentpreviewpath;
	}
	public String getContentbannerpath() {
		return contentbannerpath;
	}
	public void setContentbannerpath(String contentbannerpath) {
		this.contentbannerpath = contentbannerpath;
	}
	public String getContentvideopath() {
		return contentvideopath;
	}
	public void setContentvideopath(String contentvideopath) {
		this.contentvideopath = contentvideopath;
	}
	public String getContentpreviewvideopath() {
		return contentpreviewvideopath;
	}
	public void setContentpreviewvideopath(String contentpreviewvideopath) {
		this.contentpreviewvideopath = contentpreviewvideopath;
	}
	public String getContentoperator() {
		return contentoperator;
	}
	public void setContentoperator(String contentoperator) {
		this.contentoperator = contentoperator;
	}
	public String getContentlanguage() {
		return contentlanguage;
	}
	public void setContentlanguage(String contentlanguage) {
		this.contentlanguage = contentlanguage;
	}
	public Date getContentstartdate() {
		return contentstartdate;
	}
	public void setContentstartdate(Date contentstartdate) {
		this.contentstartdate = contentstartdate;
	}
	public Date getContentenddate() {
		return contentenddate;
	}
	public void setContentenddate(Date contentenddate) {
		this.contentenddate = contentenddate;
	}
	public Integer getContentenable() {
		return contentenable;
	}
	public void setContentenable(Integer contentenable) {
		this.contentenable = contentenable;
	}
	public Integer getContentrating() {
		return contentrating;
	}
	public void setContentrating(Integer contentrating) {
		this.contentrating = contentrating;
	}
	public String getContentmediatype() {
		return contentmediatype;
	}
	public void setContentmediatype(String contentmediatype) {
		this.contentmediatype = contentmediatype;
	}
	@Override
	public String toString() {
		return "EnglishSeekhoContentBean [id=" + id + ", contentid=" + contentid + ", contenttitle=" + contenttitle
				+ ", contenttitle2=" + contenttitle2 + ", contenttype=" + contenttype + ", contentpreviewpath="
				+ contentpreviewpath + ", contentbannerpath=" + contentbannerpath + ", contentvideopath="
				+ contentvideopath + ", contentpreviewvideopath=" + contentpreviewvideopath + ", contentoperator="
				+ contentoperator + ", contentlanguage=" + contentlanguage + ", contentstartdate=" + contentstartdate
				+ ", contentenddate=" + contentenddate + ", contentenable=" + contentenable + ", contentrating="
				+ contentrating + ", contentmediatype=" + contentmediatype + "]";
	}
	
	
	
	

}
