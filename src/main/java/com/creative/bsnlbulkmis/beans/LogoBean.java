package com.creative.bsnlbulkmis.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_logo")
public class LogoBean {
 
	@Id
	@Column(name="logoid")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int logoid;
	
	@Column(name="logoname")
	private String logoname;
	
	@Column(name="logopath")
	private String logopath;
	
	@Column(name="logooperator")
	private String logooperator;
	
	@Column(name="logostatus")
	private int logostatus;
	
	@Column(name="logodate")
	private Date logodate;

	public int getLogoid() {
		return logoid;
	}

	public void setLogoid(int logoid) {
		this.logoid = logoid;
	}

	public String getLogoname() {
		return logoname;
	}

	public void setLogoname(String logoname) {
		this.logoname = logoname;
	}

	public String getLogopath() {
		return logopath;
	}

	public void setLogopath(String logopath) {
		this.logopath = logopath;
	}

	public String getLogooperator() {
		return logooperator;
	}

	public void setLogooperator(String logooperator) {
		this.logooperator = logooperator;
	}

	public int getLogostatus() {
		return logostatus;
	}

	public void setLogostatus(int logostatus) {
		this.logostatus = logostatus;
	}

	public Date getLogodate() {
		return logodate;
	}

	public void setLogodate(Date logodate) {
		this.logodate = logodate;
	}

	@Override
	public String toString() {
		return "LogoBean [logoid=" + logoid + ", logoname=" + logoname + ", logopath=" + logopath + ", logooperator="
				+ logooperator + ", logostatus=" + logostatus + ", logodate=" + logodate + "]";
	}
	
	
	
}
