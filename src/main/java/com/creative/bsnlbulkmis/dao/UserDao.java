package com.creative.bsnlbulkmis.dao;

import com.creative.bsnlbulkmis.beans.User;

public interface UserDao {
	
	public User getUserByIdAndPassword(String userid, String password);

}
