<!DOCTYPE html>
<html>
<head lang="en">

    <!-- libraries start-->
    <jsp:include page="include/libraries.jsp"/>
    <!-- libraries end -->

</head>

<body>

<!-- header Start -->
<jsp:include page="include/header.jsp"/>
<!-- header End -->

<div class="page-wrapper">
    <div class="container">

        <!--download strat-->
        <div class="download-wrapper">
            <div class="col-xs-12">
                <div class="downlaod-img">
                    <img src="${pageContext.request.contextPath}/resources/images/slider1.jpg" alt="" class="img-responsive" />
                </div>
            </div>
            <div class="col-xs-12">
                <div class="download-title">Game</div>
                <div class="download-category-name">category name : action</div>
                <div class="download-star-rating">star rating :
                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                </div>
                <a href="${pageContext.request.contextPath}/consent"><div class="download-btn">Download</div></a>
            </div>
        </div>
        <div class="clearfix"> </div>
        <!--download end-->

        <div class="grid-wrapper">
            <div class="title-head">
                <div class="title">recommended</div>
                <a href="${pageContext.request.contextPath}/listing"><div class="see-all">more</div></a>
            </div>
            <div class="clearfix"></div>

            <div class="owl-carousel owl-theme">
                <?php for($i=1;$i<=8;$i++){ ?>
                    <div class="slide-item">
                        <a href="${pageContext.request.contextPath}/details">
                            <div class="img-hover">
                                <img class="img-responsive" src="${pageContext.request.contextPath}/resources/images/JellyBears.png" alt=""/>
                            </div>
                            <div class="content-info">
                                <div class="content-title">Jelly Bears</div>
                                <div class="title-name">puzzle<span class="title-fade"></span></div>
                                <div class="star-rating">
                                    <ul>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                                <div class="icon"><i class="fa fa-gamepad" aria-hidden="true"></i></div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>

<!-- footer Start -->
<jsp:include page="include/footer.jsp"/>
<!-- footer End -->

</body>

</html>


