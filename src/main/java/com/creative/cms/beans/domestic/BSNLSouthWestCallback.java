package com.creative.cms.beans.domestic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity
@Table(name = "tb_bsnl_south_west_callback")
public class BSNLSouthWestCallback implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Integer id;
	private String token;
	@Column(name = "transaction_id")
	private String transactionId;
	@Column(name = "req_type")
	private String reqType;	
	
	@Column(name = "campaign_id")
	private Integer campaignId;
	
	@Column(name = "service_id")
	private String serviceId;
	private String keyword;
	private String cgid;
	private String validity;
	private String shortcode;
	private String reqtype;
	private String mode;
	@Column(name = "consent_mode")
	private String consentMode;
	@Column(name = "consent_status")
	private String consentstatus;
	private String remarks;	
	private String msisdn;
	@Column(name = "create_date")
	private Timestamp createDate;
	@Column(name = "query_str")
	private String queryStr;	
	@Column(name = "adnetwork_token_id")
	private Integer adnetworkTokenId;	
	@Column(name = "adnetwork_req_time_long")
	private Long adnetworkReqTimeLong;
	@Column(name="op_id")
	private Integer  opId;	
	@Column(name="circle")
	private String  circle;	
	@Column(name="price")
	private int  price;
	@Column(name="autorenewal")
	private String autorenewal;	
	@Column(name="requesttimestamp")
	private String requesttimestamp;
	@Column(name="send_to_adnetwork")
	private Boolean sendToAdnetwork=false;
	
public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        String str = this.getClass().getName();
        try {
            for (Field field : fields) {
                str += field.getName() + "=" + field.get(this) + ",";
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        return str;
    }

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getCgid() {
		return cgid;
	}
	public void setCgid(String cgid) {
		this.cgid = cgid;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getShortcode() {
		return shortcode;
	}
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}
	public String getReqtype() {
		return reqtype;
	}
	public void setReqtype(String reqtype) {
		this.reqtype = reqtype;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getConsentMode() {
		return consentMode;
	}
	public void setConsentMode(String consentMode) {
		this.consentMode = consentMode;
	}
	public String getConsentstatus() {
		return consentstatus;
	}
	public void setConsentstatus(String consentstatus) {
		this.consentstatus = consentstatus;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public String getQueryStr() {
		return queryStr;
	}
	public void setQueryStr(String queryStr) {
		this.queryStr = queryStr;
	}
	public Integer getAdnetworkTokenId() {
		return adnetworkTokenId;
	}
	public void setAdnetworkTokenId(Integer adnetworkTokenId) {
		this.adnetworkTokenId = adnetworkTokenId;
	}
	public Long getAdnetworkReqTimeLong() {
		return adnetworkReqTimeLong;
	}
	public void setAdnetworkReqTimeLong(Long adnetworkReqTimeLong) {
		this.adnetworkReqTimeLong = adnetworkReqTimeLong;
	}
	public Integer getOpId() {
		return opId;
	}
	public void setOpId(Integer opId) {
		this.opId = opId;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getAutorenewal() {
		return autorenewal;
	}
	public void setAutorenewal(String autorenewal) {
		this.autorenewal = autorenewal;
	}
	public String getRequesttimestamp() {
		return requesttimestamp;
	}
	public void setRequesttimestamp(String requesttimestamp) {
		this.requesttimestamp = requesttimestamp;
	}
	

	public Boolean getSendToAdnetwork() {
		return sendToAdnetwork;
	}

	public void setSendToAdnetwork(Boolean sendToAdnetwork) {
		this.sendToAdnetwork = sendToAdnetwork;
	}

	public Integer getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}

	public String getReqType() {
		return reqType;
	}

	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
	
	
}
