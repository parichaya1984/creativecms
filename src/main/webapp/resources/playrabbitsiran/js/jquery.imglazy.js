(function(){

    function LazyLoad(options)
    {
        var $win = $(window),
            opt = $.extend({
                selector: "img[data-src]",
                src: "data-src",
                effect: "none",
                threshold: 0,
                timeout: 0,
                viewport: false
            }, options),
            src = opt.src,
            threshold = opt.threshold,
            eventName = "scroll resize",
            timerID, goLoadImg;

        this.init = function() {

            removeEvent();
            bindEvent();
            goLoadImg();
        };

        goLoadImg = opt.timeout === 0 ? loadImg : function(){
            clearTimeout(timerID);
            timerID = setTimeout(loadImg, opt.timeout);
        };

        function loadImg()
        {
            // console.info("loadImg start!");
            var elements = $(opt.selector),
                i = 0,
                len = elements.length,
                scrollTop = $win.scrollTop(),
                viewHeight = $win.height(),
                didLazy = 0;

            for(; i < len; i++){
                checkImg(elements.eq(i));
            }

            len === 0 && removeEvent();

            function checkImg($ele)
            {
                var offsetTop = $ele.offset().top;
                var imgSrc = $ele.attr(src);
                var cando = offsetTop <= scrollTop + viewHeight + threshold;
                var oImage;

                cando = cando && (!opt.viewport || offsetTop + $ele.height() + threshold >= scrollTop);

                if(cando){

                    didLazy += 1;

                    oImage = new Image();

                    oImage.onload = function() {

                        $ele.attr("src", imgSrc).removeAttr(src);
                        opt.effect === "fadeIn" && $ele.css("opacity", 0).animate({opacity: 1});
                        doLazyAgain();
                    };

                    oImage.onerror = function() {

                        $ele.attr("src", imgSrc).removeAttr(src);
                        doLazyAgain();
                    };

                    oImage.src = imgSrc;
                }
            }

            function doLazyAgain()
            {
                didLazy--;

                if(didLazy <= 0) {
                    setTimeout(loadImg, 400);
                }
            }
        }


        function bindEvent()
        {
            if(document.addEventListener) {

                window.addEventListener('scroll', goLoadImg);
                window.addEventListener('resize', goLoadImg);

            }else{

                window.attachEvent('onscroll', goLoadImg);
                window.attachEvent('onresize', goLoadImg);
            }
        }

        function removeEvent()
        {
            if(document.addEventListener) {

                window.removeEventListener('scroll', goLoadImg);
                window.removeEventListener('resize', goLoadImg);

            }else{

                window.detachEvent('onscroll', goLoadImg);
                window.detachEvent('onresize', goLoadImg);
            }
        }
    }

    $.imgLazy = function(options){

        var obj = new LazyLoad(options);
        $.imgLazy = obj.init;
        obj.init();
    };

})();