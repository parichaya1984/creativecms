package com.creative.cms.utilities;

import java.security.spec.AlgorithmParameterSpec;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptDecryptUtility {
	
	public static String formatString(List<String> list) {
		if(list!=null&&list.size()>0){
			return list.get(0);
		}		
		return null;
	}
	
public static String getFormatEncryptedCGCallbackQueryString(String cgCallbackEncryptedParameter){
		
		String queryString=(cgCallbackEncryptedParameter==null)?"":cgCallbackEncryptedParameter.trim();
	    StringBuilder sb1=new StringBuilder();
	    for(int i=0;i<queryString.length();i++){
	       char ch=queryString.charAt(i);
	       if(ch==' '){
	           sb1.append("+");
	       }else
	           sb1.append((char)ch);
	   }
		return sb1.toString();
		
		
		}

	Cipher ecipher;
	Cipher dcipher;
	String keyPassword = "awdrgyji";
	SecretKeySpec key = new SecretKeySpec(keyPassword.getBytes(), "DES");
	AlgorithmParameterSpec paramSpec = new IvParameterSpec(
			keyPassword.getBytes());

	public String encrypt(String str) throws Exception {
		ecipher = Cipher.getInstance("DES/CFB8/NoPadding");
		ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
		byte[] utf8 = str.getBytes("UTF8");
		byte[] enc = ecipher.doFinal(utf8);
		return new sun.misc.BASE64Encoder().encode(enc);
	}

	public String decrypt(String str) throws Exception {
		dcipher = Cipher.getInstance("DES/CFB8/NoPadding");
		dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
		byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
		byte[] utf8 = dcipher.doFinal(dec);
		return new String(utf8, "UTF8");
	}
	
	
	public static void main(String args[]) throws Exception{
		EncryptDecryptUtility ed = new EncryptDecryptUtility();
		
		String a = "QjVEwVW1MC9licHWT3QBUZeqGiQu5gIu6sKhHw6DuC9Gq8TS+lZ6HDxfK946AdnaE1fEU4ZPlV1nLAja0LXoluHIRAPZnnuKub50W6P2jYBEhAUFBXm2RWcrxeuAPy/mELo2hfQdiwcJs63b+MKW2qLC5OAtOpxfBlh+WSNlvFIrBYoFVsG58sdCAyNgAGZ5L13+M2bWBS6Hq0BdyJ0MJI66XPJK7rOC9woGYD1YU1mK7wnOvxAQ0315e5cA1qA49i5J35Mkdls9adKLf7g0p+++dfi5ou4C6KX0zWFwVHCx31OZAvtdVQ+ISeKXJGI=";
		
		String re = ed.decrypt(a);
		
		System.out.println(re);
	}

}
