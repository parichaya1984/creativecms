<!DOCTYPE html>
<html>
<head lang="en">

    <!-- libraries start-->
    <jsp:include page="include/libraries.jsp"/>
    <!-- libraries end -->

</head>

<body>

<!-- header Start -->
<jsp:include page="include/header.jsp"/>
<!-- header End -->

<div class="page-wrapper">
    <div class="container">

        <!-- main content Start -->
        <section>
            <!-- unsubscription start -->
            <div class="subscription-bg">
                <h3>Active Subscription</h3>
            </div>
            <div class="clearfix"></div>
            <div class="subscription"><p>product name - Play Rabbits</p></div>  <div class="clearfix"></div>
            <div class="subscription"><p>started at - yyyy - mm - dd  hh : mm : ss</p></div>  <div class="clearfix"></div>
            <div class="subscription"><p>expired at - yyyy - mm - dd  hh : mm : ss</p></div>  <div class="clearfix"></div>
            <div class="subscription"><p>Number of Content left : UNLIMITED</p></div>  <div class="clearfix"></div>
            <%-- <a href="${pageContext.request.contextPath}/subscription"> <div class="un-subscription"> Play Rabbits </div> </a> --%>
            <a href="https://bit.ly/2G9QWql"> <div class="un-subscription"> Play Rabbits </div> </a>
            <div class="clearfix"></div>
            <!-- unsubscription end -->
        </section>
        <!-- main content end -->

    </div>
</div>

<!-- footer Start -->
<jsp:include page="include/footer.jsp"/>
<!-- footer End -->

</body>

</html>





