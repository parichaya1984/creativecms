package com.creative.bsnlbulkmis.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_content_category_map")
public class ContentCategoryMapBean {
	
	@Id
	@Column(name="contentcategorymapid")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer contentcategorymapid;
	 
	@Column(name="categoryid")
	private int categoryid;
	@Column(name="categoryname")
	private String categoryname;
	@Column(name="appid")
	private int appid;
	@Column(name="contentcategorymapstatus")
	private int contentcategorymapstatus;
	@Column(name="creationdate")
	private Date creationdate;
	@Column(name="operator")
	private String operator;
	@Column(name="contentid")
	private int contentid;
	@Column(name="contenttype")
	private String contenttype;
	@Column(name="contentimagepath")
	private String contentimagepath;
	@Column(name="contentvideopath")
	private String contentvideopath;
	@Column(name="contentwallpaperpath")
	private String contentwallpaperpath;
	@Column(name="contentdesc")
	private String contentdesc;
	@Column(name="contenttitle")
	private String contenttitle;
	@Column(name="contentgamepath")
	private String contentgamepath;
	@Column(name="rating")
	private int rating;
	
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public Integer getContentcategorymapid() {
		return contentcategorymapid;
	}
	public void setContentcategorymapid(Integer contentcategorymapid) {
		this.contentcategorymapid = contentcategorymapid;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public String getCategoryname() {
		return categoryname;
	}
	public String getContentgamepath() {
		return contentgamepath;
	}
	public void setContentgamepath(String contentgamepath) {
		this.contentgamepath = contentgamepath;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	public int getAppid() {
		return appid;
	}
	public void setAppid(int appid) {
		this.appid = appid;
	}
	public int getContentcategorymapstatus() {
		return contentcategorymapstatus;
	}
	public void setContentcategorymapstatus(int contentcategorymapstatus) {
		this.contentcategorymapstatus = contentcategorymapstatus;
	}
	public Date getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public int getContentid() {
		return contentid;
	}
	public void setContentid(int contentid) {
		this.contentid = contentid;
	}
	public String getContenttype() {
		return contenttype;
	}
	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}
	public String getContentimagepath() {
		return contentimagepath;
	}
	public void setContentimagepath(String contentimagepath) {
		this.contentimagepath = contentimagepath;
	}
	public String getContentvideopath() {
		return contentvideopath;
	}
	public void setContentvideopath(String contentvideopath) {
		this.contentvideopath = contentvideopath;
	}
	public String getContentwallpaperpath() {
		return contentwallpaperpath;
	}
	public void setContentwallpaperpath(String contentwallpaperpath) {
		this.contentwallpaperpath = contentwallpaperpath;
	}
	public String getContentdesc() {
		return contentdesc;
	}
	public void setContentdesc(String contentdesc) {
		this.contentdesc = contentdesc;
	}
	public String getContenttitle() {
		return contenttitle;
	}
	public void setContenttitle(String contenttitle) {
		this.contenttitle = contenttitle;
	}
	@Override
	public String toString() {
		return "ContentCategoryMapBean [contentcategorymapid=" + contentcategorymapid + ", categoryid=" + categoryid
				+ ", categoryname=" + categoryname + ", appid=" + appid + ", contentcategorymapstatus="
				+ contentcategorymapstatus + ", creationdate=" + creationdate + ", operator=" + operator
				+ ", contentid=" + contentid + ", contenttype=" + contenttype + ", contentimagepath=" + contentimagepath
				+ ", contentvideopath=" + contentvideopath + ", contentwallpaperpath=" + contentwallpaperpath
				+ ", contentdesc=" + contentdesc + ", contenttitle=" + contenttitle + ", contentgamepath="
				+ contentgamepath + ", rating=" + rating + "]";
	}
	
	
	
	
	

}
