<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath }/resources/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="${pageContext.request.contextPath }/resources/css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="${pageContext.request.contextPath }/resources/css/font-awesome.css" rel="stylesheet">
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/icon-font.css" type='text/css' />
<!-- //lined-icons -->
<!-- Meters graphs -->
<script src="${pageContext.request.contextPath }/resources/js/jquery-2.1.4.js"></script>