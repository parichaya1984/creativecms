package com.creative.bsnlbulkmis;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AirtelFuntymWapOnCallController {
	
	@RequestMapping(value="/airtel/funtym")
	public ModelAndView getAirtelFuntym(Model model, Locale locale, HttpServletRequest request){
		System.out.println("request-->"+request.getQueryString());
		return new ModelAndView("themes/airtelfuntym/funtymairtel");
	}

}
