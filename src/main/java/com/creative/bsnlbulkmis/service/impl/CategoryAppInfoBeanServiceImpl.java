package com.creative.bsnlbulkmis.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;
import com.creative.bsnlbulkmis.dao.CategoryAppInfoBeanDao;
import com.creative.bsnlbulkmis.service.CategoryAppInfoBeanService;

@Service
public class CategoryAppInfoBeanServiceImpl  implements CategoryAppInfoBeanService{
	
	private CategoryAppInfoBeanDao categoryAppInfoBeanDao;
	
	

	public void setCategoryAppInfoBeanDao(CategoryAppInfoBeanDao categoryAppInfoBeanDao) {
		this.categoryAppInfoBeanDao = categoryAppInfoBeanDao;
	}



	@Override
	@Transactional
	public List<CategoryAppInfoBean> getAppInfo(int appid) {
		return this.categoryAppInfoBeanDao.getAppInfo(appid);
	}

}
