package com.creative.bsnlbulkmis.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.bsnlbulkmis.beans.User;
import com.creative.bsnlbulkmis.dao.UserDao;
import com.creative.bsnlbulkmis.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private UserDao userDao;
	
	

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}



	@Override
	@Transactional
	public User getUserByIdAndPassword(String userid, String password) {
		return this.userDao.getUserByIdAndPassword(userid, password);
	}

}
