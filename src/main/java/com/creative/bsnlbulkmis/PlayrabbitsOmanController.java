package com.creative.bsnlbulkmis;

import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.creative.bsnlbulkmis.beans.AppBean;
import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;
import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;
import com.creative.bsnlbulkmis.beans.LoginBean;
import com.creative.bsnlbulkmis.beans.LogoBean;
import com.creative.bsnlbulkmis.service.AppBeanService;
import com.creative.bsnlbulkmis.service.BannerBeanService;
import com.creative.bsnlbulkmis.service.CategoryAppInfoBeanService;
import com.creative.bsnlbulkmis.service.ContentCategoryMapBeanService;
import com.creative.bsnlbulkmis.service.EnglishSeekhoSubscriptionBeanService;
import com.creative.bsnlbulkmis.service.LogoBeanService;
import com.creative.bsnlbulkmis.service.UserService;
import com.creative.cms.beans.TbIranOtp;
import com.creative.cms.beans.TbSubscribersReg;
import com.creative.cms.service.TbIranOtpService;
import com.creative.cms.service.TbSubscribersRegService;
import com.creative.cms.utilities.CommonUtilities;
import com.creative.cms.utilities.ConstantUtils;

@Controller
public class PlayrabbitsOmanController {/*

	private static final Logger logger = LoggerFactory.getLogger(PlayrabbitsOmanController.class);

	private BannerBeanService bannerBeanService;
	private AppBeanService appBeanService;
	private LogoBeanService logoBeanSerivce;
	private CategoryAppInfoBeanService categoryAppInfoBeanService;
	private ContentCategoryMapBeanService contentCategoryMapBeanService;
	private EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService;
	
	private TbSubscribersRegService tbSubscribersRegService;
	
	private TbIranOtpService tbIranOtpService;
	
	
	
	@Autowired
	@Qualifier(value = "tbIranOtpService")
	public void setTbIranOtpService(TbIranOtpService tbIranOtpService) {
		this.tbIranOtpService = tbIranOtpService;
	}

	@Autowired
	@Qualifier(value = "tbSubscribersRegService")
	public void setTbSubscribersRegService(TbSubscribersRegService tbSubscribersRegService) {
		this.tbSubscribersRegService = tbSubscribersRegService;
	}

	@Autowired
	@Qualifier(value = "englishSeekhoSubscriptionBeanService")
	public void setEnglishSeekhoSubscriptionBeanService(
			EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService) {
		this.englishSeekhoSubscriptionBeanService = englishSeekhoSubscriptionBeanService;
	}

	@Autowired
	@Qualifier(value = "contentCategoryMapBeanService")
	public void setContentCategoryMapBeanService(ContentCategoryMapBeanService contentCategoryMapBeanService) {
		this.contentCategoryMapBeanService = contentCategoryMapBeanService;
	}

	@Autowired
	@Qualifier(value = "categoryAppInfoBeanService")
	public void setCategoryAppInfoBeanService(CategoryAppInfoBeanService categoryAppInfoBeanService) {
		this.categoryAppInfoBeanService = categoryAppInfoBeanService;
	}

	@Autowired
	@Qualifier(value = "logoBeanService")
	public void setLogoBeanSerivce(LogoBeanService logoBeanSerivce) {
		this.logoBeanSerivce = logoBeanSerivce;
	}

	@Autowired
	@Qualifier(value = "bannerBeanService")
	public void setBannerBeanService(BannerBeanService bannerBeanService) {
		this.bannerBeanService = bannerBeanService;
	}

	@Autowired
	@Qualifier(value = "appBeanService")
	public void setAppBeanService(AppBeanService appBeanService) {
		this.appBeanService = appBeanService;
	}
	
	@RequestMapping(value = "/playrabbitsiran/sendOtp", method = RequestMethod.GET)
	public ModelAndView getUserVerification(Locale locale, ModelAndView model, Model m, HttpServletRequest request,
			HttpServletResponse response) {
		
		logger.info("Welcome home! The client locale is {}.", locale);
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
			logger.info("Key-->" + key + "|Value-->" + value);
		}

		String appname = request.getParameter("appname");
		String operatorid = request.getParameter("operatorid");
		String operatorname = request.getParameter("operatorname");
		String sappid = request.getParameter("appid");
		String msisdn = request.getParameter("msisdn");
		String otp = request.getParameter("otp");
		
		int appid = -1;
		if (StringUtils.isEmpty(sappid)) {
			appid = 3;
			appname = "Play Rabbits";
		} else {
			appid = Integer.parseInt(sappid);
		}
		if (StringUtils.isEmpty(appname)) {
			operatorid = "0";
			operatorname = "Demo Play Rabbits";
			appid = 3;
		} else {
			operatorid = "1";
			operatorname = "Iran";
			appid = 3;
		}

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		List<BannerBean> bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid),
				operatorname);
		logger.info("List of Banners::" + bannerList);

		m.addAttribute("serverTime", formattedDate);
		m.addAttribute("banners", bannerList);
		
		List<AppBean> appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::" + appList);

		List<LogoBean> logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::" + logoList);


		List<CategoryAppInfoBean> categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::" + categoryAppInfoBeans);

		List<ContentCategoryMapBean> contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid,
				operatorname);
		logger.info("contentCategoryMapBeans::" + contentCategoryMapBeans);

		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		if (appList != null) {
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		} else {
			m.addAttribute("appname", "Play Rabbits");
			m.addAttribute("logo", "");

		}
		
		m.addAttribute("otppage", "2");
		m.addAttribute("msisdn", msisdn);
		//Write OTP Code here
		
		//Calling Http URL from here.
		CommonUtilities cu = new CommonUtilities();
		int retVal 	= cu.sendOtpToMsisdnIran(msisdn);
		logger.info("Otp Sent to Msisdn-->"+retVal);
		
		return new ModelAndView("themes/playrabbitsiran/home");
	}
	
	@RequestMapping(value = "/playrabbitsiran/validateOtp", method = RequestMethod.GET)
	public ModelAndView getOtpValidation(Locale locale, ModelAndView model, Model m, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
			logger.info("Key-->" + key + "|Value-->" + value);
		}
		
		

		String appname = request.getParameter("appname");
		String operatorid = request.getParameter("operatorid");
		String operatorname = request.getParameter("operatorname");
		String sappid = request.getParameter("appid");
		String msisdn = request.getParameter("msisdn");
		String otp = request.getParameter("otp");
		int appid = -1;
		if (StringUtils.isEmpty(sappid)) {
			appid = 3;
			appname = "Play Rabbits";
		} else {
			appid = Integer.parseInt(sappid);
		}
		if (StringUtils.isEmpty(appname)) {
			operatorid = "0";
			operatorname = "Demo Play Rabbits";
			appid = 3;
		} else {
			operatorid = "1";
			operatorname = "Iran";
			appid = 3;
		}

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		List<BannerBean> bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid),
				operatorname);
		logger.info("List of Banners::" + bannerList);

		m.addAttribute("serverTime", formattedDate);
		m.addAttribute("banners", bannerList);
		
		List<AppBean> appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::" + appList);

		List<LogoBean> logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::" + logoList);


		List<CategoryAppInfoBean> categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::" + categoryAppInfoBeans);

		List<ContentCategoryMapBean> contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid,
				operatorname);
		logger.info("contentCategoryMapBeans::" + contentCategoryMapBeans);

		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		if (appList != null) {
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		} else {
			m.addAttribute("appname", "Play Rabbits");
			m.addAttribute("logo", "");

		}
		m.addAttribute("otppage", "3");
		m.addAttribute("disablelinks", "2");
		
		//System.out.println("Secnod DB Info--->>>>>"+this.tbSubscribersRegService.getUserInfo("919896936967"));
		
		//System.out.println("Secnod DB Info--->>>>>"+this.tbSubscribersRegService.getUserInfo("919896936967"));
		
		//Validate Msisdn & Otp Logic Here msisdn
		
		TbSubscribersReg tbIranUser = this.tbSubscribersRegService.getUserInfo(msisdn);
		System.out.println("Secnod DB Info--->>>>>"+this.tbSubscribersRegService.getUserInfo(msisdn.trim()));
		if(tbIranUser!=null){
			int status = tbIranUser.getStatus();
			if(status == 1){
				// Validate OTP logic
				
				TbIranOtp tbIranOtpDetails = this.tbIranOtpService.getUserInfo(msisdn);
				
				if(tbIranOtpDetails != null){
					int otpStatus = tbIranOtpDetails.getUsed();
					if(otpStatus == 0){
						if(!StringUtils.isEmpty(otp)){
							if(otp.equalsIgnoreCase(tbIranOtpDetails.getOtp()) && msisdn.equalsIgnoreCase(tbIranUser.getMsisdn())){
								logger.info("Otp Validated.. Go to Portal");
								System.out.println("Otp Validated.. Go to Portal");
								
								//this.tbIranOtpService.
								
								return new ModelAndView("themes/playrabbitsiran/home");
							}else{
								logger.info("Invalid OTP or OTP Expired");
								System.out.println("Invalid OTP or OTP Expired");
								return new ModelAndView("themes/playrabbitsiran/home");
							}
						}
					}
				}
				
			}else{
				//rediredct: http://185.173.105.87/international/cnt/c/cmp?adid=1&cmpid=129&token=sss
				return new ModelAndView("redirect:"+ConstantUtils.REDIRECT_TO_NON_SUBSCRIBER);
			}
		}else{
			return new ModelAndView("redirect:"+ConstantUtils.REDIRECT_TO_NON_SUBSCRIBER);
		}
		
		return new ModelAndView("themes/playrabbitsiran/home");
	}

	@RequestMapping(value = "/playrabbitsiran", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, ModelAndView model, Model m, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
			logger.info("Key-->" + key + "|Value-->" + value);
		}

		String appname = request.getParameter("appname");
		String operatorid = request.getParameter("operatorid");
		String operatorname = request.getParameter("operatorname");
		String sappid = request.getParameter("appid");
		String msisdn = request.getParameter("msisdn");
		String otp = request.getParameter("otp");
		int appid = -1;
		if (StringUtils.isEmpty(sappid)) {
			appid = 3;
			appname = "Play Rabbits";
		} else {
			appid = Integer.parseInt(sappid);
		}
		if (StringUtils.isEmpty(appname)) {
			operatorid = "0";
			operatorname = "Demo Play Rabbits";
			appid = 3;
		} else {
			operatorid = "1";
			operatorname = "Iran";
			appid = 3;
		}

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		List<BannerBean> bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid),
				operatorname);
		logger.info("List of Banners::" + bannerList);

		m.addAttribute("serverTime", formattedDate);
		m.addAttribute("banners", bannerList);
		
		List<AppBean> appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::" + appList);

		List<LogoBean> logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::" + logoList);


		List<CategoryAppInfoBean> categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::" + categoryAppInfoBeans);

		List<ContentCategoryMapBean> contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid,
				operatorname);
		logger.info("contentCategoryMapBeans::" + contentCategoryMapBeans);

		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		if (appList != null) {
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		} else {
			m.addAttribute("appname", "Play Rabbits");
			m.addAttribute("logo", "");

		}
		m.addAttribute("otppage", "1");
		m.addAttribute("disablelinks", "1");
		
		return new ModelAndView("themes/playrabbitsiran/home");
	}
	
	@RequestMapping(value={"/playrabbitsiran/listing"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getListingPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/listing";
	  }
	  
	  @RequestMapping(value={"/playrabbitsiran/details"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getDetailsPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/details";
	  }
	  
	  @RequestMapping(value={"/playrabbitsiran/subscription"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getSubscriptionPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/subscription";
	  }
	  
	  @RequestMapping(value={"/playrabbitsiran/unsubscription"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getUnSubscriptionPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/unsubscription";
	  }
	  
	  @RequestMapping(value={"/playrabbitsiran/tnc"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getTermsAndConditionPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/tnc";
	  }
	  
	  @RequestMapping(value={"/playrabbitsiran/consent"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getConsentPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/consent";
	  }


*/}
