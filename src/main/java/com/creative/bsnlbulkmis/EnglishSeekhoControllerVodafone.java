package com.creative.bsnlbulkmis;

import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.creative.bsnlbulkmis.beans.AppBean;
import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;
import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;
import com.creative.bsnlbulkmis.beans.LoginBean;
import com.creative.bsnlbulkmis.beans.LogoBean;
import com.creative.bsnlbulkmis.service.AppBeanService;
import com.creative.bsnlbulkmis.service.BannerBeanService;
import com.creative.bsnlbulkmis.service.CategoryAppInfoBeanService;
import com.creative.bsnlbulkmis.service.ContentCategoryMapBeanService;
import com.creative.bsnlbulkmis.service.EnglishSeekhoSubscriptionBeanService;
import com.creative.bsnlbulkmis.service.LogoBeanService;
import com.creative.bsnlbulkmis.service.UserService;

@Controller
public class EnglishSeekhoControllerVodafone {
	
	private static final Logger logger = LoggerFactory.getLogger(EnglishSeekhoControllerVodafone.class);
	
	private BannerBeanService bannerBeanService;
	private AppBeanService appBeanService;
	private LogoBeanService logoBeanSerivce;
	private CategoryAppInfoBeanService categoryAppInfoBeanService;
	private ContentCategoryMapBeanService contentCategoryMapBeanService;
	private EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService;
	
	
	@Autowired
	@Qualifier(value="englishSeekhoSubscriptionBeanService")
	public void setEnglishSeekhoSubscriptionBeanService(
			EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService) {
		this.englishSeekhoSubscriptionBeanService = englishSeekhoSubscriptionBeanService;
	}


	@Autowired
	@Qualifier(value="contentCategoryMapBeanService")
	public void setContentCategoryMapBeanService(ContentCategoryMapBeanService contentCategoryMapBeanService) {
		this.contentCategoryMapBeanService = contentCategoryMapBeanService;
	}


	@Autowired
	@Qualifier(value="categoryAppInfoBeanService")
	public void setCategoryAppInfoBeanService(CategoryAppInfoBeanService categoryAppInfoBeanService) {
		this.categoryAppInfoBeanService = categoryAppInfoBeanService;
	}


	@Autowired
	@Qualifier(value="logoBeanService")
	public void setLogoBeanSerivce(LogoBeanService logoBeanSerivce) {
		this.logoBeanSerivce = logoBeanSerivce;
	}


	@Autowired
	@Qualifier(value="bannerBeanService")
	public void setBannerBeanService(BannerBeanService bannerBeanService) {
		this.bannerBeanService = bannerBeanService;
	}
	
	
	@Autowired
	@Qualifier(value="appBeanService")
	public void setAppBeanService(AppBeanService appBeanService) {
		this.appBeanService = appBeanService;
	}



	@RequestMapping(value = "/vodafone", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, ModelAndView model, Model m,
			HttpServletRequest request, HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		        //LoginBean loginBean = new LoginBean();
		        //model.addObject("loginBean", loginBean);
		        
			Map<String, String> map = new HashMap<String, String>();
	
	        Enumeration headerNames = request.getHeaderNames();
	        while (headerNames.hasMoreElements()) {
	            String key = (String) headerNames.nextElement();
	            String value = request.getHeader(key);
	            map.put(key, value);
	            logger.info("Key-->"+key+"|Value-->"+value);
	        }
		        
		        String appname = request.getParameter("appname");
		        String operatorid = request.getParameter("operatorid");
		        String operatorname = request.getParameter("operatorname");
		        String sappid = request.getParameter("appid");
		        int appid = -1;
		        if(StringUtils.isEmpty(sappid)){
		        	appid = -1;
		        	appname = "Vodafone";
		        }else{
		        	appid = Integer.parseInt(sappid);
		        }
		        if(StringUtils.isEmpty(appname)){
		        	operatorid = "0";
		        	operatorname = "Demo";
		        	appid = 4;
		        }else{
		        	operatorid = "1";
		        	operatorname = "Vodafone";
		        	appid= 4;
		        }

		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		List<BannerBean>  bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid), operatorname);
		logger.info("List of Banners::"+bannerList);
		
		m.addAttribute("serverTime", formattedDate );
		m.addAttribute("banners", bannerList);
		
		
		List<AppBean>  appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::"+appList);
		
		List<LogoBean>  logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::"+logoList);
		
		List<CategoryAppInfoBean>  categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::"+categoryAppInfoBeans);
		
		
		List<ContentCategoryMapBean>  contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid, operatorname);
		logger.info("contentCategoryMapBeans::"+contentCategoryMapBeans);
		
		if(appList != null){
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		}else{
			m.addAttribute("appname", "Learn English");
			m.addAttribute("logo", "");
				
		}
		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		//return new ModelAndView("newindex");
		return new ModelAndView("themes/vodafonelearnenglish/index");
	}
	
	@RequestMapping(value = "/vodafone/single", method = RequestMethod.GET)
	public ModelAndView getSinglePage(Locale locale, ModelAndView model, Model m) {
		return new ModelAndView("single");
	}
	
	@RequestMapping(value = "/vodafone/callback", method = RequestMethod.GET)
	public ModelAndView callback(Locale locale, ModelAndView model, Model m, HttpServletRequest request, HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
        LoginBean loginBean = new LoginBean();
        model.addObject("loginBean", loginBean);
        
        Map<String, String> map = new HashMap<String, String>();
    	
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
            logger.info("Key-->"+key+"|Value-->"+value);
        }
        
        //String query = request.getQueryString()+"&appid=1";
        
        String appname = request.getParameter("appname");
        String operatorid = request.getParameter("operatorid");
        String operatorname = request.getParameter("operatorname");
        String sappid = request.getParameter("appid");
        int appid = -1;
        /*if(StringUtils.isEmpty(sappid)){
        	appid = -1;
        	appname = "Vodafone";
        }else{
        	appid = Integer.parseInt(sappid);
        }
        if(StringUtils.isEmpty(appname)){
        	operatorid = "0";
        	operatorname = "Demo";
        	appid = 2;
        }else{*/
        	operatorid = "1";
        	operatorname = "Vodafone";
        	appid= 1;
       // }
        	 String responseStr = request.getParameter("response");
        	 
        	 //"RESPONSE_STATUS:900:"+txId+":Success"
        	 String msg = "";
        	 int retVal = 0; 
        	 if(responseStr.contains("800")){
        		 msg = "You have Successfully Subscribed to English Seekho Service";
        		 retVal = 800;
        	 }else if(responseStr.contains("802")){
        		 msg = "You have Successfully Un-Subscribed to English Seekho Service";
        		 retVal = 802;
        	 }else if(responseStr.contains("900")){
        		 msg = "Your Request is Submitted Successfully.";
        		 retVal = 900;
        	 }else{
        		 msg = "Demo Service";
        		 retVal = 0;
        	 }
        	 
        	 /**
        	  * MSISDN=919610530767
        	  * &SERVICE_ID=CRE_ENGSIKOD
        	  * &CLASS=CRE_ENGSIKOD
        	  * &TXNID=1085 3904 8802
        	  * &ACTION=ACT
        	  * &STATUS=SUCCESS
        	  * &CHARGING_MODE=1DAYS3
        	  * &REQ_MODE=IMI_WAP
        	  * &REQUEST_ID=6365 9833 7309 7786 0191 9610 5307 67
        	  * &CIRCLE_ID=9
        	  * &SUB_TYPE=PREPAID        	
        	  */

Date date = new Date();
DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

String formattedDate = dateFormat.format(date);

List<BannerBean>  bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid), operatorname);
logger.info("List of Banners::"+bannerList);

m.addAttribute("serverTime", formattedDate );
m.addAttribute("banners", bannerList);


List<AppBean>  appList = this.appBeanService.getAppInfo(appid, operatorname);
logger.info("Applist::"+appList);

List<LogoBean>  logoList = this.logoBeanSerivce.getAppInfo(operatorname);
logger.info("logoList::"+logoList);

List<CategoryAppInfoBean>  categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
logger.info("categoryAppInfoBeans::"+categoryAppInfoBeans);


List<ContentCategoryMapBean>  contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid, operatorname);
logger.info("contentCategoryMapBeans::"+contentCategoryMapBeans);

if(appList != null){
	m.addAttribute("appname", appList.get(0).getAppname());
	m.addAttribute("logo", logoList.get(0).getLogopath());
}else{
	m.addAttribute("appname", "Learn English");
	m.addAttribute("logo", "");
		
}
m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
m.addAttribute("categories", categoryAppInfoBeans);
m.addAttribute("msg", msg);
m.addAttribute("retval", retVal);
return new ModelAndView("newindex");
}
	

	@RequestMapping(value = "/vodafone/ppucallback", method = RequestMethod.GET)
	public ModelAndView getRegPPUCallback(Locale locale, ModelAndView model, Model m, HttpServletRequest req, HttpServletResponse response) {
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		return new ModelAndView("success");
	}
	
	@RequestMapping(value = "/vodafone/videoplay", method = RequestMethod.GET)
	public ModelAndView getPlayVideo(Locale locale, ModelAndView model, Model m, 
			HttpServletRequest req, HttpServletResponse response) {
		
		
		String appname = req.getParameter("appname");
        String operatorid = req.getParameter("operatorid");
        String operatorname = req.getParameter("operatorname");
        String sappid = req.getParameter("appid");
        int appid = -1;
        if(StringUtils.isEmpty(sappid)){
        	appid = -1;
        	appname = "Vodafone";
        }else{
        	appid = Integer.parseInt(sappid);
        }
        if(StringUtils.isEmpty(appname)){
        	operatorid = "0";
        	operatorname = "Demo";
        	appid = 2;
        }else{
        	operatorid = "1";
        	operatorname = "Vodafone";
        	appid= 1;
        }


			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			
			String formattedDate = dateFormat.format(date);

		
			List<BannerBean>  bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid), operatorname);
		logger.info("List of Banners::"+bannerList);
		
		m.addAttribute("serverTime", formattedDate );
		m.addAttribute("banners", bannerList);
		
		
		List<AppBean>  appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::"+appList);
		
		List<LogoBean>  logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::"+logoList);
		
		List<CategoryAppInfoBean>  categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::"+categoryAppInfoBeans);
		
		
		List<ContentCategoryMapBean>  contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid, operatorname);
		logger.info("contentCategoryMapBeans::"+contentCategoryMapBeans);
		
		m.addAttribute("appname", appList.get(0).getAppname());
		m.addAttribute("logo", logoList.get(0).getLogopath());
		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		
		String videoUrl= req.getParameter("videourl");
		
		logger.info("Video URL::"+videoUrl);
		
		//model.addObject("videourl", videoUrl);
		m.addAttribute("videourl", videoUrl);
		//m.addAttribute("title",);
		
		
		return new ModelAndView("videoplayer");
	}
	
	
	

}
