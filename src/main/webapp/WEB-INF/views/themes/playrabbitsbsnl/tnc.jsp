<!DOCTYPE html>
<html>
<head lang="en">

    <!-- libraries start-->
    <jsp:include page="include/libraries.jsp"/>
    <!-- libraries end -->

</head>
<body>

<!-- header Start -->
<jsp:include page="include/header.jsp"/>
<!-- header End -->

<div class="page-wrapper">

    <div class="container">
        <!--terms & condition start-->
        <div class="terms-condition">
            <p>Terms & conditions</p>
            <ul class="terms-condition-list">
                <li>This is an auto renew service.</li>
                <li>The service costs  will be charged from your mobile credit.</li>
                <li>To unsubscribe from the service, please send  a free SMS with the keyword.</li>
                <li>UNSUB  .</li>
                <li>The service is available in English only.</li>
            </ul>
        </div>
        <!--terms & condition end-->
    </div>

</div>

<!-- footer Start -->
<jsp:include page="include/footer.jsp"/>
<!-- footer End -->

</body>

</html>