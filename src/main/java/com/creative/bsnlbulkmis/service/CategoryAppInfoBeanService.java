package com.creative.bsnlbulkmis.service;

import java.util.List;

import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;

public interface CategoryAppInfoBeanService {
	
	public List<CategoryAppInfoBean> getAppInfo(int appid);

}
