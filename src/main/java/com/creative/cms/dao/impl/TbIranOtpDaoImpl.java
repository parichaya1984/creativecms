package com.creative.cms.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.cms.beans.TbIranOtp;
import com.creative.cms.beans.TbSubscribersReg;
import com.creative.cms.dao.TbIranOtpDao;

@Repository
public class TbIranOtpDaoImpl implements TbIranOtpDao{
	
	private static final Logger logger = LoggerFactory.getLogger(TbIranOtpDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public TbIranOtp getUserInfo(String msisdn) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(TbIranOtp.class);
		criteria.add(Restrictions.eq("msisdn", msisdn));
		
		/*Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("password", password));
		criteria.add(Restrictions.and(c));*/
		
		@SuppressWarnings("unchecked")
		List<TbIranOtp> uList = criteria.list();
		System.out.println("Size::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			logger.info("TbIranOtp loaded successfully, TbIranOtp details="+uList);
			//return uList.get(uList.size()-1);
			return uList.get(0);
		}
	}

	

}
