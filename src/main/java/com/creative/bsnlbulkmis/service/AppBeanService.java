package com.creative.bsnlbulkmis.service;

import java.util.List;

import com.creative.bsnlbulkmis.beans.AppBean;

public interface AppBeanService {
	
	public List<AppBean> getAppInfo(int appid, String operatorname);

}
