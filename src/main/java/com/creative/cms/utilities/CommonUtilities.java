package com.creative.cms.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javacodegeeks.example.util.ConstantUtil;

public class CommonUtilities { 
	 
	private static final Logger logger = LoggerFactory.getLogger(CommonUtilities.class);
	
	public int sendOtpToMsisdnIran(String msisdn){
		System.out.println("Msisdn received for Sending OTP-->"+msisdn);
		int retVal = -1;
		
		try{
			if(!StringUtils.isEmpty(msisdn)){
				retVal = callUrl(ConstantUtils.IRAN_SEND_OTP_URL+msisdn.trim());
				logger.info("Otp Send To Msisdn::"+retVal);
				System.out.println("Otp Send To Msisdn::"+retVal);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return retVal;
	}

	public int callUrl(String sUrl) {

		try{
		String url = (sUrl);
		logger.info("Url formed::"+sUrl);
		System.out.println("Url::"+url);
		logger.info(url);

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		logger.info("\nSending 'GET' request to URL : " + url);
		logger.info("Response Code : " + responseCode);
		System.out.println("responseCode::"+responseCode);

		logger.info("sms response code ->>"+responseCode);
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return responseCode;
		}catch(Exception e){
			return 0;
		}

	}

}
