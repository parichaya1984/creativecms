package com.creative.cms.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.creative.cms.beans.TbSubscribersReg;
import com.creative.cms.dao.TbSubscribersRegDao;
import com.creative.cms.service.TbSubscribersRegService;

public class TbSubscribersRegServiceImpl implements TbSubscribersRegService{
	
	private TbSubscribersRegDao tbSubscribersRegDao;

	public void setTbSubscribersRegDao(TbSubscribersRegDao tbSubscribersRegDao) {
		this.tbSubscribersRegDao = tbSubscribersRegDao;
	}

	@Override
	@Transactional
	public TbSubscribersReg getUserInfo(String msisdn) {
		// TODO Auto-generated method stub
		return this.tbSubscribersRegDao.getUserInfo(msisdn);
	}
	
	

}
