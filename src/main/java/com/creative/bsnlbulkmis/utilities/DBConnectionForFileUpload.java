package com.creative.bsnlbulkmis.utilities;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Chandan Singh
 *
 */
public class DBConnectionForFileUpload
{ 
		public static Connection getConnection() throws SQLException, ClassNotFoundException
		{
				//Class.forName("com.mysql.jdbc.Driver");

				Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bsnlsms", ContantUtil.DB_USERNAME, ContantUtil.DB_PASSWORD);

				return connection;
		}

		public static void main(String[] args)
    {
				try
        {
		        getConnection();
        }
        catch (Exception e)
        {
		        e.printStackTrace();
        }
    }
}

