<%@ page contentType="text/html;charset=UTF-8" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!doctype html>
<html lang="en">
<head>
  <title><spring:message code="englishseekho.hindi.title" text="Learn English" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="My Play Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Bootstrap  css stylesheet-->
    <link href="${pageContext.request.contextPath }/resources/learnenglishnew/css/bootstrap.min.css" rel="stylesheet">
    
    
    <!-- our customised css stylesheet-->
<link rel="stylesheet" href="${pageContext.request.contextPath }/resources/learnenglishnew/css/style.css" type="text/css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>

<%-- <spring:message code="englishseekho.gujarati.lessons" text="Learn English" /> --%>
<%-- <c:forEach items="${contentCategoryMapBeans }" ></c:forEach> --%>
<c:forEach items="${englishSeekhoContentBeans }" var="content" varStatus="cnt">
<c:if test="${cnt.count %2 != 0 }">
	<div class="container wrap" style="background-color:#ccccff; padding-top: 6%; margin-bottom: 4%;">
</c:if>
<c:if test="${cnt.count %2 == 0 }">
	<div class="container wrap" style="background-color:ffcccc; padding-top: 6%; margin-bottom: 4%;">
</c:if>
<div class="col-md-6 col-xs-4 ser1">
<h5 class="text-center">${content.contenttitle }</h5>
</div>

<div class="col-md-3 col-xs-4 ser1">
<div class="panel panel-default panel1">
<div class="panel-body">
<a href="${pageContext.request.contextPath }/newvideoplay?videourl=${content.contentvideopath }&contenttitle=${content.contenttitle }&contenttitle2=${content.contenttitle2}">
<img src="${content.contentbannerpath }" style="width:100%">
</a>
</div>
</div>
</div>

<div class="col-md-3 col-xs-4 ser1">
<div class="panel panel-default panel1">
<div class="panel-body"><img src="${content.contentpreviewpath }" style="width:100%"></div>
</div>
</div>

</div>

</c:forEach>
</body>
</html>