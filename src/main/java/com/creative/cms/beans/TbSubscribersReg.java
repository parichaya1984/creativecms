package com.creative.cms.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_subscribers_reg")
public class TbSubscribersReg {

	@Id 
	@Column(name="subscriber_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int subscriber_id;
	@Column(name="campaign_id")private String campaign_id;
	@Column(name="last_activity")private String last_activity;
	@Column(name="validity_from")private Date validity_from;
	@Column(name="validity_to")private Date validity_to;
	@Column(name="last_download_date")private Date last_download_date;
	@Column(name="last_update")private Date last_update;
	@Column(name="msisdn")private String msisdn;
	@Column(name="operator_id")private int operator_id;
	@Column(name="reg_date")private Date reg_date;
	@Column(name="service_id")private int service_id;
	@Column(name="status")private int status;
	@Column(name="status_descp")private String status_descp;
	@Column(name="sub_date")private Date sub_date;
	@Column(name="token")private String token;
	@Column(name="token_id")private String token_id;
	@Column(name="total_download_count")private String total_download_count;
	@Column(name="unsub_date")private Date unsub_date;
	@Column(name="subscription_type")private String subscription_type;
	@Column(name="last_renewal")private Date last_renewal;
	@Column(name="activation_processed")private String activation_processed;
	
	
	public Date getLast_renewal() {
		return last_renewal;
	}
	public void setLast_renewal(Date last_renewal) {
		this.last_renewal = last_renewal;
	}
	public String getActivation_processed() {
		return activation_processed;
	}
	public void setActivation_processed(String activation_processed) {
		this.activation_processed = activation_processed;
	}
	public int getSubscriber_id() {
		return subscriber_id;
	}
	public void setSubscriber_id(int subscriber_id) {
		this.subscriber_id = subscriber_id;
	}
	public String getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}
	public String getLast_activity() {
		return last_activity;
	}
	public void setLast_activity(String last_activity) {
		this.last_activity = last_activity;
	}
	public Date getLast_download_date() {
		return last_download_date;
	}
	public void setLast_download_date(Date last_download_date) {
		this.last_download_date = last_download_date;
	}
	public Date getLast_update() {
		return last_update;
	}
	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public int getOperator_id() {
		return operator_id;
	}
	public void setOperator_id(int operator_id) {
		this.operator_id = operator_id;
	}
	public Date getReg_date() {
		return reg_date;
	}
	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}
	public int getService_id() {
		return service_id;
	}
	public void setService_id(int service_id) {
		this.service_id = service_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatus_descp() {
		return status_descp;
	}
	public void setStatus_descp(String status_descp) {
		this.status_descp = status_descp;
	}
	public Date getSub_date() {
		return sub_date;
	}
	public void setSub_date(Date sub_date) {
		this.sub_date = sub_date;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getToken_id() {
		return token_id;
	}
	public void setToken_id(String token_id) {
		this.token_id = token_id;
	}
	public String getTotal_download_count() {
		return total_download_count;
	}
	public void setTotal_download_count(String total_download_count) {
		this.total_download_count = total_download_count;
	}
	public Date getUnsub_date() {
		return unsub_date;
	}
	public void setUnsub_date(Date unsub_date) {
		this.unsub_date = unsub_date;
	}
	public String getSubscription_type() {
		return subscription_type;
	}
	public void setSubscription_type(String subscription_type) {
		this.subscription_type = subscription_type;
	}
	public Date getValidity_from() {
		return validity_from;
	}
	public void setValidity_from(Date validity_from) {
		this.validity_from = validity_from;
	}
	public Date getValidity_to() {
		return validity_to;
	}
	public void setValidity_to(Date validity_to) {
		this.validity_to = validity_to;
	}
	@Override
	public String toString() {
		return "TbSubscribersReg [subscriber_id=" + subscriber_id + ", campaign_id=" + campaign_id + ", last_activity="
				+ last_activity + ", validity_from=" + validity_from + ", validity_to=" + validity_to
				+ ", last_download_date=" + last_download_date + ", last_update=" + last_update + ", msisdn=" + msisdn
				+ ", operator_id=" + operator_id + ", reg_date=" + reg_date + ", service_id=" + service_id + ", status="
				+ status + ", status_descp=" + status_descp + ", sub_date=" + sub_date + ", token=" + token
				+ ", token_id=" + token_id + ", total_download_count=" + total_download_count + ", unsub_date="
				+ unsub_date + ", subscription_type=" + subscription_type + ", last_renewal=" + last_renewal
				+ ", activation_processed=" + activation_processed + "]";
	}
	
	
}
