package com.creative.cms.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.creative.bsnlbulkmis.beans.User;
import com.creative.bsnlbulkmis.dao.impl.UserDaoImpl;
import com.creative.cms.beans.TbSubscribersReg;
import com.creative.cms.dao.TbSubscribersRegDao;

public class TbSubscribersRegDaoImpl implements TbSubscribersRegDao{
	
	private static final Logger logger = LoggerFactory.getLogger(TbSubscribersRegDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public TbSubscribersReg getUserInfo(String msisdn) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(TbSubscribersReg.class);
		criteria.add(Restrictions.eq("msisdn", msisdn));
		
		/*Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("password", password));
		criteria.add(Restrictions.and(c));*/
		
		@SuppressWarnings("unchecked")
		List<TbSubscribersReg> uList = criteria.list();
		System.out.println("Size::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			logger.info("UserInformation loaded successfully, UserInformation details="+uList);
			//return uList.get(uList.size()-1);
			return uList.get(0);
		}
	}
	
	

}
