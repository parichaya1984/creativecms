package com.creative.cms.beans.domestic.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.cms.beans.domestic.BSNLSouthWestCallback;
import com.creative.cms.beans.domestic.dao.BSNLSouthWestCallbackDao;
import com.creative.cms.beans.domestic.service.BSNLSouthWestCallbackService;

@Service
public class BSNLSouthWestCallbackServiceImpl implements BSNLSouthWestCallbackService{
	
	private BSNLSouthWestCallbackDao bsnlSouthWestCallbackDao;

	public void setBsnlSouthWestCallbackDao(BSNLSouthWestCallbackDao bsnlSouthWestCallbackDao) {
		this.bsnlSouthWestCallbackDao = bsnlSouthWestCallbackDao;
	}

	@Override
	@Transactional
	public void saveOperatorCallback(BSNLSouthWestCallback p) {
		this.bsnlSouthWestCallbackDao.saveOperatorCallback(p);
	}
	
	

}
