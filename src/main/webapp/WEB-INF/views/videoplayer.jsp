<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>English Seekho</title>
    <link rel="stylesheet" href="https://releases.flowplayer.org/7.1.2/skin/skin.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/flowplayer.background.css">

    <style>
    #content, #content2 {
      max-width: 900px;
      margin: 2em auto;
      position: relative;
      padding: 1em;
      box-sizing: border-box;
    }
    </style>

    <script src="https://releases.flowplayer.org/7.1.2/flowplayer.min.js"></script>
    <script src="https://releases.flowplayer.org/hlsjs/flowplayer.hlsjs.light.min.js"></script>
    <script src="${pageContext.request.contextPath }/resources/js/flowplayer.background.js"></script>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="English, Lean, speak, seekho, education, entertainment" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>

<jsp:include page="common/commonheaderlinks.jsp"></jsp:include>
  </head>
  
  
  <body class="sticky-header left-side-collapsed" onload="initMap()">
	<section>
		<div class="left-side sticky-left-side">
			<div class="logo">
				<h1>
					<a href="${pageContext.request.contextPath }/">Learn
			<span>English</span>
</a>
				</h1>
			</div>
			<div class="logo-icon text-center">
				<a href="${pageContext.request.contextPath }/"><img alt="home" src="${logo }" height="50" width="50">  </a>
			</div>
			<jsp:include page="common/leftmenu.jsp"></jsp:include>
		</div>
		<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog facebook" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="app-grids">
							<div class="app">
								<div class="col-md-5 app-left mpl">
									<h3>English Seekho</h3>
									<p>Download and Avail Special Songs Videos and Audios.</p>
									<div class="app-devices">
										<h5>Gets the app from</h5>
										<a href="#"><img src="${pageContext.request.contextPath }/resources/images/1.png" alt=""></a> <a
											href="#"><img src="${pageContext.request.contextPath }/resources/images/2.png" alt=""></a>
										<div class="clearfix"></div>
									</div>
								</div>
								<div class="col-md-7 app-image">
									<img src="${pageContext.request.contextPath }/resources/images/apps.png" alt="">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
 
    
    <div class="main-content">
			<!-- header-starts -->
			<div class="header-section">
				<!--toggle button start-->
				<a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->
				<!--notification menu start -->
				<div class="clearfix"></div>
			</div>
			<div id="page-wrapper">
				<div class="inner-content">
					<div class="music-left">
						<!--banner-section-->
						<div class="banner-section">
							<div class="banner">
								<div class="callbacks_container">
									<ul class="rslides callbacks callbacks1" id="slider4">
									<c:forEach items="${banners }" var="banner" varStatus="cnt">
										<li>
											<div class="banner-img">
												<img src="${banner.bannerpath }" class="img-responsive" alt="">
											</div>
											<div class="banner-info">
											</div>
										</li>
										</c:forEach>
									</ul>
								</div>
								<!--banner-->
								<div class="clearfix"></div>
								 <c:set var="playvideo" value="${videourl }" scope="page" />
								 <c:set var="title" value="${title }" scope="page" />
  <br><br>
    <div id="content">
      <div id="player"></div>
    </div>
    <div id="content2">
    </div>
    <script>
   // var leagueCode=playvideo.videourl;
    flowplayer('#player', {
      ratio: 5/12,
      background: false,
      autoBuffering: true ,
      muted: false, // issue #7

      //splash: true,
      clip: {
        title: 'English Seekho',
        sources: [/* {
          type: 'application/x-mpegurl',
          src: 'https://edge.flowplayer.org/bauhaus.m3u8'
        },  */{
          type: 'video/mp4',
          src: '${playvideo}'
        }]
      }
    });
    </script>
    
								<script src="${pageContext.request.contextPath }/resources/js/responsiveslides.min.js"></script>
								<script>
									// You can also use "$(window).load(function() {"
									$(function() {
										// Slideshow 4
										$("#slider4")
												.responsiveSlides(
														{
															auto : true,
															pager : true,
															nav : true,
															speed : 500,
															namespace : "callbacks",
															before : function() {
																$('.events')
																		.append(
																				"<li>before event fired.</li>");
															},
															after : function() {
																$('.events')
																		.append(
																				"<li>after event fired.</li>");
															}
														});

									});
								</script>
								<div class="clearfix"></div>
							</div>
						</div>
						<c:forEach items="${categories }" var="category" varStatus="cnt">
						<c:if test="${category.categoryname == 'Popular' }">
						<div class="albums">
								<div class="tittle-head">
									<h3 class="tittle">
										<!-- New Releases <span class="new">New</span> -->
										${category.categoryname }
									</h3>
									<a href="${pageContext.request.contextPath }/">
									<h4 class="tittle">See all</h4></a>
									<div class="clearfix"></div>
								</div>
							
							<c:forEach items="${contentCategoryMapBeans }" var="category1" varStatus="cnt1">
							<div class="col-md-3 content-grid">
<a href="${pageContext.request.contextPath }/videoplay?videourl=${category1.contentvideopath }">Lesson No. ${cnt1.index+1 }<img src="${pageContext.request.contextPath }/resources/images/newpics/${ cnt1.index+1 }.gif" title="allbum-name">
								</a>							
</div>
							</c:forEach>
							<br>
							</div>
							</c:if>
						</c:forEach>
					</div>
					<div class="music-right">
						<!--/video-main--><!--  preload="metadata" -->
						<!-- script for play-list -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
    
		<footer>
			<p>
				&copy 2018 Creative Antenna. All Rights Reserved | Associated with Vodafone
			</p>
			<p><a href="http://live.vodafone.in"> Click to Visit Vodafone Live</span></p>
		</footer>
	</section>
	<jsp:include page="common/commonfooterscripts.jsp"></jsp:include>
</body>
</html>
