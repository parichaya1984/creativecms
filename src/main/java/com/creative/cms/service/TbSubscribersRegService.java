package com.creative.cms.service;

import com.creative.cms.beans.TbSubscribersReg;

public interface TbSubscribersRegService { 
	
	public TbSubscribersReg getUserInfo(String msisdn);

}
