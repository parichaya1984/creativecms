package com.creative.bsnlbulkmis.service;

import java.util.List;

import com.creative.bsnlbulkmis.beans.BannerBean;

public interface BannerBeanService {
	
	public List<BannerBean> getBannerListByOperator(int opsid, String operatorname);
	public void insertBrowsingDetails(BannerBean p);

}
