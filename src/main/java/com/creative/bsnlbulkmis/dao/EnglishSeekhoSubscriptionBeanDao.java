package com.creative.bsnlbulkmis.dao;

import java.util.List;

import com.creative.bsnlbulkmis.beans.EnglishSeekhoSubscriptionBean;

public interface EnglishSeekhoSubscriptionBeanDao {
	
	public void add(EnglishSeekhoSubscriptionBean entity) ;
    public void saveOrUpdate(EnglishSeekhoSubscriptionBean entity) ;
    public void update(EnglishSeekhoSubscriptionBean entity) ;
    public void remove(EnglishSeekhoSubscriptionBean entity);
    public EnglishSeekhoSubscriptionBean find(Integer key);
    public List<EnglishSeekhoSubscriptionBean> getAll() ;

}
