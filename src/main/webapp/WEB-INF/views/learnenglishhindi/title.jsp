<%@ page contentType="text/html;charset=UTF-8" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!doctype html>
<html lang="en">
<head>
  <title><spring:message code="englishseekho.hindi.title" text="Learn English" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="My Play Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Bootstrap  css stylesheet-->
    <link href="${pageContext.request.contextPath }/resources/learnenglishnew/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- our customised css stylesheet-->
<link rel="stylesheet" href="${pageContext.request.contextPath }/resources/learnenglishnew/css/style.css" type="text/css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>


<div class="container example2">

<div class="navbar-header"">
<div class="panel-body"><img src="${pageContext.request.contextPath }/resources/learnenglishnew/images/vodafonelogo.png/" title="" alt="" class="img-responsive" style="width:20%" align="left"/>

<div class="panel-body"><img src="${pageContext.request.contextPath }/resources/learnenglishnew/images/creativelogo.png/" title="" alt="" class="img-responsive" style="width:30%" align="right"/>
<br><br><br>
<div class="panel-body"><img src="${pageContext.request.contextPath }/resources/learnenglishnew/images/vodafone_1.png/" title="" alt="" class="img-responsive" style="width:55%" align="center">
</div>
</div>
</div>
</div>

<div class="col-md-6 col-xs-4 ser1">
<h3 style="font-family: verdana; font-weight: bold; font-size: 16px;">
<spring:message code="englishseekho.hindi.lessons" text="Lessons" /></h3>
</div>

</div>

</body>
</html>