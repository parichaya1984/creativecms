package com.creative.cms.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_iran_otp")
public class TbIranOtp {
	 
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="msisdn")private String msisdn;
	@Column(name="msg")private String msg;
	@Column(name="otp")private String otp;
	@Column(name="create_time")private Date create_time;
	@Column(name="send")private int send;
	@Column(name="used")private int used;
	@Column(name="request_soap_message")private String request_soap_message;
	@Column(name="response_soap_message")private String response_soap_message;
	@Column(name="mt_message_send")private int mt_message_send;
	@Column(name="status")private int status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public int getSend() {
		return send;
	}
	public void setSend(int send) {
		this.send = send;
	}
	public int getUsed() {
		return used;
	}
	public void setUsed(int used) {
		this.used = used;
	}
	public String getRequest_soap_message() {
		return request_soap_message;
	}
	public void setRequest_soap_message(String request_soap_message) {
		this.request_soap_message = request_soap_message;
	}
	public String getResponse_soap_message() {
		return response_soap_message;
	}
	public void setResponse_soap_message(String response_soap_message) {
		this.response_soap_message = response_soap_message;
	}
	public int getMt_message_send() {
		return mt_message_send;
	}
	public void setMt_message_send(int mt_message_send) {
		this.mt_message_send = mt_message_send;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "TbIranOtp [id=" + id + ", msisdn=" + msisdn + ", msg=" + msg + ", otp=" + otp + ", create_time="
				+ create_time + ", send=" + send + ", used=" + used + ", request_soap_message=" + request_soap_message
				+ ", response_soap_message=" + response_soap_message + ", mt_message_send=" + mt_message_send
				+ ", status=" + status + "]";
	}
	
	

}
