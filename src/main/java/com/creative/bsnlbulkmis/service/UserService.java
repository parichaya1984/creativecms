package com.creative.bsnlbulkmis.service;

import com.creative.bsnlbulkmis.beans.User;

public interface UserService {
	
	public User getUserByIdAndPassword(String userid, String password);

}
