package com.creative.bsnlbulkmis.dao;

import java.util.List;

import com.creative.bsnlbulkmis.beans.LogoBean;

public interface LogoBeanDao {
	
	public List<LogoBean> getAppInfo(String operatorname);

}
