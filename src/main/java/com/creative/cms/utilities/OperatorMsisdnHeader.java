package com.creative.cms.utilities;


public enum OperatorMsisdnHeader {

	BSNL_NORTH(MConstants.BSNLNORTH_OPERATOR_ID,"MSISDN",false),
	BSNL_EAST(MConstants.BSNLEAST_OPERATOR_ID,"MSISDN",false),
	BSNL_WEST(MConstants.BSNLWEST_OPERATOR_ID,"x-msisdn",false),
	BSNL_SOUTH(MConstants.BSNLSOUTH_OPERATOR_ID,"x-msisdn",false),	
	IDEA(MConstants.IDEA_OPERATOR_ID,"X-Nokia-msisdn",true),
	VODAFONE(MConstants.VODA_OPERATOR_ID,"x-msisdn",true);
	
	OperatorMsisdnHeader(int opId,String msisdnHeaderName,boolean opIdentifierHeader){
		this.opId=opId;
		this.msisdnHeaderName=msisdnHeaderName;
		this.opIdentifierHeader= opIdentifierHeader;
	}
	
	private int opId;
	private String msisdnHeaderName;
	private boolean opIdentifierHeader;
	
	public int getOpId() {
		return opId;
	}
	public void setOpId(int opId) {
		this.opId = opId;
	}
	public String getMsisdnHeaderName() {
		return msisdnHeaderName;
	}
	public void setMsisdnHeaderName(String msisdnHeaderName) {
		this.msisdnHeaderName = msisdnHeaderName;
	}
	public boolean isOpIdentifierHeader() {
		return opIdentifierHeader;
	}
	public void setOpIdentifierHeader(boolean opIdentifierHeader) {
		this.opIdentifierHeader = opIdentifierHeader;
	}
	
}
