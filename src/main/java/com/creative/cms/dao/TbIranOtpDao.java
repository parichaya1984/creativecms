package com.creative.cms.dao;

import com.creative.cms.beans.TbIranOtp;
import com.creative.cms.beans.TbSubscribersReg;

public interface TbIranOtpDao {
	
	public TbIranOtp getUserInfo(String msisdn);

}
