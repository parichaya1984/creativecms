package com.creative.bsnlbulkmis;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.creative.bsnlbulkmis.beans.AppBean;
import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;
import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;
import com.creative.bsnlbulkmis.beans.EnglishSeekhoContentBean;
import com.creative.bsnlbulkmis.beans.LoginBean;
import com.creative.bsnlbulkmis.beans.LogoBean;
import com.creative.bsnlbulkmis.service.AppBeanService;
import com.creative.bsnlbulkmis.service.BannerBeanService;
import com.creative.bsnlbulkmis.service.CategoryAppInfoBeanService;
import com.creative.bsnlbulkmis.service.ContentCategoryMapBeanService;
import com.creative.bsnlbulkmis.service.EnglishSeekhoContentBeanService;
import com.creative.bsnlbulkmis.service.EnglishSeekhoSubscriptionBeanService;
import com.creative.bsnlbulkmis.service.LogoBeanService;
import com.creative.bsnlbulkmis.service.UserService;

@Controller
public class VodafoneEnglishSeekhoControllerMultilanguage {
	
	private static final Logger logger = LoggerFactory.getLogger(VodafoneEnglishSeekhoControllerMultilanguage.class);
	
	private BannerBeanService bannerBeanService;
	private AppBeanService appBeanService;
	private LogoBeanService logoBeanSerivce;
	private CategoryAppInfoBeanService categoryAppInfoBeanService;
	private ContentCategoryMapBeanService contentCategoryMapBeanService;
	private EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService;
	private EnglishSeekhoContentBeanService englishSeekhoContentBeanService;
	
	
	@Autowired
	@Qualifier(value="englishSeekhoContentBeanService")
	public void setEnglishSeekhoContentBeanService(EnglishSeekhoContentBeanService englishSeekhoContentBeanService) {
		this.englishSeekhoContentBeanService = englishSeekhoContentBeanService;
	}


	@Autowired
	@Qualifier(value="englishSeekhoSubscriptionBeanService")
	public void setEnglishSeekhoSubscriptionBeanService(
			EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService) {
		this.englishSeekhoSubscriptionBeanService = englishSeekhoSubscriptionBeanService;
	}


	@Autowired
	@Qualifier(value="contentCategoryMapBeanService")
	public void setContentCategoryMapBeanService(ContentCategoryMapBeanService contentCategoryMapBeanService) {
		this.contentCategoryMapBeanService = contentCategoryMapBeanService;
	}


	@Autowired
	@Qualifier(value="categoryAppInfoBeanService")
	public void setCategoryAppInfoBeanService(CategoryAppInfoBeanService categoryAppInfoBeanService) {
		this.categoryAppInfoBeanService = categoryAppInfoBeanService;
	}


	@Autowired
	@Qualifier(value="logoBeanService")
	public void setLogoBeanSerivce(LogoBeanService logoBeanSerivce) {
		this.logoBeanSerivce = logoBeanSerivce;
	}


	@Autowired
	@Qualifier(value="bannerBeanService")
	public void setBannerBeanService(BannerBeanService bannerBeanService) {
		this.bannerBeanService = bannerBeanService;
	}
	
	
	@Autowired
	@Qualifier(value="appBeanService")
	public void setAppBeanService(AppBeanService appBeanService) {
		this.appBeanService = appBeanService;
	}



	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, ModelAndView model, Model m,
			HttpServletRequest request, HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		        
			Map<String, String> map = new HashMap<String, String>();
	
	        Enumeration headerNames = request.getHeaderNames();
	        while (headerNames.hasMoreElements()) {
	            String key = (String) headerNames.nextElement();
	            String value = request.getHeader(key);
	            map.put(key, value);
	            logger.info("Key-->"+key+"|Value-->"+value);
	        }
		        
		        String appname = request.getParameter("appname");
		        String operatorid = request.getParameter("operatorid");
		        String operatorname = request.getParameter("operatorname");
		        String sappid = request.getParameter("appid");
		        int appid = -1;
		        if(StringUtils.isEmpty(sappid)){
		        	appid = -1;
		        	appname = "Vodafone";
		        }else{
		        	appid = Integer.parseInt(sappid);
		        }
		        if(StringUtils.isEmpty(appname)){
		        	operatorid = "0";
		        	operatorname = "Demo";
		        	appid = 2;
		        }else{
		        	operatorid = "1";
		        	operatorname = "Vodafone";
		        	appid= 1;
		        }

		        System.out.println("Details....."+this.englishSeekhoContentBeanService.getAll());
		
		        List<EnglishSeekhoContentBean> englishSeekhoContentBeans = this.englishSeekhoContentBeanService.getAll();
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		List<BannerBean>  bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid), operatorname);
		logger.info("List of Banners::"+bannerList);
		
		m.addAttribute("serverTime", formattedDate );
		m.addAttribute("banners", bannerList);
		
		
		List<AppBean>  appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::"+appList);
		
		List<LogoBean>  logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::"+logoList);
		
		List<CategoryAppInfoBean>  categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::"+categoryAppInfoBeans);
		
		
		List<ContentCategoryMapBean>  contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid, operatorname);
		logger.info("contentCategoryMapBeans::"+contentCategoryMapBeans);
		
		if(appList != null){
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		}else{
			m.addAttribute("appname", "Learn English");
			m.addAttribute("logo", "");
				
		}
		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		m.addAttribute("englishSeekhoContentBeans", englishSeekhoContentBeans);
		
		return new ModelAndView("learnenglishhindi/newindex");
	}
	
	@RequestMapping(value = "/newsingle", method = RequestMethod.GET)
	public ModelAndView getSinglePage(Locale locale, ModelAndView model, Model m) {
		return new ModelAndView("single");
	}
	
	@RequestMapping(value = "/newcallback", method = RequestMethod.GET)
	public ModelAndView callback(Locale locale, ModelAndView model, Model m, HttpServletRequest request, HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
        LoginBean loginBean = new LoginBean();
        model.addObject("loginBean", loginBean);
        
        Map<String, String> map = new HashMap<String, String>();
    	
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
            logger.info("Key-->"+key+"|Value-->"+value);
        }
        
        //String query = request.getQueryString()+"&appid=1";
        
        String appname = request.getParameter("appname");
        String operatorid = request.getParameter("operatorid");
        String operatorname = request.getParameter("operatorname");
        String sappid = request.getParameter("appid");
        int appid = -1;
        /*if(StringUtils.isEmpty(sappid)){
        	appid = -1;
        	appname = "Vodafone";
        }else{
        	appid = Integer.parseInt(sappid);
        }
        if(StringUtils.isEmpty(appname)){
        	operatorid = "0";
        	operatorname = "Demo";
        	appid = 2;
        }else{*/
        	operatorid = "1";
        	operatorname = "Vodafone";
        	appid= 1;
       // }
        	 String responseStr = request.getParameter("response");
        	 
        	 //"RESPONSE_STATUS:900:"+txId+":Success"
        	 String msg = "";
        	 int retVal = 0; 
        	 if(responseStr.contains("800")){
        		 msg = "You have Successfully Subscribed to English Seekho Service";
        		 retVal = 800;
        	 }else if(responseStr.contains("802")){
        		 msg = "You have Successfully Un-Subscribed to English Seekho Service";
        		 retVal = 802;
        	 }else if(responseStr.contains("900")){
        		 msg = "Your Request is Submitted Successfully.";
        		 retVal = 900;
        	 }else{
        		 msg = "Demo Service";
        		 retVal = 0;
        	 }
        	 
        	 /**
        	  * MSISDN=919610530767
        	  * &SERVICE_ID=CRE_ENGSIKOD
        	  * &CLASS=CRE_ENGSIKOD
        	  * &TXNID=1085 3904 8802
        	  * &ACTION=ACT
        	  * &STATUS=SUCCESS
        	  * &CHARGING_MODE=1DAYS3
        	  * &REQ_MODE=IMI_WAP
        	  * &REQUEST_ID=6365 9833 7309 7786 0191 9610 5307 67
        	  * &CIRCLE_ID=9
        	  * &SUB_TYPE=PREPAID        	
        	  */

Date date = new Date();
DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

String formattedDate = dateFormat.format(date);

List<BannerBean>  bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid), operatorname);
logger.info("List of Banners::"+bannerList);

m.addAttribute("serverTime", formattedDate );
m.addAttribute("banners", bannerList);


List<AppBean>  appList = this.appBeanService.getAppInfo(appid, operatorname);
logger.info("Applist::"+appList);

List<LogoBean>  logoList = this.logoBeanSerivce.getAppInfo(operatorname);
logger.info("logoList::"+logoList);

List<CategoryAppInfoBean>  categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
logger.info("categoryAppInfoBeans::"+categoryAppInfoBeans);


List<ContentCategoryMapBean>  contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid, operatorname);
logger.info("contentCategoryMapBeans::"+contentCategoryMapBeans);

if(appList != null){
	m.addAttribute("appname", appList.get(0).getAppname());
	m.addAttribute("logo", logoList.get(0).getLogopath());
}else{
	m.addAttribute("appname", "Learn English");
	m.addAttribute("logo", "");
		
}
m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
m.addAttribute("categories", categoryAppInfoBeans);
m.addAttribute("msg", msg);
m.addAttribute("retval", retVal);
return new ModelAndView("newindex");
}
	

	@RequestMapping(value = "/newppucallback", method = RequestMethod.GET)
	public ModelAndView getRegPPUCallback(Locale locale, ModelAndView model, Model m, HttpServletRequest req, HttpServletResponse response) {
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		return new ModelAndView("success");
	}
	
	@RequestMapping(value = "/newvideoplay", method = RequestMethod.GET)
	public ModelAndView getPlayVideo(Locale locale, ModelAndView model, Model m, 
			HttpServletRequest req, HttpServletResponse response) {
		
		
		String appname = req.getParameter("appname");
        String operatorid = req.getParameter("operatorid");
        String operatorname = req.getParameter("operatorname");
        String sappid = req.getParameter("appid");
        int appid = -1;
        if(StringUtils.isEmpty(sappid)){
        	appid = -1;
        	appname = "Vodafone";
        }else{
        	appid = Integer.parseInt(sappid);
        }
        if(StringUtils.isEmpty(appname)){
        	operatorid = "0";
        	operatorname = "Demo";
        	appid = 2;
        }else{
        	operatorid = "1";
        	operatorname = "Vodafone";
        	appid= 1;
        }


			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			
			String formattedDate = dateFormat.format(date);

		
			List<BannerBean>  bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid), operatorname);
		logger.info("List of Banners::"+bannerList);
		
		m.addAttribute("serverTime", formattedDate );
		m.addAttribute("banners", bannerList);
		
		
		List<AppBean>  appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::"+appList);
		
		List<LogoBean>  logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::"+logoList);
		
		List<CategoryAppInfoBean>  categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::"+categoryAppInfoBeans);
		
		
		List<ContentCategoryMapBean>  contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid, operatorname);
		logger.info("contentCategoryMapBeans::"+contentCategoryMapBeans);
		
		m.addAttribute("appname", appList.get(0).getAppname());
		m.addAttribute("logo", logoList.get(0).getLogopath());
		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		
		String videoUrl= req.getParameter("videourl");
		String contenttitle2= req.getParameter("contenttitle2");
		
		logger.info("Video URL::"+videoUrl);
		String videotitle = videoUrl.split(".mp4")[0].split("/")[8];
		System.out.println("Video Title --> "+ videotitle);
		//model.addObject("videourl", videoUrl);
		m.addAttribute("videourl", videoUrl);
		m.addAttribute("videotitle", videotitle);
		m.addAttribute("contenttitle2", contenttitle2);
		//m.addAttribute("title",);
		
		
		return new ModelAndView("learnenglishhindi/videoplayer");
	}
	
	
	@RequestMapping(value = "/newnewenglish", method = RequestMethod.GET)
	public ModelAndView getRegnew(Locale locale, ModelAndView model, Model m, HttpServletRequest req, HttpServletResponse response) {
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		return new ModelAndView("themes/playrabbitsbsnl/WelcomePage");
	}
	
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/newhindi", method = RequestMethod.GET)
	public ModelAndView gethindi(Locale locale, ModelAndView model, Model m, HttpServletRequest req, HttpServletResponse response) {
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		//locale.setDefault(Locale.forLanguageTag("hi_IN"));
		System.out.println("Get locale-->"+locale.getDefault());
		Locale[] localeList = NumberFormat.getAvailableLocales();
		for(int i=0;i<localeList.length;i++){
			System.out.println("Localename -->"+localeList[i].toLanguageTag()+";; Country::"+localeList[i].getCountry());
		}
		System.out.println("List of Lnaguages-->"+localeList.toString());
		return new ModelAndView("themes/playrabbitsbsnl/languageCheck");
	}
	
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/newmarathi", method = RequestMethod.GET)
	public ModelAndView getRegmarathi(Locale locale, ModelAndView model, Model m, HttpServletRequest req, HttpServletResponse response) {
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		locale.setDefault(new Locale("mr_IN"));
		System.out.println("Get marathilocale-->"+locale.getDefault());
		//locale.
		return new ModelAndView("themes/playrabbitsbsnl/languageCheck");
	}
	
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/newverbs", method = RequestMethod.GET)
	public ModelAndView getVerbs(Locale locale, ModelAndView model, Model m, HttpServletRequest req, HttpServletResponse response) {
		logger.info("Request URL::"+ req.getRequestURL());
		logger.info("Request String::"+ req.getQueryString());
		locale.setDefault(new Locale("mr_IN"));
		System.out.println("Get marathilocale-->"+locale.getDefault());
		//locale.
		return new ModelAndView("themes/playrabbitsbsnl/languageCheck");
	}

}
