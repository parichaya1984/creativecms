<title>games boots</title>

<!-- For-Mobile-Apps-and-Meta-Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Custom Theme files -->
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all">

<!--script-->
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- slide-touch-plugin -->
<script src="js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $(".owl-carousel").owlCarousel({

            autoPlay: 3000, //Set AutoPlay to 3 seconds
            autoPlay : true,
            navigation :true,

            items: 6,
            itemsDesktop : [1000,6], // 2 items between 1000px and 901px
            itemsDesktopSmall : [900,5], // betweem 900px and 601px
            itemsTablet: [700,4], // 2 items between 600 and 480
            itemsMobile : [480,3] , // 1 item between 480 and 0

        });
    });
</script>
<!-- //slide-touch-plugin -->

<script src="js/jquery.imglazy.js"></script>
<script type="text/javascript">
    jQuery(function($)
    {
        $.imgLazy({
            effect: 'fadeIn',
            viewport: true,
            threshold: 20000
        });
    });
</script>
