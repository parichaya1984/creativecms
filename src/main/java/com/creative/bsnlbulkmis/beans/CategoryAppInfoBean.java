package com.creative.bsnlbulkmis.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_category_app_info")
public class CategoryAppInfoBean {
	
	@Id 
	@Column(name="categoryappinfoid")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int categoryappinfoid;
	
	@Column(name="categoryid")
	private int categoryid;
	@Column(name="appid")
	private int appid;
	@Column(name="appname")
	private String appname;
	@Column(name="categoryname")
	private String categoryname;
	@Column(name="creationdate")
	private Date creationdate;
	@Column(name="categorystatus")
	private int categorystatus;
	public int getCategoryappinfoid() {
		return categoryappinfoid;
	}
	public void setCategoryappinfoid(int categoryappinfoid) {
		this.categoryappinfoid = categoryappinfoid;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public int getAppid() {
		return appid;
	}
	public void setAppid(int appid) {
		this.appid = appid;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public String getCategoryname() {
		return categoryname;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	public Date getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
	public int getCategorystatus() {
		return categorystatus;
	}
	public void setCategorystatus(int categorystatus) {
		this.categorystatus = categorystatus;
	}
	@Override
	public String toString() {
		return "CategoryAppInfoBean [categoryappinfoid=" + categoryappinfoid + ", categoryid=" + categoryid + ", appid="
				+ appid + ", appname=" + appname + ", categoryname=" + categoryname + ", creationdate=" + creationdate
				+ ", categorystatus=" + categorystatus + "]";
	}
	
	
	
	

}
