package com.creative.bsnlbulkmis.dao.impl;

import org.springframework.stereotype.Repository;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.creative.bsnlbulkmis.beans.EnglishSeekhoSubscriptionBean;
import com.creative.bsnlbulkmis.dao.EnglishSeekhoSubscriptionBeanDao;

@Repository
public class EnglishSeekhoSubscriptionBeanDaoImpl implements EnglishSeekhoSubscriptionBeanDao{
	
	private static final Logger logger = LoggerFactory.getLogger(EnglishSeekhoSubscriptionBeanDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public List<EnglishSeekhoSubscriptionBean> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<EnglishSeekhoSubscriptionBean> adnetworkOperatorConfigBeans = session.createQuery("from EnglishSeekhoSubscriptionBean").list();
		for(EnglishSeekhoSubscriptionBean p :  adnetworkOperatorConfigBeans){
			logger.info("EnglishSeekhoSubscriptionBean List::"+p);
		}
		return adnetworkOperatorConfigBeans;
	}

	@Override
	public void add(EnglishSeekhoSubscriptionBean entity) {
		//AdnetworksBean abc = AdnetworksBean.class.cast(a);
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(entity);
		
	}


	@Override
	public void saveOrUpdate(EnglishSeekhoSubscriptionBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(EnglishSeekhoSubscriptionBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(EnglishSeekhoSubscriptionBean entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EnglishSeekhoSubscriptionBean find(Integer key) {
		// TODO Auto-generated method stub
		return null;
	}

}
