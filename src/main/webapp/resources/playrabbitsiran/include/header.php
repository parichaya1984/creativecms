<header  class="header-down">

    <?php if
    (basename($_SERVER['PHP_SELF']) == 'index.php') {
        ?>

        <div class="top-nav">

        <span class="menu">
<!--               <i class="fa fa-bars fa-2x" aria-hidden="true" onclick="loadDoc()"></i>-->
            <img class="fa-bars" src="images/menu.png" aria-hidden="true" onclick="loadDoc()">
        </span>

            <ul>
                <a href="index.php"><li>Home</li></a>
                <a href="listing.php"><li>action</li></a>
                <a href="listing.php"><li>puzzle</li></a>
                <a href="listing.php"><li>race</li></a>
                <a href="index.php"><li>my account</li></a>
            </ul>

            <!--script-->
            <script>
                $("span.menu").click(function(){

                    $(".top-nav ul").slideToggle(500, function(){
                    });
                });

                function loadDoc() {

                    $( ".fa-bars" ).toggleClass( "menurotate" );
                }

            </script>

        </div>

        <?php
    }  else {
        ?>

        <div class="back-content-download">
            <a href="javascript:history.back();">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
        </div>

        <?php
    }
    ?>

    <script>
        var didScroll;
        var lastScrollTop = 0;
        var delta = 5;
        var navbarHeight = $('header').outerHeight();

        $(window).scroll(function(event){
            didScroll = true;
        });

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            if(Math.abs(lastScrollTop - st) <= delta)
                return;

            if (st > lastScrollTop && st > navbarHeight){
                $('header').removeClass('header-down').addClass('header-up');
            } else {
                if(st + $(window).height() < $(document).height()) {
                    $('header').removeClass('header-up').addClass('header-down');
                }
            }

            lastScrollTop = st;
        }
    </script>

    <div class="product-name">
       <a href="index.php"> <img src="images/logo.png"></a>
<!--        <a href="index.php"><span>gamez</span>Boost</a>-->
    </div>

    <!--language start -->
    <select class="languages">
        <option value="en" data-title="English">en</option>
        <option value="ar" data-title="العربية" selected="">ar</option>
    </select>
    <!--language end -->

    <!--***search start***-->
    <form class="searchbox">
        <input type="search" placeholder="Search......" name="search" class="searchbox-input" onkeyup="buttonUp();" required>
        <input type="submit" class="searchbox-submit" value="Go">
        <span class="searchbox-icon">
            <i class="fa fa-search" aria-hidden="true"></i>
        </span>
    </form>
    <!--search js-->
    <script>
        $(document).ready(function(){
            var submitIcon = $('.searchbox-icon');
            var inputBox = $('.searchbox-input');
            var searchBox = $('.searchbox');
            var isOpen = false;
            submitIcon.click(function(){
                if(isOpen == false){
                    searchBox.addClass('searchbox-open');
                    inputBox.focus();
                    isOpen = true;
                } else {
                    searchBox.removeClass('searchbox-open');
                    inputBox.focusout();
                    isOpen = false;
                }
            });
            submitIcon.mouseup(function(){
                return false;
            });
            searchBox.mouseup(function(){
                return false;
            });
            $(document).mouseup(function(){
                if(isOpen == true){
                    $('.searchbox-icon').css('display','block');
                    submitIcon.click();
                }
            });
        });
        function buttonUp(){
            var inputVal = $('.searchbox-input').val();
            inputVal = $.trim(inputVal).length;
            if( inputVal !== 0){
                $('.searchbox-icon').css('display','none');
            } else {
                $('.searchbox-input').val('');
                $('.searchbox-icon').css('display','block');
            }
        }
    </script>
    <!--*** search end ***-->

</header>
