package com.creative.bsnlbulkmis.service;

import java.util.List;

import com.creative.bsnlbulkmis.beans.EnglishSeekhoSubscriptionBean;

public interface EnglishSeekhoSubscriptionBeanService { 
	
	public void add(EnglishSeekhoSubscriptionBean entity) ;
    public void saveOrUpdate(EnglishSeekhoSubscriptionBean entity) ;
    public void update(EnglishSeekhoSubscriptionBean entity) ;
    public void remove(EnglishSeekhoSubscriptionBean entity);
    public EnglishSeekhoSubscriptionBean find(Integer key);
    public List<EnglishSeekhoSubscriptionBean> getAll() ;

}
