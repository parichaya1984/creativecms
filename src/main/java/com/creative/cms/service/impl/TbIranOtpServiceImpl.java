package com.creative.cms.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creative.cms.beans.TbIranOtp;
import com.creative.cms.dao.TbIranOtpDao;
import com.creative.cms.service.TbIranOtpService;

@Service
public class TbIranOtpServiceImpl implements TbIranOtpService{
	
	private TbIranOtpDao tbIranOtpDao;

	public void setTbIranOtpDao(TbIranOtpDao tbIranOtpDao) {
		this.tbIranOtpDao = tbIranOtpDao;
	}

	@Override
	@Transactional
	public TbIranOtp getUserInfo(String msisdn) {
		return this.tbIranOtpDao.getUserInfo(msisdn);
	}
	
	

}
