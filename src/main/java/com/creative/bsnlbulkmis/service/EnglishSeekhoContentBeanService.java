package com.creative.bsnlbulkmis.service;

import java.util.List;

import com.creative.bsnlbulkmis.beans.EnglishSeekhoContentBean;

public interface EnglishSeekhoContentBeanService {
	
	public void add(EnglishSeekhoContentBean entity) ;
    public void saveOrUpdate(EnglishSeekhoContentBean entity) ;
    public void update(EnglishSeekhoContentBean entity) ;
    public void remove(EnglishSeekhoContentBean entity);
    public EnglishSeekhoContentBean find(Integer key);
    public List<EnglishSeekhoContentBean> getAll() ;

}
