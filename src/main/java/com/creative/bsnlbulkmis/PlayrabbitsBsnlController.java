package com.creative.bsnlbulkmis;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.creative.bsnlbulkmis.beans.AppBean;
import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.beans.CategoryAppInfoBean;
import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;
import com.creative.bsnlbulkmis.beans.LoginBean;
import com.creative.bsnlbulkmis.beans.LogoBean;
import com.creative.bsnlbulkmis.service.AppBeanService;
import com.creative.bsnlbulkmis.service.BannerBeanService;
import com.creative.bsnlbulkmis.service.CategoryAppInfoBeanService;
import com.creative.bsnlbulkmis.service.ContentCategoryMapBeanService;
import com.creative.bsnlbulkmis.service.EnglishSeekhoSubscriptionBeanService;
import com.creative.bsnlbulkmis.service.LogoBeanService;
import com.creative.bsnlbulkmis.service.UserService;
import com.creative.cms.beans.TbIranOtp;
import com.creative.cms.beans.TbSubscribersReg;
import com.creative.cms.beans.domestic.BSNLSouthWestCallback;
import com.creative.cms.beans.domestic.TbSubscribersRegDomestic;
import com.creative.cms.beans.domestic.service.BSNLSouthWestCallbackService;
import com.creative.cms.beans.domestic.service.TbSubscribersRegDomesticService;
import com.creative.cms.service.TbIranOtpService;
import com.creative.cms.service.TbSubscribersRegService;
import com.creative.cms.utilities.BSNLPremiumGamesServiceID;
import com.creative.cms.utilities.BSNLSubscriptionCallbackTemplates;
import com.creative.cms.utilities.CommonUtilities;
import com.creative.cms.utilities.ConstantUtils;
import com.creative.cms.utilities.EncryptDecryptUtility;
import com.creative.cms.utilities.MUtility;
import com.creative.cms.utilities.OperatorMsisdnHeader;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64; 

@Controller
public class PlayrabbitsBsnlController { 

	private static final Logger logger = LoggerFactory.getLogger(PlayrabbitsBsnlController.class);

	private BannerBeanService bannerBeanService;
	private AppBeanService appBeanService;
	private LogoBeanService logoBeanSerivce; 
	private CategoryAppInfoBeanService categoryAppInfoBeanService;
	private ContentCategoryMapBeanService contentCategoryMapBeanService;
	private EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService;
	private BSNLSouthWestCallbackService bSNLSouthWestCallbackService;
	
	private TbSubscribersRegService tbSubscribersRegService;
	 
	private TbIranOtpService tbIranOtpService;

	private TbSubscribersRegDomesticService tbSubscribersRegDomesticService;
	
	
	
	@Autowired
	@Qualifier(value = "tbSubscribersRegDomesticService")
	public void setTbSubscribersRegDomesticService(TbSubscribersRegDomesticService tbSubscribersRegDomesticService) {
		this.tbSubscribersRegDomesticService = tbSubscribersRegDomesticService;
	}
	
	
	
	@Autowired
	@Qualifier(value = "bSNLSouthWestCallbackService")
	public void setbSNLSouthWestCallbackService(BSNLSouthWestCallbackService bSNLSouthWestCallbackService) {
		this.bSNLSouthWestCallbackService = bSNLSouthWestCallbackService;
	}

	

	@Autowired
	@Qualifier(value = "tbIranOtpService")
	public void setTbIranOtpService(TbIranOtpService tbIranOtpService) {
		this.tbIranOtpService = tbIranOtpService;
	}

	@Autowired
	@Qualifier(value = "tbSubscribersRegService")
	public void setTbSubscribersRegService(TbSubscribersRegService tbSubscribersRegService) {
		this.tbSubscribersRegService = tbSubscribersRegService;
	}

	@Autowired
	@Qualifier(value = "englishSeekhoSubscriptionBeanService")
	public void setEnglishSeekhoSubscriptionBeanService(
			EnglishSeekhoSubscriptionBeanService englishSeekhoSubscriptionBeanService) {
		this.englishSeekhoSubscriptionBeanService = englishSeekhoSubscriptionBeanService;
	}

	@Autowired
	@Qualifier(value = "contentCategoryMapBeanService")
	public void setContentCategoryMapBeanService(ContentCategoryMapBeanService contentCategoryMapBeanService) {
		this.contentCategoryMapBeanService = contentCategoryMapBeanService;
	}

	@Autowired
	@Qualifier(value = "categoryAppInfoBeanService")
	public void setCategoryAppInfoBeanService(CategoryAppInfoBeanService categoryAppInfoBeanService) {
		this.categoryAppInfoBeanService = categoryAppInfoBeanService;
	}

	@Autowired
	@Qualifier(value = "logoBeanService")
	public void setLogoBeanSerivce(LogoBeanService logoBeanSerivce) {
		this.logoBeanSerivce = logoBeanSerivce;
	}

	@Autowired
	@Qualifier(value = "bannerBeanService")
	public void setBannerBeanService(BannerBeanService bannerBeanService) {
		this.bannerBeanService = bannerBeanService;
	}

	@Autowired
	@Qualifier(value = "appBeanService")
	public void setAppBeanService(AppBeanService appBeanService) {
		this.appBeanService = appBeanService;
	}
	
	@RequestMapping(value = "/playrabbitsbsnl/sendOtp", method = RequestMethod.GET)
	public ModelAndView getUserVerification(Locale locale, ModelAndView model, Model m, HttpServletRequest request,
			HttpServletResponse response) {
		
		logger.info("Welcome home! The client locale is {}.", locale);
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
			logger.info("Key-->" + key + "|Value-->" + value);
		}

		String appname = request.getParameter("appname");
		String operatorid = request.getParameter("operatorid");
		String operatorname = request.getParameter("operatorname");
		String sappid = request.getParameter("appid");
		String msisdn = request.getParameter("msisdn");
		String otp = request.getParameter("otp");
		
		int appid = -1;
		if (StringUtils.isEmpty(sappid)) {
			appid = 3;
			appname = "Play Rabbits";
		} else {
			appid = Integer.parseInt(sappid);
		}
		if (StringUtils.isEmpty(appname)) {
			operatorid = "0";
			operatorname = "Demo Play Rabbits";
			appid = 3;
		} else {
			operatorid = "1";
			operatorname = "Iran";
			appid = 3;
		}

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		List<BannerBean> bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid),
				operatorname);
		logger.info("List of Banners::" + bannerList);

		m.addAttribute("serverTime", formattedDate);
		m.addAttribute("banners", bannerList);
		
		List<AppBean> appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::" + appList);

		List<LogoBean> logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::" + logoList);


		List<CategoryAppInfoBean> categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::" + categoryAppInfoBeans);

		List<ContentCategoryMapBean> contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid,
				operatorname);
		logger.info("contentCategoryMapBeans::" + contentCategoryMapBeans);

		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		if (appList != null) {
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		} else {
			m.addAttribute("appname", "Play Rabbits");
			m.addAttribute("logo", "");

		}
		
		m.addAttribute("otppage", "2");
		m.addAttribute("msisdn", msisdn);
		//Write OTP Code here
		
		//Calling Http URL from here.
		CommonUtilities cu = new CommonUtilities();
		int retVal 	= cu.sendOtpToMsisdnIran(msisdn);
		logger.info("Otp Sent to Msisdn-->"+retVal);
		
		return new ModelAndView("themes/playrabbitsiran/home");
	}
	
	@RequestMapping(value = "/playrabbitsbsnl/validateOtp", method = RequestMethod.GET)
	public ModelAndView getOtpValidation(Locale locale, ModelAndView model, Model m, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
			logger.info("Key-->" + key + "|Value-->" + value);
		}
		
		

		String appname = request.getParameter("appname");
		String operatorid = request.getParameter("operatorid");
		String operatorname = request.getParameter("operatorname");
		String sappid = request.getParameter("appid");
		String msisdn = request.getParameter("msisdn");
		String otp = request.getParameter("otp");
		int appid = -1;
		if (StringUtils.isEmpty(sappid)) {
			appid = 3;
			appname = "Play Rabbits";
		} else {
			appid = Integer.parseInt(sappid);
		}
		if (StringUtils.isEmpty(appname)) {
			operatorid = "0";
			operatorname = "Demo Play Rabbits";
			appid = 3;
		} else {
			operatorid = "1";
			operatorname = "Iran";
			appid = 3;
		}

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		List<BannerBean> bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid),
				operatorname);
		logger.info("List of Banners::" + bannerList);

		m.addAttribute("serverTime", formattedDate);
		m.addAttribute("banners", bannerList);
		
		List<AppBean> appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::" + appList);

		List<LogoBean> logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::" + logoList);


		List<CategoryAppInfoBean> categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::" + categoryAppInfoBeans);

		List<ContentCategoryMapBean> contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid,
				operatorname);
		logger.info("contentCategoryMapBeans::" + contentCategoryMapBeans);

		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		if (appList != null) {
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		} else {
			m.addAttribute("appname", "Play Rabbits");
			m.addAttribute("logo", "");

		}
		m.addAttribute("otppage", "3");
		m.addAttribute("disablelinks", "2");
		
		//System.out.println("Secnod DB Info--->>>>>"+this.tbSubscribersRegService.getUserInfo("919896936967"));
		
		//System.out.println("Secnod DB Info--->>>>>"+this.tbSubscribersRegService.getUserInfo("919896936967"));
		
		//Validate Msisdn & Otp Logic Here msisdn
		
		TbSubscribersReg tbIranUser = this.tbSubscribersRegService.getUserInfo(msisdn);
		System.out.println("Secnod DB Info--->>>>>"+this.tbSubscribersRegService.getUserInfo(msisdn.trim()));
		if(tbIranUser!=null){
			int status = tbIranUser.getStatus();
			if(status == 1){
				// Validate OTP logic
				
				TbIranOtp tbIranOtpDetails = this.tbIranOtpService.getUserInfo(msisdn);
				
				if(tbIranOtpDetails != null){
					int otpStatus = tbIranOtpDetails.getUsed();
					if(otpStatus == 0){
						if(!StringUtils.isEmpty(otp)){
							if(otp.equalsIgnoreCase(tbIranOtpDetails.getOtp()) && msisdn.equalsIgnoreCase(tbIranUser.getMsisdn())){
								logger.info("Otp Validated.. Go to Portal");
								System.out.println("Otp Validated.. Go to Portal");
								
								//this.tbIranOtpService.
								
								return new ModelAndView("themes/playrabbitsiran/home");
							}else{
								logger.info("Invalid OTP or OTP Expired");
								System.out.println("Invalid OTP or OTP Expired");
								return new ModelAndView("themes/playrabbitsiran/home");
							}
						}
					}
				}
				
			}else{
				//rediredct: http://185.173.105.87/international/cnt/c/cmp?adid=1&cmpid=129&token=sss
				return new ModelAndView("redirect:"+ConstantUtils.REDIRECT_TO_NON_SUBSCRIBER);
			}
		}else{
			return new ModelAndView("redirect:"+ConstantUtils.REDIRECT_TO_NON_SUBSCRIBER);
		}
		
		return new ModelAndView("themes/playrabbitsiran/home");
	}

	@RequestMapping(value = "/playrabbitsbsnl", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, ModelAndView model, Model m, HttpServletRequest request,
			HttpServletResponse response) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		String msisdn = null;
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
			logger.info("Key-->" + key + "|Value-->" + value);
			if(key.equalsIgnoreCase("MSISDN")){
				msisdn = value;
			}
		}
		
		
		
		
		for(OperatorMsisdnHeader operatorMsisdnHeader:OperatorMsisdnHeader.values()){
			 msisdn=request.getHeader(operatorMsisdnHeader.getMsisdnHeaderName());
		}

		String appname = request.getParameter("appname");
		String operatorid = request.getParameter("operatorid");
		String operatorname = request.getParameter("operatorname");
		String sappid = request.getParameter("appid");
		// msisdn = request.getParameter("msisdn");
		//String otp = request.getParameter("otp");
		
		 TbSubscribersRegDomestic p =  this.tbSubscribersRegDomesticService.getUserInfo(msisdn);
		//Check Subscriber status
		 if(p!=null){
		// System.out.println("User Details-->"+p.toString()+":for MSISDN -->:"+msisdn);
		 logger.info("User Details-->"+p.toString()+":for MSISDN -->:"+msisdn);
		 }
		 
		int appid = -1;
		if (StringUtils.isEmpty(sappid)) {
			appid = 3;
			appname = "Play Rabbits";
		} else {
			appid = Integer.parseInt(sappid);
		}
		if (StringUtils.isEmpty(appname)) {
			operatorid = "0";
			operatorname = "Demo Play Rabbits";
			appid = 3;
		} else {
			operatorid = "6";
			operatorname = "BSNL";
			appid = 3;
		}

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		List<BannerBean> bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid),
				operatorname);
		logger.info("List of Banners::" + bannerList);

		m.addAttribute("serverTime", formattedDate);
		m.addAttribute("banners", bannerList);
		
		List<AppBean> appList = this.appBeanService.getAppInfo(appid, operatorname);
		logger.info("Applist::" + appList);

		List<LogoBean> logoList = this.logoBeanSerivce.getAppInfo(operatorname);
		logger.info("logoList::" + logoList);


		List<CategoryAppInfoBean> categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
		logger.info("categoryAppInfoBeans::" + categoryAppInfoBeans);

		List<ContentCategoryMapBean> contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid,
				operatorname);
		logger.info("contentCategoryMapBeans::" + contentCategoryMapBeans);

		m.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
		m.addAttribute("categories", categoryAppInfoBeans);
		
		if (appList != null) {
			m.addAttribute("appname", appList.get(0).getAppname());
			m.addAttribute("logo", logoList.get(0).getLogopath());
		} else {
			m.addAttribute("appname", "Play Rabbits");
			m.addAttribute("logo", "");

		}
		m.addAttribute("otppage", "1");
		m.addAttribute("disablelinks", "1");
		
		return new ModelAndView("themes/playrabbitsbsnl/home");
	}
	
	@RequestMapping(value={"/playrabbitsbsnl/listing"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getListingPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/listing";
	  }
	  
	  @RequestMapping(value={"/playrabbitsbsnl/details"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getDetailsPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/details";
	  }
	  
	  @RequestMapping(value={"/playrabbitsbsnl/subscription"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getSubscriptionPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/subscription";
	  }
	  
	  @RequestMapping(value={"/playrabbitsbsnl/unsubscription"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getUnSubscriptionPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/unsubscription";
	  }
	  
	  @RequestMapping(value={"/playrabbitsbsnl/tnc"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getTermsAndConditionPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/tnc";
	  }
	  
	  @RequestMapping(value={"/playrabbitsbsnl/consent"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getConsentPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsiran/consent";
	  }

	  //New Methods Started for BSNL
	  
	  @RequestMapping(value={"/playrabbitsbsnl/unlimitedgamessubscribebsnl"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public String getBsnlSubscriptionPage(Locale locale, Model model)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    return "themes/playrabbitsbsnl/subscription";
	  }
	  
	  @RequestMapping(value={"/playrabbitsbsnl/subscriptionrequest"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public ModelAndView getProcessSubscriptionPage(Locale locale, Model model, HttpServletRequest request)
	  {
	    logger.info("Welcome home! The client locale is {}.", locale);
	    
	    Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		String msisdn = null;
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
			logger.info("Key-->" + key + "|Value-->" + value);
			if(key.equalsIgnoreCase("MSISDN")){
				msisdn = value;
			}
		}
		
		
		
		
		for(OperatorMsisdnHeader operatorMsisdnHeader:OperatorMsisdnHeader.values()){
			 msisdn=request.getHeader(operatorMsisdnHeader.getMsisdnHeaderName());
		}
	    
	    //return "themes/playrabbitsbsnl/callCG";
//http://bsnlsouth.netxcell.com:6767/cvps/ConsentRequestAction?cp_id=9999&tid=12345&source=WAP&consent_source=WAP&language=ENGLISH&action=new_subscription&subscription_id=2345&msisdn=9876543212&service_id=1234567&service_name=234567&charge=9&validity=3&validity_type=DAYS&url=aHR0cDovL3RoZXBpbmQubW9iaS9qcC9icy9jb25zZW50cmV0dXJuLzQ4OTExNTA0MjQxMTQ0MDIzMDk=
	    BSNLSubscriptionCallbackTemplates services = new BSNLSubscriptionCallbackTemplates();
	    String redirecturl = Base64.encode("localhost:8081/englishseekho/playrabbitsbsnl".getBytes());
	    
	    /*String bsnlCgPage = "http://bsnlsouth.netxcell.com:6767/cvps/ConsentRequestAction?"
	    		+ "cp_id=208"
	    		+ "&tid="+services.getBSNLTID("208")
	    		+ "&source=WAP"
	    		+ "&consent_source=WAP"
	    		+ "&language=ENGLISH"
	    		+ "&action=new_subscription"
	    		+ "&subscription_id="+System.currentTimeMillis()
	    		+ "&msisdn=919444351866"
	    		+ "&service_id="+BSNLPremiumGamesServiceID.SOUTH_PREMIUM_UNLIMITED_GAMES
	    		+ "&service_name=UNLIMITED_GAMES"
	    		+ "&charge=30"
	    		+ "&validity=30"
	    		+ "&validity_type=DAYS"
	    		+ "&url="+redirecturl;
	    		*/
	    String t = System.currentTimeMillis()+"";
	    String tx = "TX"+t;
	    if(StringUtils.isEmpty(msisdn)){
	    	msisdn="919444351866";
	    }
	    String redirect = "http://creativegames.in/creativecms/playrabbitsbsnl/callback?TransId="+tx+"&serviceImage=abc.jpg";
	    //String redirect = "http://localhost:8081/englishseekho/playrabbitsbsnl/callback?TransId="+tx+"&serviceImage=abc.jpg";
	    String bsnlCgPage = "http://bsnlsouth.netxcell.com:6767/WEBWAP2/interface/cg?"
	    		+ "msisdn="+msisdn
	    		+ "&serviceid=20849749&"
	    		+ "keyword=NA"
	    		+ "&shortcode=51009"
	    		+ "&authkey=creativegames"
	    		+ "&reqtype=ACT"
	    		+ "&cpid=208"
	    		+ "&mode=WAP"
	    		+ "&lang=01"
	    		+ "&responsetype=HTML"
	    		+ "&seid=01"
	    		+ "&transactionid="+tx
	    		+ "&requesttimestamp="+t
	    		+ "&seid=01&circleid=NA"
	    		+ "&redirecturl="+redirect;
	    return new ModelAndView("redirect:"+bsnlCgPage);
	  }
	  
	  @RequestMapping(value={"/playrabbitsbsnl/callback"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	  public ModelAndView getCallbackSubscriptionPage(Locale locale, Model model,HttpServletRequest request)
	  {
		  
		  logger.info("Welcome home! The client locale is {}.", locale);
		  Map<String, String> map = new HashMap<String, String>();
			Enumeration headerNames = request.getHeaderNames();
			String msisdn = null;
			while (headerNames.hasMoreElements()) {
				String key = (String) headerNames.nextElement();
				String value = request.getHeader(key);
				map.put(key, value);
				logger.info("Key-->" + key + "|Value-->" + value);
				if(key.equalsIgnoreCase("MSISDN")){
					msisdn = value;
				}
			}
			
			
			
			
			for(OperatorMsisdnHeader operatorMsisdnHeader:OperatorMsisdnHeader.values()){
				 msisdn=request.getHeader(operatorMsisdnHeader.getMsisdnHeaderName());
			}
		    

			String appname = request.getParameter("appname");
			String operatorid = request.getParameter("operatorid");
			String operatorname = request.getParameter("operatorname");
			String sappid = request.getParameter("appid");
			//String msisdn = request.getParameter("msisdn");
			//String otp = request.getParameter("otp");
			int appid = -1;
			if (StringUtils.isEmpty(sappid)) {
				appid = 3;
				appname = "Play Rabbits";
			} else {
				appid = Integer.parseInt(sappid);
			}
			if (StringUtils.isEmpty(appname)) {
				operatorid = "0";
				operatorname = "Demo Play Rabbits";
				appid = 3;
			} else {
				operatorid = "6";
				operatorname = "BSNL";
				appid = 3;
			}

			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

			String formattedDate = dateFormat.format(date);

			List<BannerBean> bannerList = this.bannerBeanService.getBannerListByOperator(Integer.parseInt(operatorid),
					operatorname);
			logger.info("List of Banners::" + bannerList);

			model.addAttribute("serverTime", formattedDate);
			model.addAttribute("banners", bannerList);
			
			List<AppBean> appList = this.appBeanService.getAppInfo(appid, operatorname);
			logger.info("Applist::" + appList);

			List<LogoBean> logoList = this.logoBeanSerivce.getAppInfo(operatorname);
			logger.info("logoList::" + logoList);


			List<CategoryAppInfoBean> categoryAppInfoBeans = this.categoryAppInfoBeanService.getAppInfo(appid);
			logger.info("categoryAppInfoBeans::" + categoryAppInfoBeans);

			List<ContentCategoryMapBean> contentCategoryMapBeans = this.contentCategoryMapBeanService.getAppInfo(appid,
					operatorname);
			logger.info("contentCategoryMapBeans::" + contentCategoryMapBeans);

			model.addAttribute("contentCategoryMapBeans", contentCategoryMapBeans);
			model.addAttribute("categories", categoryAppInfoBeans);
			
			if (appList != null) {
				model.addAttribute("appname", appList.get(0).getAppname());
				model.addAttribute("logo", logoList.get(0).getLogopath());
			} else {
				model.addAttribute("appname", "Play Rabbits");
				model.addAttribute("logo", "");

			}
		  EncryptDecryptUtility ed = new EncryptDecryptUtility();
		  
		  String q =ed.getFormatEncryptedCGCallbackQueryString(request.getParameter("q"));
		 
		  
		  
		  String reqFinal =null; 
				  try{
					  		
					  System.out.println("Q Param-->"+q);
				  reqFinal = (!StringUtils.isEmpty(q)?ed.decrypt(q):"INVALID RESPONSE");
				  }catch(Exception e){
					  e.printStackTrace();
				  }
				  System.out.println("Request String-->"+reqFinal);
				  
				  try{
					  // insert records callback
					  BSNLSouthWestCallback bsnlSouthWestCallback = new BSNLSouthWestCallback();
					  MultiValueMap<String, String> parameters =
					            UriComponentsBuilder.fromUriString("?"+reqFinal).build().getQueryParams();			  
					  bsnlSouthWestCallback.setMsisdn(ed.formatString(parameters.get("msisdn")));
					  bsnlSouthWestCallback.setKeyword(ed.formatString(parameters.get("keyword")));
					  bsnlSouthWestCallback.setValidity(ed.formatString(parameters.get("validity")));
					  bsnlSouthWestCallback.setShortcode(ed.formatString(parameters.get("shortcode")));
					  bsnlSouthWestCallback.setMode(ed.formatString(parameters.get("mode")));
					  bsnlSouthWestCallback.setConsentMode(ed.formatString(parameters.get("consentmode")));
					  bsnlSouthWestCallback.setPrice(MUtility.toInt(ed.formatString(parameters.get("price")),0));
					  bsnlSouthWestCallback.setReqtype(ed.formatString(parameters.get("reqtype")));
					  bsnlSouthWestCallback.setConsentstatus(ed.formatString(parameters.get("consentstatus")));
					  bsnlSouthWestCallback.setRemarks(ed.formatString(parameters.get("remarks")));
					  bsnlSouthWestCallback.setCgid(ed.formatString(parameters.get("cgid")));
					  bsnlSouthWestCallback.setCircle(ed.formatString(parameters.get("circle")));
		          	bsnlSouthWestCallback.setTransactionId(ed.formatString(parameters.get("transactionid")));
		          	bsnlSouthWestCallback.setAutorenewal(ed.formatString(parameters.get("autorenewal")));
		          	bsnlSouthWestCallback.setRequesttimestamp(ed.formatString(parameters.get("requesttimestamp")));
		          	bsnlSouthWestCallback.setServiceId(ed.formatString(parameters.get("serviceid")));
		          	bsnlSouthWestCallback.setOpId(6); // write here logic for Operator id
					  this.bSNLSouthWestCallbackService.saveOperatorCallback(bsnlSouthWestCallback);
					  //if(bsnlSouthWestCallback.setConsentstatus(ed.formatString(parameters.get("consentstatus")));)
					  if(bsnlSouthWestCallback.getConsentstatus().equalsIgnoreCase("SUCCESS")){
						  	Date currentDate = new Date();
					        Calendar c = Calendar.getInstance();
					        c.setTime(currentDate);
					        // manipulate date
					        c.add(Calendar.YEAR, 1);
					        c.add(Calendar.MONTH, 1);
					        c.add(Calendar.DATE, 30); //same with c.add(Calendar.DAY_OF_MONTH, 1);
					        c.add(Calendar.HOUR, 1);
					        c.add(Calendar.MINUTE, 1);
					        c.add(Calendar.SECOND, 1);
					        // convert calendar to date
					        Date currentDatePlusOne = c.getTime();
					        System.out.println(dateFormat.format(currentDatePlusOne));
						  TbSubscribersRegDomestic p = new TbSubscribersRegDomestic();
						  p.setCampaign_id(bsnlSouthWestCallback.getCampaignId());
						  //p.setCircle_id(bsnlSouthWestCallback.getCircle()); // To-do Need to do logic for Circle id from circle
						  p.setLast_activity(null);
						  p.setLast_download_date(new Date());
						  p.setLast_update(new Date());
						  p.setMsisdn(bsnlSouthWestCallback.getMsisdn());
						  p.setOperator_id(bsnlSouthWestCallback.getOpId());
						  p.setParam1(bsnlSouthWestCallback.getServiceId()); // this we are using for operator service id map for reporting
						  p.setReg_date(new Date());
						  p.setService_id(Integer.parseInt(bsnlSouthWestCallback.getServiceId()));
						  p.setStatus(1);
						  ///p.setStatus_descp("SUBSCRIBERD");
						  p.setStatus_descp(bsnlSouthWestCallback.getRemarks());
						  p.setSub_date(new Date());
						  p.setToken(bsnlSouthWestCallback.getToken());
						 // p.setToken_id(bsnlSouthWestCallback.getToken());
						  p.setTotal_download_count(0);
						  p.setUnsub_date(null);
						  p.setSubscription_type("GREATOR_THAN_ZERO_PRICE_ACTIVATION");
						  p.setValidity_from(new Date());
						  p.setValidity_to(c.getTime());
						  this.tbSubscribersRegDomesticService.saveSubscriberDataFromCallback(p);
						  model.addAttribute("screenpage", "1");
							model.addAttribute("disablelinks", "2");
							model.addAttribute("screenmessage", 1);
					  }else{
						  
						  	Date currentDate = new Date();
					        Calendar c = Calendar.getInstance();
					        c.setTime(currentDate);

					        // manipulate date
					        c.add(Calendar.YEAR, 1);
					        c.add(Calendar.MONTH, 1);
					        c.add(Calendar.DATE, 0); //same with c.add(Calendar.DAY_OF_MONTH, 1);
					        c.add(Calendar.HOUR, 1);
					        c.add(Calendar.MINUTE, 1);
					        c.add(Calendar.SECOND, 1);

					        // convert calendar to date
					        Date currentDatePlusOne = c.getTime();

					        System.out.println(dateFormat.format(currentDatePlusOne));
						  
						  TbSubscribersRegDomestic p = new TbSubscribersRegDomestic();
						  p.setCampaign_id((bsnlSouthWestCallback.getCampaignId()!=null)?bsnlSouthWestCallback.getCampaignId():0);
						  //p.setCircle_id(bsnlSouthWestCallback.getCircle()); // To-do Need to do logic for Circle id from circle
						  p.setMsisdn(bsnlSouthWestCallback.getMsisdn());
						  p.setOperator_id(bsnlSouthWestCallback.getOpId());
						  p.setParam1(bsnlSouthWestCallback.getServiceId()); // this we are using for operator service id map for reporting
						  p.setService_id(Integer.parseInt(bsnlSouthWestCallback.getServiceId()));
						  p.setToken(bsnlSouthWestCallback.getToken());
							 
						  p.setLast_activity(null);
						  p.setLast_download_date(new Date());
						  p.setLast_update(new Date());
						  p.setReg_date(new Date());
						  p.setStatus(0);
						  //p.setStatus_descp("ERROR DURING SUBSCRIPTION");
						  p.setStatus_descp(bsnlSouthWestCallback.getRemarks());
						  p.setSub_date(new Date());
						  // p.setToken_id(bsnlSouthWestCallback.getToken());
						  p.setTotal_download_count(0);
						  p.setUnsub_date(null);
						  p.setSubscription_type(null);
						  p.setValidity_from(new Date());
						  p.setValidity_to(c.getTime());
						  this.tbSubscribersRegDomesticService.saveSubscriberDataFromCallback(p);
						  model.addAttribute("screenpage", "1");
							model.addAttribute("disablelinks", "1");
							model.addAttribute("screenmessage", 0);
					  }
					  
				  }catch(Exception e){
					  e.printStackTrace();
				  }
				  
				  
				
				  
		  
		  return new ModelAndView("themes/playrabbitsbsnl/home");
	  }
	  

}
