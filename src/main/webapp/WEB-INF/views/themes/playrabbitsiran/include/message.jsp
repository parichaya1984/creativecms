<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="message-status">
    <c:if test="${otppage == 2 }">
    
    <div class="message-alert">
       Please Enter OTP Shared on your Mobile
    </div>
    
    <div class="price-btn">
        <!-- <a href="#">monthly</a>
        <a href="#">15days</a>
        <a href="#">weekly</a>
        <a href="#">Daily</a> -->
        <form action="${pageContext.request.contextPath}/playrabbitsiran/validateOtp" id="validateotp" method="get"> 
        	<input type="text" id="otp" name="otp"/>
        	<input type="hidden" id="msisdn" name="msisdn" value="${msisdn }"/>
        	<input type="submit" value="Enter Otp">
        </form>
    </div>
    
    </c:if>
    <c:if test="${otppage == 1 }">
    <div class="message-alert">
       Please Enter Your Mobile Number for Validation
    </div>
    
    <div class="price-btn">
        <!-- <a href="#">monthly</a>
        <a href="#">15days</a>
        <a href="#">weekly</a>
        <a href="#">Daily</a> -->
        <form action="${pageContext.request.contextPath}/playrabbitsiran/sendOtp" id="validatemobileno" method="get"> 
        	<input type="text" id="msisdn" name="msisdn" />
        	<input type="submit" value="Validate Mobile Number">
        </form>
    </div>
    </c:if>
    <c:if test="${otppage == 3 }">
    <div class="message-alert">
       Welcome to Play Rabbits
    </div>
    </c:if>
</div>