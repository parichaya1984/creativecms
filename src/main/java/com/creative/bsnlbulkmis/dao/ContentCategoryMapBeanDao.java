package com.creative.bsnlbulkmis.dao;

import java.util.List;

import com.creative.bsnlbulkmis.beans.ContentCategoryMapBean;

public interface ContentCategoryMapBeanDao {
	
	public List<ContentCategoryMapBean> getAppInfo(int appid, String operatorname);

}
