package com.creative.cms.beans.domestic.dao;

import com.creative.cms.beans.domestic.TbSubscribersRegDomestic;

public interface TbSubscribersRegDomesticDao {
	
	public TbSubscribersRegDomestic getUserInfo(String msisdn);
	public void saveSubscriberDataFromCallback(TbSubscribersRegDomestic p);

}
