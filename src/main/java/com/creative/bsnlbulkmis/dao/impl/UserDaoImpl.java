package com.creative.bsnlbulkmis.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.bsnlbulkmis.beans.User;
import com.creative.bsnlbulkmis.dao.UserDao;

@Repository
public class UserDaoImpl implements UserDao {
	
	private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public User getUserByIdAndPassword(String userid, String password) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("userid", userid));
		
		Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("password", password));
		criteria.add(Restrictions.and(c));
		@SuppressWarnings("unchecked")
		List<User> uList = criteria.list();
		System.out.println("Size::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			logger.info("UserInformation loaded successfully, UserInformation details="+uList);
			//return uList.get(uList.size()-1);
			return uList.get(0);
		}
	}

}
