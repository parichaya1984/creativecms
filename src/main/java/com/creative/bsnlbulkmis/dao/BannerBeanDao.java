package com.creative.bsnlbulkmis.dao;

import java.util.List;

import com.creative.bsnlbulkmis.beans.BannerBean;

public interface BannerBeanDao {
	
	public List<BannerBean> getBannerListByOperator(int opsid, String operatorname);
	public void insertBrowsingDetails(BannerBean p);

}
