package com.creative.cms.beans.domestic.dao;

import com.creative.cms.beans.domestic.BSNLSouthWestCallback;

public interface BSNLSouthWestCallbackDao {
	
	public void saveOperatorCallback(BSNLSouthWestCallback p);

}
