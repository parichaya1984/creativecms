package com.creative.bsnlbulkmis.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity@Table(name="tb_banner")
public class BannerBean  {
	
	@Id
	@Column(name="bannerid")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bannerid;
	
	@Column(name="bannername")
	private String bannername;
	@Column(name="banneropsid")
	private int banneropsid;
	@Column(name="banneropsname")
	private String banneropsname;
	@Column(name="bannerstatus")
	private int bannerstatus;
	@Column(name="bannercreationdate")
	private Date bannercreationdate;
	@Column(name="bannerpath")
	private String bannerpath;
	public int getBannerid() {
		return bannerid;
	}
	public void setBannerid(int bannerid) {
		this.bannerid = bannerid;
	}
	public String getBannername() {
		return bannername;
	}
	public void setBannername(String bannername) {
		this.bannername = bannername;
	}
	public int getBanneropsid() {
		return banneropsid;
	}
	public void setBanneropsid(int banneropsid) {
		this.banneropsid = banneropsid;
	}
	public String getBanneropsname() {
		return banneropsname;
	}
	public void setBanneropsname(String banneropsname) {
		this.banneropsname = banneropsname;
	}
	public int getBannerstatus() {
		return bannerstatus;
	}
	public void setBannerstatus(int bannerstatus) {
		this.bannerstatus = bannerstatus;
	}
	public Date getBannercreationdate() {
		return bannercreationdate;
	}
	public void setBannercreationdate(Date bannercreationdate) {
		this.bannercreationdate = bannercreationdate;
	}
	public String getBannerpath() {
		return bannerpath;
	}
	public void setBannerpath(String bannerpath) {
		this.bannerpath = bannerpath;
	}
	@Override
	public String toString() {
		return "BannerBean [bannerid=" + bannerid + ", bannername=" + bannername + ", banneropsid=" + banneropsid
				+ ", banneropsname=" + banneropsname + ", bannerstatus=" + bannerstatus + ", bannercreationdate="
				+ bannercreationdate + ", bannerpath=" + bannerpath + "]";
	}
	
	

}
