<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<header  class="header-down">

   

        <div class="top-nav">

        <span class="menu">
            <img class="fa-bars" src="${pageContext.request.contextPath}/resources/playrabbitsiran/images/menu.png" aria-hidden="true" onclick="loadDoc()">
        </span>
            <ul>
            <c:forEach items="${categories }" var="category" varStatus="cnt">
            <a href="${pageContext.request.contextPath }/playrabbitsiran"><li>${category.categoryname }</li></a>
							
						</c:forEach>
</ul>
            <!--script-->
            <script>
                $("span.menu").click(function(){

                    $(".top-nav ul").slideToggle(500, function(){
                    });
                });

                function loadDoc() {

                    $( ".fa-bars" ).toggleClass( "menurotate" );
                }

            </script>

        </div>

        <?php
    }  else {
        ?>

        <div class="back-content-download">
            <a href="javascript:history.back();">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
        </div>

        <?php
    }
    ?>

    <script>
        var didScroll;
        var lastScrollTop = 0;
        var delta = 5;
        var navbarHeight = $('header').outerHeight();

        $(window).scroll(function(event){
            didScroll = true;
        });

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            if(Math.abs(lastScrollTop - st) <= delta)
                return;

            if (st > lastScrollTop && st > navbarHeight){
                $('header').removeClass('header-down').addClass('header-up');
            } else {
                if(st + $(window).height() < $(document).height()) {
                    $('header').removeClass('header-up').addClass('header-down');
                }
            }

            lastScrollTop = st;
        }
    </script>

    <div class="product-name">
       <a href="${pageContext.request.contextPath }/playrabbitsiran"> <img src="${logo }"></a>
<!--        <a href="index.php"><span>gamez</span>Boost</a>-->
    </div>

    <!--language start -->
    <!-- <select class="languages">
        <option value="en" data-title="English">en</option>
        <option value="ar" data-title="Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©" selected="">ar</option>
    </select> -->
    <!--language end -->

    <!--***search start***-->
    <form class="searchbox">
        <input type="search" placeholder="Search......" name="search" class="searchbox-input" onkeyup="buttonUp();" required>
        <input type="submit" class="searchbox-submit" value="Go">
        <span class="searchbox-icon">
            <i class="fa fa-search" aria-hidden="true"></i>
        </span>
    </form>
    <!--search js-->
    <script>
        $(document).ready(function(){
            var submitIcon = $('.searchbox-icon');
            var inputBox = $('.searchbox-input');
            var searchBox = $('.searchbox');
            var isOpen = false;
            submitIcon.click(function(){
                if(isOpen == false){
                    searchBox.addClass('searchbox-open');
                    inputBox.focus();
                    isOpen = true;
                } else {
                    searchBox.removeClass('searchbox-open');
                    inputBox.focusout();
                    isOpen = false;
                }
            });
            submitIcon.mouseup(function(){
                return false;
            });
            searchBox.mouseup(function(){
                return false;
            });
            $(document).mouseup(function(){
                if(isOpen == true){
                    $('.searchbox-icon').css('display','block');
                    submitIcon.click();
                }
            });
        });
        function buttonUp(){
            var inputVal = $('.searchbox-input').val();
            inputVal = $.trim(inputVal).length;
            if( inputVal !== 0){
                $('.searchbox-icon').css('display','none');
            } else {
                $('.searchbox-input').val('');
                $('.searchbox-icon').css('display','block');
            }
        }
    </script>
    <!--*** search end ***-->

</header>
