package com.creative.cms.beans.domestic.service;

import com.creative.cms.beans.domestic.TbSubscribersRegDomestic;

public interface TbSubscribersRegDomesticService { 
	
	public TbSubscribersRegDomestic getUserInfo(String msisdn);
	public void saveSubscriberDataFromCallback(TbSubscribersRegDomestic p);

}
