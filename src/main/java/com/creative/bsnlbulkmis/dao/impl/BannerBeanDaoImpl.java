package com.creative.bsnlbulkmis.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.creative.bsnlbulkmis.beans.BannerBean;
import com.creative.bsnlbulkmis.dao.BannerBeanDao;

@Repository
public class BannerBeanDaoImpl implements BannerBeanDao {
	
	private static final Logger logger = LoggerFactory.getLogger(BannerBeanDaoImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public List<BannerBean> getBannerListByOperator(int opsid, String operatorname) {
		Session session = this.sessionFactory.getCurrentSession();		
		Criteria criteria = session.createCriteria(BannerBean.class);
		criteria.add(Restrictions.eq("banneropsname", operatorname));
		/*Conjunction c = Restrictions.conjunction();
		c.add(Restrictions.eq("keyword", keyword));
		criteria.add(Restrictions.and(c));*/
		@SuppressWarnings("unchecked")
		List<BannerBean> uList = criteria.list();
		System.out.println("Size of Banner::"+uList.size());
		if(uList.size() ==0){
			return null;
		}else{
			//logger.info("ContentMsisdnKeywordMapping loaded successfully, ContentMsisdnKeywordMapping details="+uList);
			return uList;
		}
	}

	@Override
	public void insertBrowsingDetails(BannerBean p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
	}

}
